package callido.lms.controller;

import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import callido.lms.dao.CourseDao;
import callido.lms.dao.UserDao;
import callido.lms.model.Course;
import callido.lms.model.User;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CourseController {
	private static final Logger log = LoggerFactory.getLogger(CourseController.class);
	CourseDao courseDao =new CourseDao();
	UserDao userDao =new UserDao();
	@PostMapping(value = "/addUserCourseDetailsLMS.do", produces = "application/json")
	public Map<String, Object> addUserCourseDetails(@RequestBody Course course,HttpServletRequest req ) throws ClassNotFoundException, SQLException, PropertyVetoException {
		
		Map<String, Object> map = new LinkedHashMap<>();
		
		if(courseDao.insertUserCourseData(course)) {
			map.put("success", true);
			map.put("data",courseDao.insertUserCourseData(course));
		}else {
			map.put("success", false);
		}
		return map;
	}
	
	@PostMapping(value = "/getLastVisitedPageLMS.do", produces = "application/json")
	public Map<String, Object> getLastVisitedPage(@RequestBody Course course,HttpServletRequest request, HttpServletResponse res) throws ClassNotFoundException, SQLException, PropertyVetoException {
		Map<String, Object> map = new LinkedHashMap<>();
		
		String pageName = courseDao.getLastVisitedPage(course.getUserId(),course.getCourseId());
		map.put("success", true);
		map.put("data",pageName );
		
		return map;
	}
	@PostMapping(value = "/loginLMS.do", produces = "application/json")
	public Map<String, Object> loginLMS(@RequestBody User user,HttpServletRequest request, HttpServletResponse res) throws ClassNotFoundException, SQLException, PropertyVetoException {
		Map<String, Object> map = new LinkedHashMap<>();
		userDao.isUserExist(user.getUserId(), user.getPassword());
		map.put("success", true);
		map.put("message", "Login success");
		return map;
	}
	
}
