package callido.lms.controller;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import callido.lms.dao.DBConnect;
import callido.lms.dao.UserDao;
import callido.lms.model.User;

@Controller
@RequestMapping("/")
public class LoginController {
	private static final Logger log = LoggerFactory.getLogger(LoginController.class);
	
	UserDao userDao =new UserDao();
	@RequestMapping(value = "/login.do")
	public ModelAndView login(HttpServletRequest req, HttpServletResponse res)
			throws SQLException, PropertyVetoException {
	
		HttpSession session = req.getSession(); 
		session.setMaxInactiveInterval(120 * 60);
		
		User user = new User();
		String userId = req.getParameter("userId");
		String password = req.getParameter("password");
		User sessionScopeObj = (User) session.getAttribute("user");
		user = userDao.getUserData(userId, password) ;
		session.setAttribute("user", user);
		
		return new ModelAndView("viewCourse","user",user);
	}
	
	@RequestMapping(value = "/playContent.do")
	public ModelAndView playContent(HttpServletRequest req, HttpServletResponse res)
			throws SQLException, PropertyVetoException {
	
		HttpSession session = req.getSession();
		User user = new User();
		User sessionScopeObj = (User) session.getAttribute("user");
		System.out.println("sessionScopeObj "+sessionScopeObj.getEmail() +" "+sessionScopeObj.getFullname());
		
		return new ModelAndView("viewCourse","user",user);
	}
	
}
