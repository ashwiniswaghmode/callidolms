package callido.lms.model;

public class Course {
int courseId;
int moduleId;
int pageId;
String pageName;
int userId;
public int getCourseId() {
	return courseId;
}
public void setCourseId(int courseId) {
	this.courseId = courseId;
}
public int getModuleId() {
	return moduleId;
}
public void setModuleId(int moduleId) {
	this.moduleId = moduleId;
}
public int getPageId() {
	return pageId;
}
public void setPageId(int pageId) {
	this.pageId = pageId;
}
public String getPageName() {
	return pageName;
}
public void setPageName(String pageName) {
	this.pageName = pageName;
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}



}
