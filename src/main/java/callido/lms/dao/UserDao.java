package callido.lms.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import callido.lms.model.User;

public class UserDao {

	public User getUserData(String userId, String password) throws SQLException, PropertyVetoException {
		User userObj = null;
		ResultSet rs =null; 
		Connection con =DBConnect.getConnection();
		Statement stmt = con.createStatement();
		StringBuffer queryString =new StringBuffer();
		
		try {
			queryString.append(" SELECT user_id,fullname, ");
			queryString.append(" email_id ");
			queryString.append(" FROM user ");
			queryString.append(" WHERE user_id = '"+userId +"'");
			queryString.append(" AND password =  '"+password +"'");
			rs = stmt.executeQuery(queryString.toString());
			System.out.println("queryString.toString : " +queryString.toString());
			while (rs.next()) {
				userObj =new User();
				userObj.setUserId(rs.getString(1));
				userObj.setFullname(rs.getString(2));
				userObj.setEmail(rs.getString(3));
			}
		} catch (Exception e) {
		} finally {
			rs.close();
			stmt.close();
			con.close();
		}
	return userObj;
	}
	
	public boolean isUserExist(String userId, String password) throws SQLException, PropertyVetoException {
		boolean exist =false;
		ResultSet rs =null;
		Connection con = DBConnect.getConnection();
		PreparedStatement stmt = null;
		StringBuffer queryString =new StringBuffer();
		try {
			queryString.append(" select count(*) from user where ");
			queryString.append(" user_id = ? and password = ? ");
			stmt = con.prepareStatement(queryString.toString());
			stmt.setString(1, userId);
			stmt.setString(2, password);
			rs = stmt.executeQuery();
			while (rs.next()) {
				if (rs.getInt(1) > 0) {
					exist = true;
					System.out.println("user exists");
				}
			}
		}catch(Exception e) {
			rs.close();
			stmt.close();
			con.close();
		}
		return exist;
	}
	
}
