package callido.lms.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import com.mysql.cj.jdbc.MysqlDataSource;

public class DBConnect {
	
	private static MysqlDataSource ds = null;
	private static final Logger log = LogManager.getLogger(DBConnect.class);
	private static Connection connection =null;
	public static MysqlDataSource getDataSource() throws PropertyVetoException {
		
		ds = new MysqlDataSource();
		ds.setUrl("jdbc:mysql://localhost:3306/callido_lms");
	 	ds.setUser("root");
		ds.setPassword("rnt");
		return ds;
	}
	
	public static Connection getConnection() throws SQLException, PropertyVetoException {
		if(ds==null) {
			ds = (MysqlDataSource) DBConnect.getDataSource();
		}
		connection = ds.getConnection();
		//System.out.println("connection eshatablished");
		return connection;
	}
}
