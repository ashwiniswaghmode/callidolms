package callido.lms.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import callido.lms.model.Course;

public class CourseDao {
	private static final Logger log = LoggerFactory.getLogger(CourseDao.class);
	public boolean insertUserCourseData(Course course)
			throws SQLException, ClassNotFoundException, PropertyVetoException {

		PreparedStatement statement = null;
		Connection con = null;
		StringBuffer queryString = null;
		boolean status = false;
		queryString = new StringBuffer();

		try {
			con = DBConnect.getConnection();
			queryString.append(" INSERT INTO user_course_tracking(");
			queryString.append(" user_id, course_id ,page_name) VALUES(?,?,?)");

			statement = con.prepareStatement(queryString.toString());

			statement.setLong(1,course.getUserId() );
			statement.setLong(2, course.getCourseId());
			statement.setString(3,course.getPageName() );
			int rowsInserted = statement.executeUpdate();

			if (rowsInserted > 0) {
				status = true;
			}
		} catch (Exception e) {
			log.info("error occured" + e);
		} finally {
			if (statement != null)
				statement.close();
			con.close();
		}
		return status;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "null" })
	public String getLastVisitedPage(int userId,int courseId) throws SQLException {

		//String lastPage="a001_test_1_menu01_dot_01.html";
		String lastPage="";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnect.getConnection();
		
			String query = " SELECT page_name FROM  user_course_tracking where user_id = "+ userId +" AND course_id = "+ courseId +" ORDER BY seq_id DESC LIMIT 1 ";
			pstmt = conn.prepareStatement(query); // create a statement
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
			if(rs.getString(1)!=null) {
				lastPage = rs.getString(1);
				System.out.println("lastPage :: " +lastPage);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Exception occurred");
		} finally {
			rs.close();
			pstmt.close();
			conn.close();
		}
		return lastPage;
	}
}
