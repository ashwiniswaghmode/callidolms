<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		page = window.location.pathname.split("/").pop();
		//alert(page);
		menuChildren = $('a[href="' + page + '"]');
		$(menuChildren).parent('li').addClass('active');
		$(menuChildren).closest('[id ^= "listItemActive"]').addClass('active');

	});
</script>
<aside id="aside" class="ui-aside">
	<ul class="nav" ui-nav>
		<li class="nav-head">
			<h5 class="nav-title text-uppercase light-txt">${sessionScope.user.role}</h5>
		</li>

		<li id="listItemActive1"><a href="login.do"><img
				src="images/home (1).png" class="wid-8" /><span class="mt-8">Dashboard
			</span></a></li>


		<c:if
			test="${(sessionScope.user.role ne 'School Success Team') and (sessionScope.user.role ne 'School Success Admin')}">
			<li id="listItemActive2"><a href=""> <img
					src="images/Group 234.png" class="wid-8" /> <i
					class="fa fa-angle-right pull-right"></i> <span class="mt-8">Content
						Author </span>
			</a>
				<ul class="nav nav-sub RSRA_block">
					<li><a href="SystemAdmincontentAuthorNext.do"><span>Add
								Content</span></a></li>
					<li><a href="systemAdminContentAuthor.do"><span>Question
								Management</span></a></li>
				</ul></li>

		</c:if>
		<c:if
			test="${sessionScope.user.roleAccessMap['School Group'].isModuleAccess() or sessionScope.user.roleAccessMap['School'].isModuleAccess() or sessionScope.user.roleAccessMap['Teams'].isModuleAccess() or sessionScope.user.roleAccessMap['Student'].isModuleAccess() or sessionScope.user.roleAccessMap['Token Generator'].isModuleAccess() or sessionScope.user.roleAccessMap['Test Assignment'].isModuleAccess() or sessionScope.user.roleAccessMap['Client User Management'].isModuleAccess()}">
			<li id="listItemActive3"><a id="clientMgtMenu" href=""><img
					src="images/Group 234.png" class="wid-8" /><span class="mt-8">Client
						Management </span><i class="fa fa-angle-right pull-right"></i></a>
				<ul class="nav nav-sub RSRA_Client_block">
					<c:if
						test="${sessionScope.user.roleAccessMap['School Group'].isModuleAccess()}">
						<li><a href="schoolGroup.do"><span>School Group</span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['School'].isModuleAccess()}">
						<li><a href="displaySchool.do" id="schoolIdActive"><span>School
							</span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Teams'].isModuleAccess()}">
						<li><a href="team.do"><span>Team </span></a>
					</c:if>


					<c:if
						test="${sessionScope.user.roleAccessMap['Student'].isModuleAccess()}">
						<li><a href="student.do"><span>Student </span></a>
					</c:if>
					<c:if
						test="${sessionScope.user.roleAccessMap['Tagging Management'].isModuleAccess()}">
						<li><a href="taggingManagement.do" id="taggingId"><span>Tagging
									Management </span></a>
					</c:if>
					<c:if
						test="${sessionScope.user.roleAccessMap['Token Generator'].isModuleAccess()}">
						<li><a href="tokenGenerator.do"><span>Token
									Generator </span></a>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Test Assignment'].isModuleAccess()}">
						<li><a href="testAssignment.do"><span>Test
									Assignment </span></a>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Report Release View'].isModuleAccess()}">
						<li><a href="reportReleaseView.do"><span>Report
									Release View </span></a>
					</c:if>


					<c:if
						test="${sessionScope.user.roleAccessMap['Client User Management'].isModuleAccess()}">
						<li><a href="clientUserManagement.do" id="clientUserIdactive"><span>School
									Admin </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Teacher Dashboard'].isModuleAccess()}">
						<li><a href="teacherDashboardForAdmin.do"><span>Teacher Dashboard</span></a></li>
					</c:if>
					
						<li><a href="gradeReport.do"><span>Grade Report</span></a></li>

				</ul></li>
		</c:if>

		<c:if
			test="${sessionScope.user.roleAccessMap['Test Configuration'].isModuleAccess()}">
			<li id="listItemActive4"><a href="testConfiguration.do"> <img
					src="images/Subtraction 22.png" class="wid-8" /><span class="mt-8">Test
						Configuration </span></a>
		</c:if>


		<c:if
			test="${sessionScope.user.roleAccessMap['Callido User Management'].isModuleAccess() or sessionScope.user.roleAccessMap['Client User Management'].isModuleAccess() or sessionScope.user.roleAccessMap['Difficulty Levels'].isModuleAccess() or sessionScope.user.roleAccessMap['Question Types'].isModuleAccess() or sessionScope.user.roleAccessMap['Skills'].isModuleAccess() or sessionScope.user.roleAccessMap['Sub-skills'].isModuleAccess() or sessionScope.user.roleAccessMap['Grades'].isModuleAccess() or sessionScope.user.roleAccessMap['Role'].isModuleAccess() or sessionScope.user.roleAccessMap['Use Cases'].isModuleAccess() or sessionScope.user.roleAccessMap['RBAC'].isModuleAccess() or sessionScope.user.roleAccessMap['Assign User Role'].isModuleAccess()}">
			<li id="listItemActive5"><a href=""><img
					src="images/Group 234.png" class="wid-8" /><span class="mt-8">Admin
				</span><i class="fa fa-angle-right pull-right"></i></a>
				<ul class="nav nav-sub RSRA_Admin_block">
					<c:if
						test="${sessionScope.user.roleAccessMap['Callido User Management'].isModuleAccess()}">
						<li><a href="displaycallidoUserManagement.do"><span>Callido
									User Management </span></a></li>
					</c:if>



					<c:if
						test="${sessionScope.user.roleAccessMap['Difficulty Levels'].isModuleAccess()}">
						<li><a href="difficultyLevel.do"><span>Difficulty
									Levels </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Question Types'].isModuleAccess()}">
						<li><a href="questionType.do"><span>Question Types
							</span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Skills'].isModuleAccess()}">
						<li><a href="skillSet.do"><span>Skills </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Sub-skills'].isModuleAccess()}">
						<li><a href="subSkills.do"><span>Sub-Skills </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Grades'].isModuleAccess()}">
						<li><a href="grades.do"><span>Grades </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Role'].isModuleAccess()}">
						<li><a href="displayRole.do"><span>Role </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Use Cases'].isModuleAccess()}">
						<li><a href="displayUseCases.do"><span>Use Cases </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['RBAC'].isModuleAccess()}">
						<li><a href="displayRBAC.do"><span>RBAC </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Assign User Role'].isModuleAccess()}">
						<li><a href="assignrole.do"><span>Assign User Role
							</span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Level-Up'].isModuleAccess()}">
						<li><a href="levelUp.do"><span>Level-Up </span></a></li>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Educator'].isModuleAccess()}">
						<li><a href="displayEducator.do"><span>Educator </span></a></li>
					</c:if>
					
					<!-- <li><a href="sampleQuestionTeacherReport.do"><span>Sample Question</span></a></li> -->

				</ul></li>
		</c:if>
		<c:if
			test="${sessionScope.user.roleAccessMap['IRT Analysis'].isModuleAccess()}">

		</c:if>
		<c:if
			test="${sessionScope.user.roleAccessMap['Analyst'].isModuleAccess()}">
			<li id="listItemActive4"><a href="analyst.do"> <img
					src="images/Subtraction 22.png" class="wid-8" /><span class="mt-8">Analyst</span></a>
		</c:if>
		<c:if
			test="${sessionScope.user.roleAccessMap['Data Exports'].isModuleAccess()}">
			<li id="listItemActive7"><a id="reportIdActive"><i
					class="fa fa-home"></i><span>Data Exports</span><i
					class="fa fa-angle-right pull-right"></i></a>
				<ul class="nav nav-sub report_block">
					<li><a href="studentReport.do"><span>Test Report</span></a></li>
					<li><a href="testStatusReport.do"> <span>Test
								Status Report</span></a></li>
					<li><a href="adaptiveTestReport.do"> <span>Adaptive Test
								Report</span></a></li>
					<li><a href="questionCountReport.do"> <span>Question
								Count Report</span></a></li>
					<li><a href="questionItemWiseReport.do"> <span>Question
								Item Report</span></a></li>
					<li><a href="adaptiveQuestionItemWiseReport.do"> <span>Adaptive
								Question Item Report</span></a></li>
				</ul></li>

		</c:if>
					<!-- <li id=""><a href="sampleQuestionTeacherReport.do"><i
					class="fa fa-home"></i><span>Question Sample Text</span></a></li> -->
					<!-- 
					<li id=""><a href="uploadFolder.do"><i
					class="fa fa-home"></i><span>Upload Folder</span></a></li> -->
					
				<!-- 	<li id=""><a href="co-ordinatorReport.do"><i
					class="fa fa-home"></i><span>Co-ordinator Report</span></a></li> -->



	</ul>
</aside>