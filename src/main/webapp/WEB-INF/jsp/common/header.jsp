<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<header id="header" class="ui-header">

	<div class="navbar-header">
		<!--logo start-->
		<a href="login.do" class="navbar-brand"> <span class="logo"><img
				src="images/callido_logo.png" alt=""
				style="width: 135px; margin-left: 25px;" /></span> <span
			class="logo-compact"><img src="images/callido_logo.png" alt=""
				style="width: 135px; margin-left: 25px;" /></span>
		</a>
		<!--logo end-->
	</div>


 
	<div class="navbar-collapse nav-responsive-disabled navbar-inverse">
		<!--notification start-->
		
		<ul class="nav navbar-nav navbar-right" style="width: 17%;">
			<li class="dropdown dropdown-usermenu"><a href="#"
				class=" dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="padding-right: 40px;">
					<div class="user-avatar" style="float:left;">
						<img src="images/profile.png" alt="...">
					</div>
					<div style="float:right;"> 
					<span class="hidden-sm hidden-xs headeruser-text"  >${sessionScope.user.firstName}
						</span><br> <span class="hidden-sm hidden-xs
					role-li" style="width:14%;">${sessionScope.user.role}</span>
					<span class="caret hidden-sm hidden-xs caret_position" ></span>
					</div>
			</a>
			
				<ul class="dropdown-menu dropdown-menu-usermenu pull-right">
					<c:choose>
						<c:when test="${sessionScope.user.roleUserList.size() == '1'}">

						</c:when>
						<c:otherwise>
							<form action="loginwrole.do" method="POST">
								<li class="divider"></li>

								<c:forEach var="type" items="${sessionScope.user.roleUserList}">
									<li class="ml-5" style="display:flex;" ><input type="radio" style="top:6px;"  name="roleid" value="${type.key}"><span class="headerspan-text">${type.value}</span></li>
								</c:forEach>

								<input type="submit" value="Change Role" class="ml-5 sg-btn role-btn">
							</form>
						</c:otherwise>
					</c:choose>
					<li><a href="updateProfile.do"><i class="fa fa-pencil" style="color:#545252;"></i>Edit
							Profile</a></li>
					<li><a href="logout.do"><i class="fa fa-sign-out" style="color:#545252;"></i> Log
							Out</a></li>
				</ul>
				</li>

		</ul>
		<!--notification end-->

	</div>

</header>