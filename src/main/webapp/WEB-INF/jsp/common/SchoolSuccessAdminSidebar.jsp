<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		page = window.location.pathname.split("/").pop();
		//alert(page);
		menuChildren = $('a[href="' + page + '"]');
		$(menuChildren).parent('li').addClass('active');
		$(menuChildren).closest('[id ^= "listItemActive"]').addClass('active');

	});
</script>
<aside id="aside" class="ui-aside">
	<ul class="nav" ui-nav>
		<li class="nav-head">
			<h5 class="nav-title text-uppercase light-txt">${sessionScope.user.role}</h5>
		</li>
		
		<li id="listItemActive1"><a href="login.do"><img
				src="images/home (1).png" class="wid-8" /><span class="mt-8">Dashboard
			</span></a></li>

		<li id="listItemActive2"><a href="displaySchoolQuestions.do"><img
				src="images/Group 234.png" class="wid-8" /><span class="mt-8">Question
							Management </span>
		</a>
			</li>
				<c:if
						test="${sessionScope.user.roleAccessMap['Teams'].isModuleAccess()}">
						<li ><a href="team.do"><img src="images/team (1).png" class="wid-8"/><span class="mt-8">Team </span></a>
					</c:if>


					<c:if
						test="${sessionScope.user.roleAccessMap['Student'].isModuleAccess()}">
						<li ><a href="student.do"><img src="images/Group 234.png" class="wid-8"/><span class="mt-8">Student </span></a>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Token Generator'].isModuleAccess()}">
						<li ><a href="tokenGenerator.do"><img src="images/Group 235.png" class="wid-8"/><span class="mt-8">Token Generator </span></a>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Test Assignment'].isModuleAccess()}">
						<li ><a href="testAssignment.do"><img src="images/Subtraction 22.png" class="wid-8"/><span class="mt-8">Test Assignment </span></a>
					</c:if>

					<c:if
						test="${sessionScope.user.roleAccessMap['Client User Management'].isModuleAccess()}">
						<li><a href="clientUserManagement.do" id="clientUserIdactive"><img
				src="images/Group 234.png" class="wid-8" /><span class="mt-8">Client User Management </span></a></li>
					</c:if>
					<c:if
						test="${sessionScope.user.roleAccessMap['Tagging Management'].isModuleAccess()}">
						<li ><a href="taggingManagement.do"
							id="taggingId"><span>Tagging Management </span></a>
					</c:if>


		<c:if
			test="${sessionScope.user.roleAccessMap['Data Exports'].isModuleAccess()}">
			<li id="listItemActive7"><a id="reportIdActive"><i
					class="fa fa-home"></i><span>Data Exports</span><i
					class="fa fa-angle-right pull-right"></i></a>
				<ul class="nav nav-sub report_block">
					<li><a href="studentReport.do"><span>Test Report</span></a>
					<li><a href="testStatusReport.do"> <span>Test
								Status Report</span></a>
				</ul></li>

		</c:if>

	</ul>
</aside>