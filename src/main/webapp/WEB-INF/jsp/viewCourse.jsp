<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>View Course</title>

<!-- inject:css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/simple-line-icons.css">
<link rel="stylesheet" href="css/weather-icons.min.css">
<link rel="stylesheet" href="css/themify-icons.css">

<!-- endinject -->

<!-- Main Style  -->
<link rel="stylesheet" href="css/SAE.css">
<link rel="stylesheet" href="css/new_common.css">
<link href="datatable/jquery.dataTables.css" rel="stylesheet" />
<link href="datatable/dataTables.bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="css/sweetalert.css">

<link rel="stylesheet" href="css/dataTables.colVis.css">

<!--horizontal-timeline-->
<link rel="stylesheet" href="css/style.css">

<link href="datatable/jquery.dataTables.css" rel="stylesheet" />
<link href="datatable/dataTables.bootstrap.min.css" rel="stylesheet" />
<style>
#role_table tbody tr td {
	padding: 2px 4px;
	border-right: 1px solid #d9d9d9;
	font-size: 12px;
	/* text-align: center; */
}

#role_table thead tr th {
	text-align: center;
	font-size: 13px;
}
</style>

</head>
<body class="orgLevel5">

	<div id="ui" class="ui">

		<!--header start-->
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<header id="header" class="ui-header">

	<div class="navbar-header">
		<!--logo start-->
		<a href="login.do" class="navbar-brand"> <span class="logo"><img
				src="images/callido_logo.png" alt=""
				style="width: 135px; margin-left: 25px;" /></span> <span
			class="logo-compact"><img src="images/callido_logo.png" alt=""
				style="width: 135px; margin-left: 25px;" /></span>
		</a>
		<!--logo end-->
	</div>


 
	<div class="navbar-collapse nav-responsive-disabled navbar-inverse">
		<!--notification start-->
		
		<ul class="nav navbar-nav navbar-right" style="width: 17%;">
			<li class="dropdown dropdown-usermenu"><a href="#"
				class=" dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="padding-right: 40px;">
					<div class="user-avatar" style="float:left;">
						<img src="images/profile.png" alt="...">
					</div>
					<div style="float:right;"> 
					<span class="hidden-sm hidden-xs headeruser-text">${user.fullname}
						</span><br> <span class="hidden-sm hidden-xs
					role-li" style="width:14%;"></span>
					<span class="caret hidden-sm hidden-xs caret_position" ></span>
					</div>
			</a>
			
				</li>

		</ul>
		<!--notification end-->

	</div>

</header>
		<!--header end-->

		<!--sidebar start-->
		
		<!--sidebar end-->

		<!--main content start-->
		
		<div id="container" class="container-fluid">

	<div id="page" class="panel panel-default">
		<div id="page-content" class="panel-body">

			<!-- <button class="btn btn-danger" id="iframeBtn"  >Play</button> -->
			<form action="playContent.do">
			<button class="btn btn-danger" id="iframeBtn"  >Play</button>
			</form>
			<br> <br>
			
				<div class="quiz-time">
  			   </div>
    
			<div id="iframeHolder"></div>
		</div>
	
	</div>
	
	

</div>
		
		
		<!--main content end-->
		<!--footer start-->
		<div id="footer" class="ui-footer">
            2019 &copy; CALLIDO LMS.
        </div>
		<!--footer end-->

	</div>
	
	<!-- modal for dialogue box -->
	
	

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.nicescroll.min.js"></script>
	<script src="js/autosize.min.js"></script>
	<!-- endinject -->

	<!--horizontal-timeline-->
	<script src="js/jquery.mobile.custom.min.js"></script>
	<script src="js/main.js"></script>

	<script src="js/modernizr-custom.js"></script>

	<script src="js/utils.js"></script>
	<script src="js/Chart.bundle.js"></script>
	<!-- Common Script   -->
	<script src="js/SAE.js"></script>
	<script src="datatable/jquery.dataTables.min.js"></script>
	<script src="datatable/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript" src="js/dataTables.select.min.js"></script>
	<script src="js/sweetalert.min.js"></script>

<script>


$(function() {
		$('#iframeBtn').click(function() {
			
							if (!$('#iframe').length) {
								$('#iframeHolder')
										.html(
												'<iframe id="iframe" src="https://rntprojectdata.s3.us-east-2.amazonaws.com/LMS/Part1/index.html" target="_blank" height= "650" width="100%"></iframe>');
								// $('#iframeHolder').html('<iframe id="iframe" src="http://3.15.27.132/javatintan/" target="_blank" height= "650" width="100%"></iframe>');
							}

//start 

 var sec = 0;
      function pad ( val ) {

        return val > 9 ? val : "0" + val;
       }
      var myTimer= setInterval( function(){
          
          $("#seconds").html(pad(++sec%60));
          $("#minutes").html(pad(parseInt(sec/60,10)));
      }, 1000);
      function myStopFunction() {
              clearInterval(myTimer);
      }
// end 
							
							
});
	
});

</script>

	<script>
	 	$(document).ready(function(){
	 		$('#listItemActive5').addClass('active');
	 		$('.RSRA_Admin_block li:nth-child(6)').addClass('active');
	 	});
	</script>
</body>
</html>