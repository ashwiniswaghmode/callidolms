$(function() {
	/* Global variable */
	var selectedDropDownValue = 0;
	var dropDownBlankCouter = 0;
	var dropDownCounter = 1;
	var gobalMediaType = "video";
	var globalMediaTypePickOne = "video";
	var selectedValue, selectedText;
	var dropDownUpdateBoolean = false;
	var dropDownBlankUpdateBoolean = false;

	/* $('#questionBank1').dataTable(); */
	/* var questionTable = $('#questionBank1').dataTable(); */

	var d = new Date();
	$('title').html(
			"QB_" + d.getDate() + "-" + (d.getMonth() + 1) + "-"
					+ d.getFullYear());

	$('#questionBank1').dataTable({

		"order" : [ [ 0, "desc" ] ]

	/*
	 * dom : 'lBfrtip', buttons : [
	 *  { extend : 'excel', text : 'Export File', title: '', exportOptions : {
	 * modifier : { selected : null } } }, { extend : 'copy', text : 'Import
	 * File', exportOptions : { modifier : { selected : null } } }
	 *  ]
	 */
	});

	// $('#questionBankCA').dataTable();

	/* $('#questionText').richText(); */

	$('.dynamicQuestionTypeSelect')
			.on(
					'change',
					function() {

						selectedValue = $('.dynamicQuestionTypeSelect').find(
								':selected').val();
						selectedText = $('.dynamicQuestionTypeSelect').find(
								':selected').text();

						if (selectedValue == 0) {
							$('.titleQuestionType').text('');
						} else {
							$('.titleQuestionType').text(' : ' + selectedText);
						}

						$('.dynamicOptionContent ').css({
							'borderTop' : '1px solid #dadada',
							'padding' : '20px'
						});

						$('.richText-editor').html('');

						$('[id ^= "backgroundQusetionDiv"]').remove();

						if (selectedValue == 1) {
							selectedDropDownValue = 1;
							$('.dynamicOptionContent').load(
									'dragAndDropOption.do');

							$('#backgroundImageInput').addClass(
									"backgroundQuestionDragAndDrop");

							/*
							 * $('.dynamicOptionContent').before('<div
							 * class="form-group" id="backgroundQusetionDiv">' + '<label
							 * class=" col-sm-2 control-label">Background Image:
							 * <span class="text-danger">*</span></label>' + '<div
							 * class="col-sm-10">' + '<input type="file"
							 * name="" id="backgroundQuestionDragAndDrop"
							 * style="border: 1px solid #dadada; padding: 5px
							 * 12px;width: 40%;">' + '</div>' + '</div>');
							 */
							/* $('#questionText').richText(); */

							/*
							 * if ($('.richText-toolbar ul li:last-child a')
							 * .hasClass("addDropDown")) { $('.richText-toolbar
							 * ul li:last-child') .remove(); }
							 */
							if ($('.richText-toolbar>button').hasClass(
									"addDropDown")) {
								$('.richText-toolbar>button').remove();
							}

						}
						if (selectedValue == 2) {
							$('.dynamicOptionContent')
									.load('dropDownOption.do');

							$('#backgroundImageInput').addClass(
									"backgroundQuestionDropDown");

							/*
							 * $('.dynamicOptionContent').before('<div
							 * class="form-group" id="backgroundQusetionDiv">' + '<label
							 * class=" col-sm-2 control-label">Background Image:
							 * <span class="text-danger">*</span></label>' + '<div
							 * class="col-sm-10">' + '<input type="file"
							 * name="" id="backgroundQuestionDropDown"
							 * style="border: 1px solid #dadada; padding: 5px
							 * 12px;width: 40%;">' + '</div>' + '</div>');
							 */

							/*
							 * if ($('.richText-toolbar ul li:last-child a')
							 * .hasClass("addDropDown")) { $('.richText-toolbar
							 * ul li:last-child') .remove(); }
							 */
							if ($('.richText-toolbar>button').hasClass(
									"addDropDown")) {
								$('.richText-toolbar>button').remove();
							}
						}
						if (selectedValue == 3) {
							$('.dynamicOptionContent').load(
									'dropDownBlankOption.do');

							$('#backgroundImageInput').addClass(
									"backgroundQuestionDropBlank");

							/*
							 * $('.dynamicOptionContent').before('<div
							 * class="form-group" id="backgroundQusetionDiv">' + '<label
							 * class=" col-sm-2 control-label">Background Image:
							 * <span class="text-danger">*</span></label>' + '<div
							 * class="col-sm-10">' + '<input type="file"
							 * name="" id="backgroundQuestionDropBlank"
							 * style="border: 1px solid #dadada; padding: 5px
							 * 12px;width: 40%;">' + '</div>' + '</div>');
							 */

							/*
							 * $('.richText-toolbar ul') .append( '<li><a
							 * class="addDropDown richText-btn" data-command=""
							 * title="Add Drop down"><span class="fa fa-plus"></span>
							 * Dropdown</a></li>');
							 */
							$('.richText-toolbar')
									.append(
											'<button class="addDropDown btn"><span class="fa fa-plus"></span> Dropdown</button>');

							$('.dynamicOptionContent ').css({
								'borderTop' : '0px solid #dadada'
							});

						}
						if (selectedValue == 4) {
							$('.dynamicOptionContent').load('hotSpotOption.do');

							$('#backgroundImageInput').addClass(
									"backgroundQuestionHotSpot");

							/*
							 * $('.dynamicOptionContent').before('<div
							 * class="form-group" id="backgroundQusetionDiv">' + '<label
							 * class=" col-sm-2 control-label">Background Image:
							 * <span class="text-danger">*</span></label>' + '<div
							 * class="col-sm-10">' + '<input type="file"
							 * name="" id="backgroundQuestionHotSpot"
							 * style="border: 1px solid #dadada; padding: 5px
							 * 12px;width: 40%;">' + '</div>' + '</div>');
							 */

							/*
							 * if ($('.richText-toolbar ul li:last-child
							 * a').hasClass("addDropDown")) {
							 * $('.richText-toolbar ul li:last-child').remove(); }
							 */
							if ($('.richText-toolbar>button').hasClass(
									"addDropDown")) {
								$('.richText-toolbar>button').remove();
							}

						}
						/*
						 * if (selectedValue == 4) {
						 * $('.dynamicOptionContent').load('pickOneOption.do');
						 * 
						 * if ($('.richText-toolbar ul li:last-child
						 * a').hasClass("addDropDown")) { $('.richText-toolbar
						 * ul li:last-child').remove(); } }
						 */
						if (selectedValue == 5) {
							$('.dynamicOptionContent')
									.load('pickManyOption.do');

							$('#backgroundImageInput').addClass(
									"backgroundQuestionPickMany");

							/*
							 * $('.dynamicOptionContent').before('<div
							 * class="form-group" id="backgroundQusetionDiv">' + '<label
							 * class=" col-sm-2 control-label">Background Image:
							 * <span class="text-danger">*</span></label>' + '<div
							 * class="col-sm-10">' + '<input type="file"
							 * name="" id="backgroundQuestionPickMany"
							 * style="border: 1px solid #dadada; padding: 5px
							 * 12px;width: 40%;">' + '</div>' + '</div>');
							 */

							/*
							 * if ($('.richText-toolbar ul li:last-child a')
							 * .hasClass("addDropDown")) { $('.richText-toolbar
							 * ul li:last-child') .remove(); }
							 */
							if ($('.richText-toolbar>button').hasClass(
									"addDropDown")) {
								$('.richText-toolbar>button').remove();
							}

						}
						if (selectedValue == 6) {
							$('.dynamicOptionContent').load('pickOneOption.do');

							$('#backgroundImageInput').addClass(
									"backgroundQuestionPickOne");

							/*
							 * if ($('.richText-toolbar ul li:last-child a')
							 * .hasClass("addDropDown")) { // alert('working
							 * hasClass'); $('.richText-toolbar ul
							 * li:last-child') .remove(); }
							 */
							if ($('.richText-toolbar>button').hasClass(
									"addDropDown")) {
								$('.richText-toolbar>button').remove();
							}
						}

						// reset background image on question type change
						/*
						 * $('#backgroundImageInput').val('');
						 * $('#backgroundImageInput').attr('src','');
						 */
					});

	/* preview create question starts */

	$(document)
			.on(
					'change',
					'.backgroundQuestionDragAndDrop',
					function() {
						$('#previewCreateQuestionModal .modal-body').css(
								'background-image',
								'url(' + $(this).attr("src") + ')');
						// readURLBackground(this);
					});
	$(document)
			.on(
					'change',
					'.backgroundQuestionHotSpot',
					function() {
						$('#previewCreateHotSpotModal .modal-body').css(
								'background-image',
								'url(' + $(this).attr("src") + ')');
						// readURLBackgroundHotSpot(this);
					});
	$(document)
			.on(
					'change',
					'.backgroundQuestionDropDown',
					function() {
						$('#previewCreateDropDownModal .modal-body').css(
								'background-image',
								'url(' + $(this).attr("src") + ')');
						// readURLBackgroundDropDown(this);
					});
	$(document)
			.on(
					'change',
					'.backgroundQuestionPickMany',
					function() {
						$('#previewCreatePickManyModal .modal-body').css(
								'background-image',
								'url(' + $(this).attr("src") + ')');
						// readURLBackgroundPickMany(this);
					});
	$(document)
			.on(
					'change',
					'.backgroundQuestionPickOne',
					function() {
						$('#previewCreatePickOneModal .modal-body').css(
								'background-image',
								'url(' + $(this).attr("src") + ')');
						// readURLBackgroundPickMany(this);
					});
	$(document)
			.on(
					'change',
					'.backgroundQuestionDropBlank',
					function() {
						$('#previewCreateDropBlankModal .modal-body').css(
								'background-image',
								'url(' + $(this).attr("src") + ')');
						// readURLBackgroundDropBlank(this);
					});

	$(document).on('change', '#dragItemImage1', function() {
		readURLPreviewDragDropItem(this);
	});

	$(document)
			.on(
					'click',
					'#previewCreateQuestion',
					function() {
						var selectedValue = $('.dynamicQuestionTypeSelect')
								.find(':selected').val();
						// selectedValue = $('#questionTypeId').val();
						var selectedText = $('.dynamicQuestionTypeSelect')
								.find(':selected').text();

						var questionTable = $('#questionBank1').dataTable();

						var questionRowCount = questionTable.fnGetData().length;
						$('.questionNumberInPreview')
								.text(questionRowCount + 1);

						if (selectedValue == 1) {
							$('#previewCreateQuestionModal').modal({
								backdrop : 'static',
								keyboard : false
							});
							// BOTH METHOD CHANGES FROM .text() TO
							// .html()....THEN IT IS WORKING FOR PREVIEW
							/* $('#dragAndDropPreviewQuestionText').text($('.richText-editor').text()); */
							$('#dragAndDropPreviewQuestionText').html(
									$('.richText-editor').html());

							$('[id ^= "dropZoneName"]')
									.each(
											function() {
												var dropZoneNameId = this.id[12];
												// alert(hotSpotId);
												$(
														'#widthDropZoneOption'
																+ dropZoneNameId)
														.val(
																$(
																		'#dropZoneName'
																				+ dropZoneNameId)
																		.outerWidth());
												$(
														'#heightDropZoneOption'
																+ dropZoneNameId)
														.val(
																$(
																		'#dropZoneName'
																				+ dropZoneNameId)
																		.outerHeight());
												$(
														'#xAxisDropZoneOption'
																+ dropZoneNameId)
														.val(
																$(
																		'#dropZoneName'
																				+ dropZoneNameId)
																		.position().left);
												$(
														'#yAxisDropZoneOption'
																+ dropZoneNameId)
														.val(
																$(
																		'#dropZoneName'
																				+ dropZoneNameId)
																		.position().top);
											});

							$('[id ^= "dropItemName"]')
									.each(
											function() {
												var dropItemNameId = this.id[12];
												// alert(hotSpotId);
												$(
														'#widthDragAndDropOption'
																+ dropItemNameId)
														.val(
																$(
																		'#dropItemName'
																				+ dropItemNameId)
																		.outerWidth());
												$(
														'#heightDragAndDropOption'
																+ dropItemNameId)
														.val(
																$(
																		'#dropItemName'
																				+ dropItemNameId)
																		.outerHeight());
												$(
														'#xAxisDragAndDropOption'
																+ dropItemNameId)
														.val(
																$(
																		'#dropItemName'
																				+ dropItemNameId)
																		.position().left);
												$(
														'#yAxisDragAndDropOption'
																+ dropItemNameId)
														.val(
																$(
																		'#dropItemName'
																				+ dropItemNameId)
																		.position().top);
											});

							$('[id ^= "dropZoneName"]')
									.each(
											function() {
												var dropZoneNameId = this.id[12];
												// alert(hotSpotId);
												$(
														'#dropZonePreviewName'
																+ dropZoneNameId)
														.outerWidth(
																$(
																		'#widthDropZoneOption'
																				+ dropZoneNameId)
																		.val());
												$(
														'#dropZonePreviewName'
																+ dropZoneNameId)
														.outerHeight(
																$(
																		'#heightDropZoneOption'
																				+ dropZoneNameId)
																		.val());
												var previewLeft = $(
														'#dropZoneName'
																+ dropZoneNameId)
														.position().left
														+ 'px';
												var previewTop = $(
														'#dropZoneName'
																+ dropZoneNameId)
														.position().top
														+ 'px';

												$(
														'#dropZonePreviewName'
																+ dropZoneNameId)
														.css(
																{
																	'left' : previewLeft,
																	'top' : previewTop
																});
											});

							$('[id ^= "dropItemName"]')
									.each(
											function() {
												var dropZoneNameId = this.id[12];
												// alert(dropZoneNameId);
												$(
														'#dropItemPreviewName'
																+ dropZoneNameId)
														.outerWidth(
																$(
																		'#widthDragAndDropOption'
																				+ dropZoneNameId)
																		.val());
												$(
														'#dropItemPreviewName'
																+ dropZoneNameId)
														.outerHeight(
																$(
																		'#heightDragAndDropOption'
																				+ dropZoneNameId)
																		.val());
												var previewLeft = $(
														'#dropItemName'
																+ dropZoneNameId)
														.position().left
														+ 'px';
												var previewTop = $(
														'#dropItemName'
																+ dropZoneNameId)
														.position().top
														+ 'px';

												$(
														'#dropItemPreviewName'
																+ dropZoneNameId)
														.css(
																{
																	'left' : previewLeft,
																	'top' : previewTop
																});
											});

							if ($('#backgroundImageInput').attr('src') == '') {

								$('#previewCreateQuestionModal .modal-body')
										.css('background-image',
												'url("images/Image_LightGrey.jpg")');
							}
						}
						if (selectedValue == 4) {
							$('#previewCreateHotSpotModal').modal({
								backdrop : 'static',
								keyboard : false
							});
							// BOTH METHOD CHANGES FROM .text() TO
							// .html()....THEN IT IS WORKING FOR PREVIEW
							$('#hotSpotPreviewQuestionText').html(
									$('.richText-editor').html());

							$('[id ^= "hotSpotRegionName"]')
									.each(
											function() {
												var hotSpotId = this.id[17];
												// alert(hotSpotId);
												$(
														'#widthHotSpotRegion'
																+ hotSpotId)
														.val(
																$(
																		'#hotSpotRegionName'
																				+ hotSpotId)
																		.outerWidth());
												$(
														'#heightHotSpotRegion'
																+ hotSpotId)
														.val(
																$(
																		'#hotSpotRegionName'
																				+ hotSpotId)
																		.outerHeight());
												$(
														'#xAxisHotSpotRegion'
																+ hotSpotId)
														.val(
																$(
																		'#hotSpotRegionName'
																				+ hotSpotId)
																		.position().left);
												$(
														'#yAxisHotSpotRegion'
																+ hotSpotId)
														.val(
																$(
																		'#hotSpotRegionName'
																				+ hotSpotId)
																		.position().top);
											});
							$('[id ^= "hotSpotRegionName"]')
									.each(
											function() {
												var dropZoneNameId = this.id[17];
												// alert($('#widthHotSpotRegion'+dropZoneNameId).val());
												$(
														'#hotSpotPreviewRegionName'
																+ dropZoneNameId)
														.outerWidth(
																$(
																		'#widthHotSpotRegion'
																				+ dropZoneNameId)
																		.val());
												$(
														'#hotSpotPreviewRegionName'
																+ dropZoneNameId)
														.outerHeight(
																$(
																		'#heightHotSpotRegion'
																				+ dropZoneNameId)
																		.val());
												var previewLeft = $(
														'#hotSpotRegionName'
																+ dropZoneNameId)
														.position().left
														+ 'px';
												var previewTop = $(
														'#hotSpotRegionName'
																+ dropZoneNameId)
														.position().top
														+ 'px';
												// alert(previewLeft+'\n'+previewTop);
												$(
														'#hotSpotPreviewRegionName'
																+ dropZoneNameId)
														.css(
																{
																	'left' : previewLeft,
																	'top' : previewTop
																});
											});
							if ($('#backgroundImageInput').attr('src') == '') {
								$('#previewCreateHotSpotModal .modal-body')
										.css('background-image',
												'url("images/Image_LightGrey.jpg")');
							}
						}
						if (selectedValue == 2) {
							$('#previewCreateDropDownModal').modal({
								backdrop : 'static',
								keyboard : false
							});
							// BOTH METHOD CHANGES FROM .text() TO
							// .html()....THEN IT IS WORKING FOR PREVIEW
							$('#dropdownPreviewQuestionText').html(
									$('.richText-editor').html());

							var numberOfImageDropdown = $('.containerDropdown').length;

							$('[id ^= "dropdownImageTable"]')
									.each(
											function() {
												var dropdownImageId = this.id[18];

												var optionCounter = $(
														'#dropdownImageTable'
																+ dropdownImageId)
														.find(
																'[id ^= "optionDropdown"]').length;
												// alert('no. of option:
												// '+optionCounter);
												for (var i = 1; i <= optionCounter; i++) {
													// alert(
													// $('#dropdownImageTable'+dropdownImageId+'
													// #optionDropdown'+i).val()
													// );
													$(
															'#dropdownPreviewImageSelect'
																	+ dropdownImageId)
															.append(
																	'<option value="">'
																			+ $(
																					'#dropdownImageTable'
																							+ dropdownImageId
																							+ ' #optionDropdown'
																							+ i)
																					.val()
																			+ '</option>');
												}
												if ($(
														'.dropDownTypeSelect'
																+ dropdownImageId)
														.find(':selected')
														.text() == "Text") {
													// alert( 'textarea:
													// '+$('#textareaDropDown'+dropdownImageId).val()
													// );
													$(
															'#textareaDropDownPreview'
																	+ dropdownImageId)
															.val(
																	$(
																			'#textareaDropDown'
																					+ dropdownImageId)
																			.val());
												}
											});
							if ($('#backgroundImageInput').attr('src') == '') {
								$('#previewCreateDropDownModal .modal-body')
										.css('background-image',
												'url("images/Image_LightGrey.jpg")');
							}

						}
						if (selectedValue == 5) {
							$('#previewCreatePickManyModal').modal({
								backdrop : 'static',
								keyboard : false
							});
							// BOTH METHOD CHANGES FROM .text() TO
							// .html()....THEN IT IS WORKING FOR PREVIEW
							$('#pickManyPreviewQuestionText').html(
									$('.richText-editor').html());

							if ($('.optionTypePickMany').find(':selected')
									.text() == "Image") {
								$('[id ^= "inputPickMany"]')
										.each(
												function() {
													var inputPickManyId = this.id[13];

													$(
															'#answerOptionImageType'
																	+ inputPickManyId)
															.css(
																	'background-image',
																	'url('
																			+ $(
																					this)
																					.attr(
																							"src")
																			+ ')');
												});
							} else {
								$('[id ^= "inputPickMany"]')
										.each(
												function() {
													var inputPickManyId = this.id[13];
													$(
															'#answerPickManyLabelSpan'
																	+ inputPickManyId)
															.text($(this).val());
													if ($(this).val() == "") {
														$(
																'#answerPickManyLabelSpan'
																		+ inputPickManyId)
																.text(
																		"Option "
																				+ inputPickManyId);
													}
												});
							}
							if ($('#backgroundImageInput').attr('src') == '') {
								$('#previewCreatePickManyModal .modal-body')
										.css('background-image',
												'url("images/Image_LightGrey.jpg")');
							}

						}
						if (selectedValue == 3) {
							//alert('called once..??');
							$('#previewCreateDropBlankModal').modal({
								backdrop : 'static',
								keyboard : false
							});
							// BOTH METHOD CHANGES FROM .text() TO
							// .html()....THEN IT IS WORKING FOR PREVIEW
							
							$('#previewModalDropBlank').html($('.richText-editor').html());
							
							/*
							 * var heightDiv =
							 * $('#dropBlankPreviewQuestionText').height();
							 * alert(heightDiv); $('#previewQDiv').css();
							 */
							$(
									'#previewModalDropBlank [id ^= "dynamicBlankSelect"]')
									.find('option:first').text('');
														

							if ($('#instructionBlank').val() == "") {
								$('#dropBlankPreviewQuestionText').html(
										'Please select appropriate words');
							} else {
								$('#dropBlankPreviewQuestionText').html(
										$('#instructionBlank').val());
							}

							var numberOfImageDropdown = $('.containerDropdown').length;

							$('[id ^= "dropDownBlankTable"]')
									.each(
											function() {
												var dropdownImageId = this.id[18];

												var optionCounter = $(
														'#dropDownBlankTable'
																+ dropdownImageId)
														.find(
																'[id ^= "response"]').length;
												// alert(optionCounter);
												for (var i = 1; i <= optionCounter; i++) {
													// alert(
													// $('#dropdownImageTable'+dropdownImageId+'
													// #optionDropdown'+i).val()
													// );
													// $('#dropdownPreviewImageSelect'+dropdownImageId).append('<option
													// value="">'+$('#dropdownImageTable'+dropdownImageId+'
													// #optionDropdown'+i).val()+'</option>');
													$('#previewModalDropBlank #dynamicBlankSelect'+ dropdownImageId).append(
																	'<option value="">'
																			+ $('#dropDownBlankTable'
																							+ dropdownImageId
																							+ ' #response'
																							+ dropdownImageId
																							+ 'option'
																							+ i)
																					.val()
																			+ '</option>');
												}
											});
							if ($('#backgroundImageInput').attr('src') == '') {
								$('#previewCreateDropBlankModal .modal-body')
										.css('background-image',
												'url("images/Image_LightGrey.jpg")');
							}
						}

						if (selectedValue == 6) {
							$('#previewCreatePickOneModal').modal({
								backdrop : 'static',
								keyboard : false
							});
							// BOTH METHOD CHANGES FROM .text() TO
							// .html()....THEN IT IS WORKING FOR PREVIEW
							$('#pickOnePreviewQuestionText').html(
									$('.richText-editor').html());

							if ($('.pickOneOptionType').find(':selected')
									.text() == "Image") {
								$('[id ^= "pickOneOption"]')
										.each(
												function() {
													var inputPickManyId = this.id[13];
													$(
															'#answerOptionPickOneImage'
																	+ inputPickManyId)
															.css(
																	'background-image',
																	'url('
																			+ $(
																					this)
																					.attr(
																							"src")
																			+ ')');
												});
							} else {
								$('[id ^= "pickOneOption"]')
										.each(
												function() {
													var inputPickManyId = this.id[13];
													$(
															'#answerPickOneLabelSpan'
																	+ inputPickManyId)
															.text($(this).val());
													if ($(this).val() == "") {
														$(
																'#answerPickOneLabelSpan'
																		+ inputPickManyId)
																.text(
																		"Option "
																				+ inputPickManyId);
													}
												});
							}
							if ($('#backgroundImageInput').attr('src') == '') {
								$('#previewCreatePickOneModal .modal-body')
										.css('background-image',
												'url("images/Image_LightGrey.jpg")');
							}

						}
						if (selectedText == 'Select') {
							sweetAlert("Plaese select question type first");
							return false;
						}
					});

	$(document).on('click', '.previewClose', function() {
		// alert("foucs trasfer");
		setTimeout(function() {
			$('body').addClass('modal-open');
		}, 1000);

		/*
		 * $('#myCreateQuestionModal').css({ "overflowX": "hidden", "overflowY":
		 * "auto" });
		 */

		var bodyId = $(this).closest("body").attr("id");
		if (bodyId == "editQuestionDashboard") {
			// alert("edit body working");
			// $('body').removeClass('modal-open');
			setTimeout(function() {
				$('body').removeClass('modal-open');
			}, 1000);
		}

	});
	$(document).on('click', '#previewCreateDropDownModal .previewClose',
			function() {
				// alert("foucs trasfer");
				// setTimeout(function () { $('body').addClass('modal-open'); },
				// 1000);
				$('[id ^= "dropdownPreviewImageSelect"]').html('');

			});

	/* preview create question ends */

	/* ==============Drag and drop starts============ */

	/* *****Drag and drop preview after typing question***** */

	$(document).on('keyup', '.richText-editor', function() {
		// console.log( $(this).text( ));
		$('#dragAndDropQuestionText').text($(this).text());

	});

	$(document).on('keyup', '#dragAndDropQuestionNumberID', function() {
		$('#dragAndDropQuestionNumber').text($(this).val());

	});

	/* adding row to drop zone table */
	$(document)
			.on(
					'click',
					'#addDropZone',
					function() {
						var dropZoneCount = $('[id ^= "dropZoneName"]').length;
						
						if(dropZoneCount >= 8){		
							swal('Drop Zone can not be more than 8');
							return false;
						}
						
						var dropZoneCounter = parseInt($(
								'#dropZoneTable tr:last td:first').text());
						dropZoneCounterPlusOne = dropZoneCounter + 1;
						$('#dropZoneTable tr:last')
								.after(
										'<tr>' + '<td>'
												+ dropZoneCounterPlusOne
												+ '</td>'
												+ '<td>'
												+ '<input type="text" name="dropZoneName" class="pickOneOption " id="dropZoneOptionName'
												+ dropZoneCounterPlusOne
												+ '" required placeholder="Drop Zone '
												+ dropZoneCounterPlusOne
												+ '">'
												+ '</td>'
												+ '<td>'
												+ ' <i class="fa fa-trash fa-trashDropZone"></i>'
												+ '</td>'
												+ '<input type="hidden" name="widthDropZoneOption"  id="widthDropZoneOption'
												+ dropZoneCounterPlusOne
												+ '" value="0">'
												+ '<input type="hidden" name="heightDropZoneOption" id="heightDropZoneOption'
												+ dropZoneCounterPlusOne
												+ '" value="0">'
												+ '<input type="hidden" name="xAxisDropZoneOption"  id="xAxisDropZoneOption'
												+ dropZoneCounterPlusOne
												+ '" value="0">'
												+ '<input type="hidden" name="yAxisDropZoneOption"  id="yAxisDropZoneOption'
												+ dropZoneCounterPlusOne
												+ '" value="0">' + '</tr>');

						$('.dropzone').last().after(
								'<div  class="dropzone draggable resize-drag" id="dropZoneName'
										+ dropZoneCounterPlusOne
										+ '" style="clear:right;">Drop Zone '
										+ dropZoneCounterPlusOne + '</div>');
						/* For Preview */
						$('.dropZonePreviewContainer').append(
								'<div class="" id="dropZonePreviewName'
										+ dropZoneCounterPlusOne
										+ '" style="width: 25%;">Drop Zone '
										+ dropZoneCounterPlusOne + '</div>');
					});

	/* deleting row of drop zone table */
	$(document).on(
			'click',
			'.fa-trashDropZone',
			function() {
				// alert("delete for update");
				var dropZoneId = $(this).closest('tr').find('td:first-child')
						.text();

				var siblings = $(this).closest('tr').siblings();
				// alert(siblings.length);
				$(this).closest('tr').remove();

				siblings.each(function(index) {
					$(this).children('td').first().text(index + 1);
					/*
					 * $(this).children('td:nth-child(2)
					 * input').attr('id','dropZoneOptionName'+ (index+1) );
					 */
					$(this).find('td:nth-child(2) input').attr('id',
							'dropZoneOptionName' + (index + 1));
					$(this).find('td:nth-child(2) input').attr('placeholder',
							'Drop Zone ' + (index + 1));

					$(this).find('input[name=widthDropZoneOption]').attr('id',
							'widthDropZoneOption' + (index + 1));
					$(this).find('input[name=heightDropZoneOption]').attr('id',
							'heightDropZoneOption' + (index + 1));
					$(this).find('input[name=xAxisDropZoneOption]').attr('id',
							'xAxisDropZoneOption' + (index + 1));
					$(this).find('input[name=yAxisDropZoneOption]').attr('id',
							'yAxisDropZoneOption' + (index + 1));
				});

				$('#dropZoneName' + dropZoneId).remove();
				$('[id ^= "dropZoneName"]').each(function(index) {
					$(this).attr("id", "dropZoneName" + (index + 1));

					var dropZoneText = $(this).text();
					if (dropZoneText.indexOf("Drop Zone") >= 0) {
						$(this).text("Drop Zone " + (index + 1));
					}

				});

				$('#dropZonePreviewName' + dropZoneId).remove();
				$('[id ^= "dropZonePreviewName"]').each(function(index) {
					$(this).attr("id", "dropZonePreviewName" + (index + 1));

					var dropZonePreviewText = $(this).text();
					if (dropZonePreviewText.indexOf("Drop Zone") >= 0) {
						$(this).text("Drop Zone " + (index + 1));
					}

				});
				// Previous 3 line code
				/*
				 * $(this).closest('tr').remove(); $('#dropZoneName'+
				 * dropZoneId).remove(); $('#dropZonePreviewName'+
				 * dropZoneId).remove();
				 */

			});

	/* adding row to drag item table */
	$(document)
			.on(
					'click',
					'#addDragItem',
					function() {
						 var dragItemCount = $('[id ^= "dropItemName"]').length;
						    
							if(dragItemCount >= 8){		
								swal('Drop Items can not be more than 8');
								return false;
							}

						var dragItemCounter = parseInt($(
								'#dragItemTable tr:last td:first').text());
						dragItemCounterPlusOne = dragItemCounter + 1;

						if (($('.dragAndDropTypeSelect').find(':selected')
								.text() == "Image")) {

							$('#dragItemTable tr:last')
									.after(
											'<tr>' + '<td>'
													+ dragItemCounterPlusOne
													+ '</td>'
													+ '<td>'
													+ '<div class="" id="inputFileType">'
													+ '<input type="button" class="" id="buttonDropDownInputAWS'
													+ dragItemCounterPlusOne
													+ '" value="Choose File" placeholder="background image" onclick="getImages(\'image\',this)" >'
													+ '<input type="text"  value="" placeholder="No file chosen" id="dragItemImage'
													+ dragItemCounterPlusOne
													+ '" name="dropZoneOption" src="" required readonly>'
													+ '</div>'
													+ '</td>'
													+ '<td>'
													+ '<input type="text" class="checkManyDragZoneFlagAnswer" name="correctDropZoneID" id="" onchange="javascript:return isNumber(this)" placeholder="Drop Zone ID" required>'
													+

													'</td>'
													+ '<td>'
													+ ' <i class="fa fa-trash fa-trashDragItem"></i>'
													+ '</td>'
													+ '<input type="hidden" name="widthDragAndDropOption" id="widthDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">'
													+ '<input type="hidden" name="heightDragAndDropOption" id="heightDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">'
													+ '<input type="hidden" name="xAxisDragAndDropOption" id="xAxisDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">'
													+ '<input type="hidden" name="yAxisDragAndDropOption" id="yAxisDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">' + '</tr>');

							$('.drag-drop').last().after(
									'<div  class="drag-drop resize-drag" id="dropItemName'
											+ dragItemCounterPlusOne
											+ '"> Item '
											+ dragItemCounterPlusOne
											+ ' </div>');

							/* For Preview */
							$('.dragItemPreviewContainer ').append(
									'<div class="" id="dropItemPreviewName'
											+ dragItemCounterPlusOne
											+ '"> Item '
											+ dragItemCounterPlusOne
											+ ' </div>');
						} else {
							$('#dragItemTable tr:last')
									.after(
											'<tr>' + '<td>'
													+ dragItemCounterPlusOne
													+ '</td>'
													+ '<td>'
													+ '<textarea id="textareaTableDropItem'
													+ dragItemCounterPlusOne
													+ '" name="dropZoneOption" placeholder="Option '
													+ dragItemCounterPlusOne
													+ '" style="width: 100%; height: 21px;border: 0;padding: 0;resize: none;cursor: move;" required></textarea>'
													+ '</td>'
													+ '<td>'
													+ '<input type="text" class="checkManyDragZoneFlagAnswer" name="correctDropZoneID" id="" onchange="javascript:return isNumber(this)" placeholder="Drop Zone ID" required>'
													+

													'</td>'
													+ '<td>'
													+ ' <i class="fa fa-trash fa-trashDragItem"></i>'
													+ '</td>'
													+ '<input type="hidden" name="widthDragAndDropOption" id="widthDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">'
													+ '<input type="hidden" name="heightDragAndDropOption" id="heightDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">'
													+ '<input type="hidden" name="xAxisDragAndDropOption" id="xAxisDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">'
													+ '<input type="hidden" name="yAxisDragAndDropOption" id="yAxisDragAndDropOption'
													+ dragItemCounterPlusOne
													+ '" value="0">' + '</tr>');

							$('.drag-drop')
									.last()
									.after(
											'<div  class="drag-drop resize-drag" id="dropItemName'
													+ dragItemCounterPlusOne
													+ '">'
													+ '<textarea id="textareaDropItem'
													+ dragItemCounterPlusOne
													+ '" placeholder="Item '
													+ dragItemCounterPlusOne
													+ '" readonly style="width: 100%;height: 100%;border:0;resize: none;"></textarea>'
													+ '</div>');

							/* For Preview */
							$('.dragItemPreviewContainer ')
									.append(
											'<div class="" id="dropItemPreviewName'
													+ dragItemCounterPlusOne
													+ '">'
													+ '<textarea id="textareaPreviewDropItem'
													+ dragItemCounterPlusOne
													+ '" placeholder="Item '
													+ dragItemCounterPlusOne
													+ '" readonly style="width: 100%;height: 100%;border:0;resize: none;color: #000;cursor: move;"></textarea>'
													+ '</div>');
						}

					});

	/* deleting row of drop zone table */
	$(document)
			.on(
					'click',
					'.fa-trashDragItem',
					function() {
						var dragDropId = $(this).closest('tr').find(
								'td:first-child').text();

						var siblings = $(this).closest('tr').siblings();

						$(this).closest('tr').remove();

						if (($('.dragAndDropTypeSelect').find(':selected')
								.text() == "Image")) {

							siblings.each(function(index) {
								$(this).children('td').first().text(index + 1);
								/*
								 * $(this).children('td:nth-child(2)
								 * input').attr('id','dropZoneOptionName'+
								 * (index+1) );
								 */
								$(this).find('td:nth-child(2) div input:first')
										.attr(
												'id',
												'buttonDropDownInputAWS'
														+ (index + 1));
								$(this).find('td:nth-child(2) div input:last')
										.attr('id',
												'dragItemImage' + (index + 1));

								$(this).find(
										'input[name=widthDragAndDropOption]')
										.attr(
												'id',
												'widthDragAndDropOption'
														+ (index + 1));
								$(this).find(
										'input[name=heightDragAndDropOption]')
										.attr(
												'id',
												'heightDragAndDropOption'
														+ (index + 1));
								$(this).find(
										'input[name=xAxisDragAndDropOption]')
										.attr(
												'id',
												'xAxisDragAndDropOption'
														+ (index + 1));
								$(this).find(
										'input[name=yAxisDragAndDropOption]')
										.attr(
												'id',
												'yAxisDragAndDropOption'
														+ (index + 1));
							});
							$('#dropItemName' + dragDropId).remove();
							$('[id ^= "dropItemName"]')
									.each(
											function(index) {
												$(this).attr(
														"id",
														"dropItemName"
																+ (index + 1));

												if ($(this).attr("style")) {
													$(this).text("");
												} else {
													$(this)
															.text(
																	"Item "
																			+ (index + 1));
												}

											});

							$('#dropItemPreviewName' + dragDropId).remove();
							$('[id ^= "dropItemPreviewName"]')
									.each(
											function(index) {
												$(this).attr(
														"id",
														"dropItemPreviewName"
																+ (index + 1));

												if ($(this).attr("style")) {
													$(this).text("");
												} else {
													$(this)
															.text(
																	"Item "
																			+ (index + 1));
												}

											});
						} else {

							siblings
									.each(function(index) {
										$(this).children('td').first().text(
												index + 1);
										/*
										 * $(this).children('td:nth-child(2)
										 * input').attr('id','dropZoneOptionName'+
										 * (index+1) );
										 */
										$(this)
												.find(
														'td:nth-child(2) textarea')
												.attr(
														'id',
														'textareaTableDropItem'
																+ (index + 1));
										$(this)
												.find(
														'td:nth-child(2) textarea')
												.attr('placeholder',
														'Option ' + (index + 1));

										$(this)
												.find(
														'input[name=widthDragAndDropOption]')
												.attr(
														'id',
														'widthDragAndDropOption'
																+ (index + 1));
										$(this)
												.find(
														'input[name=heightDragAndDropOption]')
												.attr(
														'id',
														'heightDragAndDropOption'
																+ (index + 1));
										$(this)
												.find(
														'input[name=xAxisDragAndDropOption]')
												.attr(
														'id',
														'xAxisDragAndDropOption'
																+ (index + 1));
										$(this)
												.find(
														'input[name=yAxisDragAndDropOption]')
												.attr(
														'id',
														'yAxisDragAndDropOption'
																+ (index + 1));
									});

							$('#dropItemName' + dragDropId).remove();
							$('[id ^= "dropItemName"]').each(
									function(index) {
										$(this).attr("id",
												"dropItemName" + (index + 1));

										$(this).find('textarea').attr(
												"id",
												"textareaDropItem"
														+ (index + 1));
										$(this).find('textarea').attr(
												"placeholder",
												"Item " + (index + 1));

										/*
										 * if( $(this).attr("style") ){
										 * $(this).text(""); }else{
										 * $(this).text("Item "+ (index+1)); }
										 */

									});

							$('#dropItemPreviewName' + dragDropId).remove();
							$('[id ^= "dropItemPreviewName"]').each(
									function(index) {
										$(this).attr(
												"id",
												"dropItemPreviewName"
														+ (index + 1));

										$(this).find('textarea').attr(
												"id",
												"textareaPreviewDropItem"
														+ (index + 1));
										$(this).find('textarea').attr(
												"placeholder",
												"Item " + (index + 1));

									});

						}

						/*
						 * $(this).closest('tr').remove(); $('#dropItemName' +
						 * dragDropId).remove(); $('#dropItemPreviewName' +
						 * dragDropId).remove();
						 *//*
																			 * For
																			 * Preview
																			 */
					});

	/* TTMACDynnamic starts */
	$(document).on(
			'change',
			'[id ^= "dragItemImage"]',
			function() {
				var imageId = this.id[13];
				// alert($(this).attr('src'));
				$('#dropItemName' + imageId).text("");
				$('#dropItemPreviewName' + imageId).text(""); /* For Preview */
				// readURLDragAndDrop(this);
				$('#dropItemName' + imageId).css('background-image',
						'url(' + $(this).attr('src') + ')');
				/* For Preview */
				$('#dropItemPreviewName' + imageId).css('background-image',
						'url(' + $(this).attr('src') + ')');
			});
	/* TTMDynnamic ends */

	/* TTMACDynnamic starts */
	$(document).on(
			'keyup',
			'[id ^= "dropZoneOptionName"]',
			function() {
				var dropZoneOptionId = this.id[18];

				$('#dropZoneName' + dropZoneOptionId).text($(this).val());
				$('#dropZonePreviewName' + dropZoneOptionId)
						.text($(this).val());
				if ($(this).val() == "") {
					$('#dropZoneName' + dropZoneOptionId).text(
							"Drop Zone " + dropZoneOptionId);
					$('#dropZonePreviewName' + dropZoneOptionId).text(
							"Drop Zone " + dropZoneOptionId);
				}
			});
	/* TTMACDynnamic ends */

	/* ===============Drag and drop ends================= */

	/* saving the question */

	$('#createQuestionSave').on(
			'click',
			function() {
				var selectedValue = $('.dynamicQuestionTypeSelect').find(
						':selected').val();
				/*
				 * if(typeof(selectedValue) == "undefined"){
				 * 
				 * selectedValue =
				 * $('#dynamicQuestionTypeSelect').find(':selected').val(); }
				 */
				if (selectedValue == 5) {
					var pickOneQuestion = $('#questionText').val();
					var options = [ $('#pickOneOption1').val(),
							$('#pickOneOption2').val(),
							$('#pickOneOption3').val(),
							$('#pickOneOption4').val() ];
					var correctOption = $(
							'input[name=pickOneOptionRadio]:checked')
							.attr("id");

				}

				/* coordinates of drop zone and drag-items starts */

				if (selectedValue == 1) {

					/* TTMACDynnamic starts */
					var numberOfDropZones = $('[id ^= "dropZoneName"]').length;
					var j, dropZones = [];

					for (j = 1; j <= numberOfDropZones; j++) {
						dropZones[j] = [ $('#dropZoneName' + j).position().top,
								$('#dropZoneName' + j).position().left,
								$('#dropZoneName' + j).outerWidth(),
								$('#dropZoneName' + j).outerHeight() ];
					}
					for (j = 1; j <= numberOfDropZones; j++) {
						// console.log('Drop Zone ' + j + ': ' + dropZones[j]);
					}

					var numberOfDragItems = $('[id ^= "dropItemName"]').length;
					var i, dragItemAndDrop = [];

					for (i = 1; i <= numberOfDragItems; i++) {
						dragItemAndDrop[i] = [
								$('#dropItemName' + i).position().top,
								$('#dropItemName' + i).position().left,
								$('#dropItemName' + i).outerWidth(),
								$('#dropItemName' + i).outerHeight() ];
					}
					for (i = 1; i <= numberOfDragItems; i++) {
						// console.log('Drag Item ' + i + ': ' +
						// dragItemAndDrop[i]);
					}
					/* TTMACDynnamic ends */

					$('[id ^= "dropZoneName"]').each(
							function() {
								var hotSpotId = this.id[12];
								// alert(hotSpotId);
								$('#widthDropZoneOption' + hotSpotId).val(
										$('#dropZoneName' + hotSpotId)
												.outerWidth());
								$('#heightDropZoneOption' + hotSpotId).val(
										$('#dropZoneName' + hotSpotId)
												.outerHeight());
								$('#xAxisDropZoneOption' + hotSpotId).val(
										$('#dropZoneName' + hotSpotId)
												.position().left);
								$('#yAxisDropZoneOption' + hotSpotId).val(
										$('#dropZoneName' + hotSpotId)
												.position().top);
							});

					$('[id ^= "dropItemName"]').each(
							function() {
								var hotSpotId = this.id[12];

								$('#widthDragAndDropOption' + hotSpotId).val(
										$('#dropItemName' + hotSpotId)
												.outerWidth());
								$('#heightDragAndDropOption' + hotSpotId).val(
										$('#dropItemName' + hotSpotId)
												.outerHeight());
								$('#xAxisDragAndDropOption' + hotSpotId).val(
										$('#dropItemName' + hotSpotId)
												.position().left);
								$('#yAxisDragAndDropOption' + hotSpotId).val(
										$('#dropItemName' + hotSpotId)
												.position().top);
							});

				}

				/* coordinates of drop zone and drag-items ends */

				if (selectedValue == 4) {

					$('[id ^= "hotSpotRegionName"]').each(
							function() {
								var hotSpotId = this.id[17];

								$('#widthHotSpotRegion' + hotSpotId).val(
										$('#hotSpotRegionName' + hotSpotId)
												.outerWidth());
								$('#heightHotSpotRegion' + hotSpotId).val(
										$('#hotSpotRegionName' + hotSpotId)
												.outerHeight());
								$('#xAxisHotSpotRegion' + hotSpotId).val(
										$('#hotSpotRegionName' + hotSpotId)
												.position().left);
								$('#yAxisHotSpotRegion' + hotSpotId).val(
										$('#hotSpotRegionName' + hotSpotId)
												.position().top);
							});

				}

			});

	/* hotspot after selecting image */
	$(document).on(
			'change',
			'#hotSpotImage',
			function() {
				// readURL(this);

				$('#previewHotSpot').css('background-image',
						'url(' + $(this).attr("src") + ')');
				$('#previewModalHotSpot').css('background-image',
						'url(' + $(this).attr("src") + ')');
				$('#previewHotSpot').find('h1').remove();

			});

	// Hotspot Region name changing dynamically
	$(document).on(
			'keyup',
			'[id ^= "hotSpotNameRegion"]',
			function() {
				var hotSpotRegionNameCounter = this.id[17];
				$('#hotSpotRegionName' + hotSpotRegionNameCounter).text(
						$(this).val());
				$('#hotSpotPreviewRegionName' + hotSpotRegionNameCounter).text(
						$(this).val()); /* For Preview */
				if ($(this).val() == "") {
					$('#hotSpotRegionName' + hotSpotRegionNameCounter).text(
							"Region " + hotSpotRegionNameCounter);
					$('#hotSpotPreviewRegionName' + hotSpotRegionNameCounter)
							.text("Region " + hotSpotRegionNameCounter);
				}
			});

	/* hotSpot region type after selecting dropdown */
	$(document).on(
			'change',
			'[id ^= "regionTypeHotSpot"]',
			function() {

				var regionNumber = this.id[17];

				if ($(this).find("option:selected").text() == "Circle") {
					$('#hotSpotRegionName' + regionNumber).css('border-radius',
							'50%');
					$('#hotSpotPreviewRegionName' + regionNumber).css(
							'border-radius', '50%');
				}
				if ($(this).find("option:selected").text() == "Rectangle") {
					$('#hotSpotRegionName' + regionNumber).css('border-radius',
							'0%');
					$('#hotSpotPreviewRegionName' + regionNumber).css(
							'border-radius', '0%');
				}

			});

	$(document)
			.on(
					'click',
					'#addHotSpotRegion',
					function() {
						var hotSpotAddOptionCounter = parseInt($(
								'#hotSpotTable tr:last td:first').text());
						// alert(hotSpotAddOptionCounter);

						hotSpotAddOptionCounterPlusOne = hotSpotAddOptionCounter + 1;

						$('#hotSpotTable tr:last')
								.after(
										'<tr>' + '<td>'
												+ hotSpotAddOptionCounterPlusOne
												+ '</td>'
												+ '<td>'
												+ '<input type="text" name="option" class=" hotSpotOption" id="hotSpotNameRegion'
												+ hotSpotAddOptionCounterPlusOne
												+ '" placeholder="Region '
												+ hotSpotAddOptionCounterPlusOne
												+ '" required>'
												+ '</td>'
												+ '<td>'
												+ '<div class="col-sm-11" style="padding-left: 0;">'
												+ '<select name="shape" class="form-control " id="regionTypeHotSpot'
												+ hotSpotAddOptionCounterPlusOne
												+ '">'
												+

												'<option value="Rectangle">Rectangle</option>'
												+ '<option value="Circle">Circle</option>'
												+ '</select>'
												+ '</div>'
												+ '</td>'
												+ '<td>'
												+ '<input type="radio" name="correctanswer" id="" value="'
												+ hotSpotAddOptionCounterPlusOne
												+ '">'
												+ '</td>'
												+ '<td>'
												+ '<i class="fa fa-trash fa-trashHotSpot"></i>'
												+ '</td>'
												+ '<input type="hidden" name="widthHotSpotRegion" id="widthHotSpotRegion'
												+ hotSpotAddOptionCounterPlusOne
												+ '" style="border: 0;" value="0">'
												+ '<input type="hidden" name="heightHotSpotRegion" id="heightHotSpotRegion'
												+ hotSpotAddOptionCounterPlusOne
												+ '" style="border: 0;" value="0">'
												+ '<input type="hidden" name="xAxisHotSpotRegion" id="xAxisHotSpotRegion'
												+ hotSpotAddOptionCounterPlusOne
												+ '" style="border: 0;" value="0">'
												+ '<input type="hidden" name="yAxisHotSpotRegion" id="yAxisHotSpotRegion'
												+ hotSpotAddOptionCounterPlusOne
												+ '" style="border: 0;" value="0">'
												+ '</tr>');

						$('#previewHotSpot').append(
								'<div class="resize-drag draggable" id="hotSpotRegionName'
										+ hotSpotAddOptionCounterPlusOne
										+ '" >Region-'
										+ hotSpotAddOptionCounterPlusOne
										+ '</div>');
						$('#previewModalHotSpot').append(
								'<div class="" id="hotSpotPreviewRegionName'
										+ hotSpotAddOptionCounterPlusOne
										+ '">Region-'
										+ hotSpotAddOptionCounterPlusOne
										+ '</div>');
					});

	$(document).on(
			'click',
			'.fa-trashHotSpot',
			function() {
				/* $(this).closest('tr').remove(); */
				/*
				 * var hotSpotRegionId =
				 * $(this).closest('tr').find('td:first-child').text();
				 * 
				 * $(this).closest('tr').remove(); $('#hotSpotRegionName' +
				 * hotSpotRegionId).remove(); $('#hotSpotPreviewRegionName' +
				 * hotSpotRegionId).remove();
				 */

				var hotSpotRegionId = $(this).closest('tr').find(
						'td:first-child').text();

				var siblings = $(this).closest('tr').siblings();

				$(this).closest('tr').remove();

				siblings.each(function(index) {
					$(this).children('td').first().text(index + 1);
					/*
					 * $(this).children('td:nth-child(2)
					 * input').attr('id','dropZoneOptionName'+ (index+1) );
					 */
					$(this).find('td:nth-child(2) input').attr('id',
							'hotSpotNameRegion' + (index + 1));
					$(this).find('td:nth-child(2) input').attr('placeholder',
							'Region ' + (index + 1));

					$(this).find('td:nth-child(3) div select').attr('id',
							'regionTypeHotSpot' + (index + 1));

					$(this).find('td:nth-child(4) input').attr('value',
							(index + 1));

					$(this).find('input[name=widthHotSpotRegion]').attr('id',
							'widthHotSpotRegion' + (index + 1));
					$(this).find('input[name=heightHotSpotRegion]').attr('id',
							'heightHotSpotRegion' + (index + 1));
					$(this).find('input[name=xAxisHotSpotRegion]').attr('id',
							'xAxisHotSpotRegion' + (index + 1));
					$(this).find('input[name=yAxisHotSpotRegion]').attr('id',
							'yAxisHotSpotRegion' + (index + 1));
				});

				$('#hotSpotRegionName' + hotSpotRegionId).remove();
				$('[id ^= "hotSpotRegionName"]').each(function(index) {
					$(this).attr("id", "hotSpotRegionName" + (index + 1));
					$(this).text("Region-" + (index + 1));
				});

				$('#hotSpotPreviewRegionName' + hotSpotRegionId).remove();
				$('[id ^= "hotSpotPreviewRegionName"]').each(
						function(index) {
							$(this).attr("id",
									"hotSpotPreviewRegionName" + (index + 1));
							$(this).text("Region-" + (index + 1));
						});
			});

	/* blank dropdown */
	$(document)
			.on(
					'click',
					'.addDropDown',
					function() {
						// alert('working');

						$('.dynamicOptionContent ').css({
							'borderTop' : '1px solid #dadada'
						});

						dropDownBlankCouter++;

						var totalDropdownPresent = document
								.getElementsByClassName('dropDownBlankCounterEdit').length;
						// alert('totalDropdownPresent :
						// '+totalDropdownPresent);
						if ((totalDropdownPresent > 0)
								&& (!dropDownBlankUpdateBoolean)) {
							// alert('drop down counter update ');
							dropDownBlankCouter = totalDropdownPresent + 1;
							dropDownBlankUpdateBoolean = true;
						}

						var blankResponseOptionCounter = parseInt($(
								'#dropDownBlankTable' + dropDownBlankCouter
										+ ' tr:last td:first').text());

						if ($(".richText-editor div").length > 0) {
							$(".richText-editor div br").remove();
							$('.richText-editor div:last-child')
									.append(
											'<select name="response'
													+ dropDownBlankCouter
													+ '" id="dynamicBlankSelect'
													+ dropDownBlankCouter
													+ '" style="padding: 4px;border-radius: 5px;margin-left:5px;margin-right: 5px;"><option value="">'
													+ dropDownBlankCouter
													+ '</option></select>');
						} else {
							$(".richText-editor br").remove();
							$('.richText-editor')
									.append(
											'<select name="response'
													+ dropDownBlankCouter
													+ '" id="dynamicBlankSelect'
													+ dropDownBlankCouter
													+ '" style="padding: 4px;border-radius: 5px;margin-left:5px;margin-right: 5px;"><option value="">'
													+ dropDownBlankCouter
													+ '</option></select>');
						}

						$('#dropDownBlankDiv')
								.append(
										'<div class="col-xs-4 col-sm-4 col-lg-4" id="blankDivCounter'
												+ dropDownBlankCouter
												+ '">'
												+ '<div class="row">'
												+ '<div class="col-md-8">'
												+ '<label>Response '
												+ dropDownBlankCouter
												+ '</label>'
												+ '<input type = "hidden" name="responseCount" value="'
												+ dropDownBlankCouter
												+ '">'
												+ '</div>'
												+ '<div class="col-md-4 text-right">'
												+

												'</div>'
												+

												'</div>'
												+

												'<table id="dropDownBlankTable'
												+ dropDownBlankCouter
												+ '" class="questionResponse container">'
												+ '<thead>'
												+ '<tr>'
												+ '<th>SrNo</th>'
												+ '<th>Options</th>'
												+ '<th>Correct Option</th>'
												+ '<th>Actions</th>'
												+ '</tr>'
												+

												'</thead>'
												+ '<tbody>'
												+ '<tr>'
												+ '<td>1</td>'
												+ '<td>'
												+ '<input type="text" name="response'
												+ dropDownBlankCouter
												+ 'option1" class="pickOneOption" id="response'
												+ dropDownBlankCouter
												+ 'option1" placeholder="Option 1" required>'
												+ '</td>'
												+ '<td><input type="radio" class="checkManyDropDownBlankAnswer" name="response'
												+ dropDownBlankCouter
												+ 'correctanswer" id="" value="1"></td>'
												+ '<td>'
												+ ' <span style="cursor: not-allowed;"><i class="fa fa-trash fa-trashBlankDropdown" style="pointer-events: none;opacity: 0.7;"></i></span>'
												+ '</td>'
												+ '</tr>'
												+ '<tr>'
												+ '<td>2</td>'
												+ '<td>'
												+ '<input type="text" name="response'
												+ dropDownBlankCouter
												+ 'option2" class="pickOneOption" id="response'
												+ dropDownBlankCouter
												+ 'option2" placeholder="Option 2" required>'
												+ '</td>'
												+ '<td><input type="radio" class="checkManyDropDownBlankAnswer" name="response'
												+ dropDownBlankCouter
												+ 'correctanswer" id="" value="2"></td>'
												+ '<td>'
												+ ' <i class="fa fa-trash fa-trashBlankDropdown"></i>'
												+ '</td>'
												+ '</tr>'
												+ '</tbody>'
												+ '</table>'
												+ '<div class="col-md-12 " style="padding-top: 5px;padding-right: 0;">'
												+ '<span class="pull-right">'
												+ '<button type="button" class="res btn btn-sm" id="dynamicSelectAddOption'
												+ dropDownBlankCouter
												+ '" style="margin-bottom: 5px;">Add Option</button>'
												+ '</span>' + '</div>'
												+ '</div>');

					});

	$(document)
			.on(
					'keyup',
					'.richText-editor',
					function() {

						// alert(dropDownBlankCouter );
						var blankDropdownCount = $(this).find(
								'[id ^= "dynamicBlankSelect"]').length;
						// alert(blankDropdownCount);

						if (dropDownBlankCouter != blankDropdownCount) {
							// alert('dropdown deleted');

							var a = [], i = 0, j = 1;

							$('[id ^= "dynamicBlankSelect"]').each(function() {
								a[i] = this.id[18];
								i++;
							});
							// console.log('a: ' + a);

							for (i = 0; i < a.length; i++) {
								if (!(a[i] == (i + 1))) {
									// console.log('a:' + a[i] + ' i:' + (i +
									// 1));
									break;
								}
							}
							$('#blankDivCounter' + (i + 1)).remove();

							dropDownBlankCouter = blankDropdownCount;

							$('[id ^= "dynamicBlankSelect"]').each(function() {
								this.id = "dynamicBlankSelect" + j;
								$(this).find('option').text(j);
								j++;
							});

							j = 1;
							$('[id ^= "blankDivCounter"]')
									.each(
											function() {
												this.id = "blankDivCounter" + j;
												$(this).find('label').text(
														"Response " + j);
												$(this).find('table').attr(
														"id",
														"dropDownBlankTable"
																+ j);
												$(this)
														.find(
																'[id ^= "dynamicSelectAddOption"]')
														.attr(
																"id",
																"dynamicSelectAddOption"
																		+ j);
												
												var i=1;
												$(this).find('table tbody tr').each(function(){
													$(this).find('td:nth-child(2) input:first-child').attr('name','response'+j+'option'+i);
													$(this).find('td:nth-child(2) input:first-child').attr('id','response'+j+'option'+i);
													$(this).find('td:nth-child(3) input').attr('name','response'+j+'correctanswer');
													i++;
												});
												
												j++;
											});
						}
					});

	/* adding row to BLANK dropdown table */
	$(document)
			.on(
					'click',
					'[id ^= "dynamicSelectAddOption"]',
					function() {
						var dynamicSelectAddOptionID = this.id[22];
						// alert("working");
						var dynamicSelectAddOptionCounter = parseInt($(
								'#dropDownBlankTable'
										+ dynamicSelectAddOptionID
										+ ' tr:last td:first').text());
						// alert("1");

						dynamicSelectAddOptionCounterPlusOne = dynamicSelectAddOptionCounter + 1;

						$(
								'#dropDownBlankTable'
										+ dynamicSelectAddOptionID + ' tr:last')
								.after(
										'<tr>'
												+ '<td>'
												+ dynamicSelectAddOptionCounterPlusOne
												+ '</td>'
												+ '<td><input type="text" name="response'
												+ dynamicSelectAddOptionID
												+ 'option'
												+ dynamicSelectAddOptionCounterPlusOne
												+ '" class="pickOneOption" id="response'
												+ dynamicSelectAddOptionID
												+ 'option'
												+ dynamicSelectAddOptionCounterPlusOne
												+ '" placeholder="Option '
												+ dynamicSelectAddOptionCounterPlusOne
												+ '" required></td>'
												+ '<td><input type="radio" class="checkManyDropDownBlankAnswer" name="response'
												+ dynamicSelectAddOptionID
												+ 'correctanswer" id="" value="'
												+ dynamicSelectAddOptionCounterPlusOne
												+ '"></td>'
												+ '<td> <i class="fa fa-trash fa-trashBlankDropdown"></i></td>'
												+ '</tr>');

					});

	/* deleting row of BLANK dropdown table */
	$(document).on(
			'click',
			'.fa-trashBlankDropdown',
			function() {
				// $(this).closest('tr').remove();
				var dropZoneTableId = $(this).closest('table')[0].id[18];

				var siblings = $(this).closest('tr').siblings();

				$(this).closest('tr').remove();

				siblings.each(function(index) {
					$(this).children('td').first().text(index + 1);
					/*
					 * $(this).children('td:nth-child(2)
					 * input').attr('id','dropZoneOptionName'+ (index+1) );
					 */
					$(this).find('td:nth-child(2) input').attr(
							'id',
							'response' + dropZoneTableId + 'option'
									+ (index + 1));
					$(this).find('td:nth-child(2) input').attr(
							'name',
							'response' + dropZoneTableId + 'option'
									+ (index + 1));
					$(this).find('td:nth-child(2) input').attr('placeholder',
							'Option ' + (index + 1));

					$(this).find('td:nth-child(3) input').attr('id',
							(index + 1));
					$(this).find('td:nth-child(3) input').attr('value',
							(index + 1));

				});
			});

	$(document).on('click', '[class ^= "blankDropdownClose"]', function() {
		var blankDropdownID = this.className[18];
		$('#blankDivCounter' + blankDropdownID).remove();
		$('#dynamicBlankSelect' + blankDropdownID).remove();

	});

	/* ------DropDown Javascript------- */
	$(document)
			.on(
					'change',
					'[id ^= "dropdownQuestionImage"]',
					function() {
						// alert(this.id);
						var lastNumber = this.id[21];

						$('#previewDropDown' + lastNumber).css(
								'background-image',
								'url(' + $(this).attr("src") + ')');
						$('#previewDropDown' + lastNumber + ' h1').remove();

						$('#dropdownPreviewImageDiv' + lastNumber).css(
								'background-image',
								'url(' + $(this).attr("src") + ')');

						// readURLDropDown(this);
					});

	$(document)
			.on(
					'click',
					'#addAnotherImageDropDown',
					function() {
						dropDownCounter++;
						/*
						 * this lines counts all drop down present while
						 * update.......if there are already 2 drop downs then
						 * counter should increement to 3
						 */
						var dropDownCounterEdit = document
								.getElementsByClassName('containerDropdown');
						if ((dropDownCounterEdit.length > 0)
								&& (!dropDownUpdateBoolean)) {
							// alert('in drop down update condition if');
							dropDownCounter = dropDownCounterEdit.length + 1;
							dropDownUpdateBoolean = true;
						}
						$('#divAddAnotherDropdown')
								.append(
										'<div class="containerDropdown col-md-4" style="padding: 30px;">'
												+ '<div class="row text-right divDropdownClose">'
												+ '<span class="dropdownClose'
												+ dropDownCounter
												+ '"><i class="fa fa-times fa-timesDropDown"></i></span>'
												+ '</div>'
												+ '<div class="row">'
												+ '<div class="col-md-12" style="padding-left: 15px;">'

												+ '<div class="form-group" style="margin-top: 5px;margin-bottom: 10px;">'
												+ '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 text-left " style="padding-left: 0;">'
												+ '<label for="">Select Image/Text: </label>'
												+ '</div>'
												+ '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right" style="padding-right: 0;">'
												+ '<select class="dropDownTypeSelect'
												+ dropDownCounter
												+ '" name="mediaTypeName" id="">'
												+ '<option value="1">Text</option>'
												+ '<option value="2" selected>Image</option>'
												+ '</select>'
												+ '</div>'
												+ '</div>'

												+ '<div class="form-group selectionDropDownImage'
												+ dropDownCounter
												+ '" style="margin-bottom: 5px;">'
												+ '<label class=" col-xs-5 col-sm-5 col-md-5 col-lg-5 text-left" style="padding-left: 0;">Select Dropdown Image<span class="text-danger"> * </span></label>'

												+ '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" id="inputFileType" style="padding: 3px 0px;">'
												+ '<input type="button" class="" id="buttonDropdownMediaAWS'
												+ dropDownCounter
												+ '" value="Choose File" placeholder="background image" onclick="getImages(\'image\',this)" >'
												+ '<input type="text"  value="" placeholder="No file chosen" id="dropdownQuestionImage'
												+ dropDownCounter
												+ '" name="mediaName" src="" required readonly>'
												+ '</div>'
												+ '</div>'
												+ '</div>'
												+ '</div>'
												+ '<div class="row">'
												+ '<div class="col-md-12" id="previewDivDropDown'
												+ dropDownCounter
												+ '" style="padding-left: 15px;">'
												+ '<div class="col-md-12" id="previewDropDown'
												+ dropDownCounter
												+ '">'
												+ '<h1 style="color: #fff;display: table-cell;text-align: center;vertical-align: middle;font-weight: bold;">Preview</h1>'
												+ '</div>'
												+ '</div>'
												+ '</div>'
												+ '<div class="row">'
												+ '<div class="col-md-12" style="padding-left: 15px;">'
												+ '<div class="form-group">'
												+ '<table id="dropdownImageTable'
												+ dropDownCounter
												+ '" class="questionResponse container">'
												+ '<thead>'
												+ '<tr>'
												+ '<th>SrNo</th>'
												+ '<th>Options</th>'
												+ '<th>Correct Option</th>'
												+ '<th>Action</th>'
												+ '</tr>'
												+ '</thead>'
												+ '<tbody>'
												+ '<tr>'
												+ '<td>1</td>'
												+ '<td>'
												+ '<input type="text" name="dropDown'
												+ dropDownCounter
												+ 'Option1'
												+ '" class="pickOneOption" id="optionDropdown1" placeholder="Option 1" required>'
												+ '</td>'
												+ '<td><input type="radio" class="dropDowncheckManyAnswer"  name="dropDown'
												+ dropDownCounter
												+ 'correctanswer'
												+ '" id="" value="1"></td>'
												+ '<td>'
												+ ' <span style="cursor: not-allowed;"><i class="fa fa-trash fa-trashDropDown" style="pointer-events: none;opacity: 0.7;"></i></span>'
												+ '</td>'
												+ '</tr>'
												+ '<tr>'
												+ '<td>2</td>'
												+ '<td>'
												+ '<input type="text" name="dropDown'
												+ dropDownCounter
												+ 'Option2'
												+ '" class="pickOneOption" id="optionDropdown2" placeholder="Option 2" required>'
												+ '</td>'
												+ '<td><input type="radio" class="dropDowncheckManyAnswer"  name="dropDown'
												+ dropDownCounter
												+ 'correctanswer'
												+ '" id="" value="2"></td>'
												+ '<td>'
												+ '<i class="fa fa-trash fa-trashDropDown"></i>'
												+ '</td>'
												+ '</tr>'
												+ '<tr>'
												+ '<td>3</td>'
												+ '<td>'
												+ '<input type="text" name="dropDown'
												+ dropDownCounter
												+ 'Option3'
												+ '" class="pickOneOption" id="optionDropdown3" placeholder="Option 3" required> '
												+ '</td>'
												+ '<td><input type="radio" class="dropDowncheckManyAnswer"  name="dropDown'
												+ dropDownCounter
												+ 'correctanswer'
												+ '" id="" value="3"></td>'
												+ '<td>'
												+ ' <i class="fa fa-trash fa-trashDropDown"></i>'
												+ '</td>'
												+ '</tr>'
												+ '<tr>'
												+ '<td>4</td>'
												+ '<td>'
												+ '<input type="text" name="dropDown'
												+ dropDownCounter
												+ 'Option4'
												+ '" class="pickOneOption" id="optionDropdown4" placeholder="Option 4"  required> '
												+ '</td>'
												+ '<td><input type="radio" class="dropDowncheckManyAnswer"  name="dropDown'
												+ dropDownCounter
												+ 'correctanswer'
												+ '" id="" value="4"></td>'
												+ '<td>'
												+ ' <i class="fa fa-trash fa-trashDropDown"></i>'
												+ '</td>'
												+ '</tr>'
												+ '</tbody>'
												+ '</table>'
												+ '</div>'
												+ '</div>'
												+ '</div>'
												+ '<div class="row">'
												+ '<div class="col-md-12 " style="padding-right: 0;">'
												+ '<span class="pull-right">'
												+ '<button type="button" class="res btn btn-sm" id="dropdownAddOption'
												+ dropDownCounter
												+ '" style="margin-bottom: 5px;">Add Option</button>'
												+ '</span>'
												+ '</div>'
												+ '</div>' + '</div>');

						$('#previewModalDropDown')
								.append(
										'<div id="dropdownPreviewContainerDiv'
												+ dropDownCounter
												+ '">'
												+ '<div id="dropdownPreviewImageDiv'
												+ dropDownCounter
												+ '">'
												+ '</div>'
												+ '<div id="dropdownPreviewSelectDiv'
												+ dropDownCounter
												+ '">'
												+ '<select name="" id="dropdownPreviewImageSelect'
												+ dropDownCounter
												+ '">'
												+ '<option value="">Select Option</option>'
												+ '</select>' + '</div>'
												+ '</div>');

					});

	/* adding row to drop down image table */
	/*
	 * $(document) .on( 'click', '[id ^= "dropdownAddOption"]', function() { var
	 * dropdownAddOptionID = this.id[17]; // alert(dropdownAddOptionID); var
	 * dropdownIamgeCounter = parseInt($( '#dropdownImageTable' +
	 * dropdownAddOptionID + ' tr:last td:first').text()); //
	 * alert(dropdownIamgeCounter);
	 * 
	 * dropdownIamgeCounterPlusOne = dropdownIamgeCounter + 1;
	 *  $( '#dropdownImageTable' + dropdownAddOptionID + ' tr:last') .after( '<tr>' + '<td>' +
	 * dropdownIamgeCounterPlusOne + '</td>' + '<td>' + '<input type="text"
	 * name="option' + dropdownAddOptionID + '" class="pickOneOption" id=""
	 * placeholder="Option ' + dropdownIamgeCounterPlusOne + '" ' + '</td>' + '<td><input
	 * type="radio" name="correctanswer' +dropdownAddOptionID + '" id=""
	 * value="' + dropdownIamgeCounterPlusOne + '"></td>' + '<td>' + ' <i
	 * class="fa fa-trash fa-trashDropDown"></i>' + '</td>' + '</tr>'); });
	 */
	$(document)
			.on(
					'click',
					'[id ^= "dropdownAddOption"]',
					function() {
						var dropdownAddOptionID = this.id[17];
						// alert(dropdownAddOptionID);
						var dropdownIamgeCounter = parseInt($(
								'#dropdownImageTable' + dropdownAddOptionID
										+ ' tr:last td:first').text());
						// alert(dropdownIamgeCounter);

						dropdownIamgeCounterPlusOne = dropdownIamgeCounter + 1;

						$(
								'#dropdownImageTable' + dropdownAddOptionID
										+ ' tr:last')
								.after(
										'<tr>' + '<td>'
												+ dropdownIamgeCounterPlusOne
												+ '</td>'
												+ '<td>'
												/*
												 * + '<input type="text"
												 * name="dropDown'+
												 * dropDownCounter+'"Option"'+dropdownIamgeCounterPlusOne+ '"
												 * class="pickOneOption"
												 * id="optionDropdown'+dropdownIamgeCounterPlusOne+'"
												 * placeholder="Option '+
												 * dropdownIamgeCounterPlusOne+
												 * '">'
												 */
												+ '<input type="text" name="dropDown'
												+ dropdownAddOptionID
												+ 'Option'
												+ dropdownIamgeCounterPlusOne
												+ '" class="pickOneOption" id="optionDropdown'
												+ dropdownIamgeCounterPlusOne
												+ '" placeholder="Option '
												+ dropdownIamgeCounterPlusOne
												+ '" required>'
												+ '</td>'
												/*
												 * + '<td><input type="radio"
												 * name="dropDown'+
												 * dropDownCounter+'correctanswer"
												 * id="" value="'+
												 * dropdownIamgeCounterPlusOne+
												 * '"></td>'
												 */
												+ '<td><input type="radio" name="dropDown'
												+ dropdownAddOptionID
												+ 'correctanswer" id="" value="'
												+ dropdownIamgeCounterPlusOne
												+ '"></td>'
												+ '<td>'
												+ ' <i class="fa fa-trash fa-trashDropDown"></i>'
												+ '</td>' + '</tr>');
					});

	/* deleting row of drop down image table */
	$(document).on(
			'click',
			'.fa-trashDropDown',
			function() {
				// $(this).closest('tr').remove();

				// var dropZoneId =
				// $(this).closest('tr').find('td:first-child').text();
				var dropZoneTableId = $(this).closest('table')[0].id[18];

				var siblings = $(this).closest('tr').siblings();

				$(this).closest('tr').remove();

				siblings.each(function(index) {
					$(this).children('td').first().text(index + 1);
					/*
					 * $(this).children('td:nth-child(2)
					 * input').attr('id','dropZoneOptionName'+ (index+1) );
					 */
					$(this).find('td:nth-child(2) input').attr('id',
							'optionDropdown' + (index + 1));
					$(this).find('td:nth-child(2) input').attr('placeholder',
							'Option ' + (index + 1));
					$(this).find('td:nth-child(2) input').attr(
							'name',
							'dropDown' + dropZoneTableId + 'Option'
									+ (index + 1));

					$(this).find('td:nth-child(3) input').attr('id',
							(index + 1));
					$(this).find('td:nth-child(3) input').attr('value',
							(index + 1));

				});

			});
	/*
	 * $(document).on('click', '.divDropdownClose', function() {
	 * $(this).closest('.containerDropdown').remove(); });
	 */

	$(document)
			.on(
					'click',
					'[class ^= "dropdownClose"]',
					function() {
						var dropDownDivId = this.className[13];

						$(this).closest('.containerDropdown').remove();
						$('#dropdownPreviewContainerDiv' + dropDownDivId)
								.remove();

						/*-----------test-----------*/
						var j = 1, k = 1;

						$('[class ^= "dropdownClose"]')
								.each(
										function() {
											this.className = "dropdownClose"
													+ j;

											var $parentContainer = $(this)
													.closest(
															'.containerDropdown');
											$parentContainer
													.find(
															'[class ^= "dropDownTypeSelect"]')
													.attr(
															'class',
															'dropDownTypeSelect'
																	+ j);
											$parentContainer
													.find(
															'[class ^= "form-group selectionDropDownImage"]')
													.attr(
															'class',
															'form-group selectionDropDownImage'
																	+ j);
											$parentContainer
													.find(
															'[id ^= "buttonDropdownMediaAWS"]')
													.attr(
															'id',
															'buttonDropdownMediaAWS'
																	+ j);
											$parentContainer
													.find(
															'[id ^= "dropdownQuestionImage"]')
													.attr(
															'id',
															'dropdownQuestionImage'
																	+ j);
											$parentContainer
													.find(
															'[id ^= "previewDivDropDown"]')
													.attr(
															'id',
															'previewDivDropDown'
																	+ j);
											$parentContainer
													.find(
															'[id ^= "previewDropDown"]')
													.attr(
															'id',
															'previewDropDown'
																	+ j);
											$parentContainer
													.find(
															'[id ^= "dropdownImageTable"]')
													.attr(
															'id',
															'dropdownImageTable'
																	+ j);

											var i = 1;
											$parentContainer
													.find(
															'[id ^= "dropdownImageTable"] tbody tr')
													.each(
															function() {
																$(this)
																		.find(
																				'td:nth-child(2) input')
																		.attr(
																				'name',
																				'dropDown'
																						+ j
																						+ 'Option'
																						+ i);
																$(this)
																		.find(
																				'td:nth-child(3) input')
																		.attr(
																				'name',
																				'dropDown'
																						+ j
																						+ 'correctanswer');
																i++;
															});

											$parentContainer
													.find(
															'[id ^= "dropdownAddOption"]')
													.attr(
															'id',
															'dropdownAddOption'
																	+ j);

											/*---code for edit input hidden starts---*/
											var bodyIdDropdown = $(this)
													.closest("body").attr("id");
											if (bodyIdDropdown == "editQuestionDashboard") {
												$parentContainer
														.find(
																'[id ^= "dropDownOptionId"]')
														.attr(
																'id',
																'dropDownOptionId'
																		+ j);

											}
											/*---code for edit input hidden ends---*/

											j++;
										});

						$('[id ^= "dropdownPreviewContainerDiv"]')
								.each(
										function() {
											this.id = "dropdownPreviewContainerDiv"
													+ k;

											$(this)
													.find(
															'[id ^= "dropdownPreviewImageDiv"]')
													.attr(
															'id',
															'dropdownPreviewImageDiv'
																	+ k);
											$(this)
													.find(
															'[id ^= "dropdownPreviewSelectDiv"]')
													.attr(
															'id',
															'dropdownPreviewSelectDiv'
																	+ k);
											$(this)
													.find(
															'[id ^= "dropdownPreviewImageSelect"]')
													.attr(
															'id',
															'dropdownPreviewImageSelect'
																	+ k);

											k++;
										});

						dropDownCounter--;
						/*--------test------------*/

					});

	$(document)
			.on(
					'change',
					'.optionTypePickMany',
					function() {
						if ($(this).find(':selected').text() == "Image") {

							$('#tablePickMany tbody tr td:nth-child(2)')
									.each(
											function() {
												var pickManyRowCounter = $(this)
														.closest('tr')
														.find('td:first-child')
														.text();

												$(this)
														.html(
																'<div class="" id="inputFileType">'
																		+ '<input type="button" class="" id="buttonPickManyAWS'
																		+ pickManyRowCounter
																		+ '" value="Choose File" placeholder="background image" onclick="getImages(\'image\',this)" >'
																		+ '<input type="text"  value="" placeholder="No file chosen" id="inputPickMany'
																		+ pickManyRowCounter
																		+ '" name="option" src="" required readonly>'
																		+ '</div>');
											});
							// $('#mediaPreviewPickManyDiv').css('display','block');
							$('[id ^= "answerPreviewPickManyOptionDiv"]')
									.each(
											function() {
												var answerPickManyOptionId = this.id[30];
												// alert(answerPickManyOptionId);
												$(this)
														.html(
																'<div class="" id="answerOptionImageType'
																		+ answerPickManyOptionId
																		+ '">'
																		+ '</div>');
											});

						}
						if ($(this).find(':selected').text() == "Text") {
							$('#tablePickMany tbody tr td:nth-child(2)')
									.each(
											function() {
												var pickManyRowCounter = $(this)
														.closest('tr')
														.find('td:first-child')
														.text();

												$(this)
														.html(
																'<input type="text" name="option" class="pickOneOption" id="inputPickMany'
																		+ pickManyRowCounter
																		+ '" placeholder="Optoin '
																		+ pickManyRowCounter
																		+ '">');
											});

							$('[id ^= "answerPreviewPickManyOptionDiv"]')
									.each(
											function() {
												var answerPickManyOptionId = this.id[30];
												// alert(answerPickManyOptionId);
												$(this)
														.html(
																'<div class="checkbox">'
																		+ '<label><input type="checkbox" value=""><span id="answerPickManyLabelSpan'
																		+ answerPickManyOptionId
																		+ '">Option '
																		+ answerPickManyOptionId
																		+ '</span></label>'
																		+ '</div>');
											});

						}
					});

	$(document)
			.on(
					'click',
					'#addOptionPickMany',
					function() {

						var pickManyCounter = parseInt($(
								'#tablePickMany tr:last td:first').text());
						var pickManyCounterPlusOne = pickManyCounter + 1;
						if (($('.optionTypePickMany').find(':selected').text() == "Image")
								|| ($('#mediaTypeId').val() == "2")) {
							$('#tablePickMany')
									.append(

											'<tr>'
													+ '<td>'
													+ pickManyCounterPlusOne
													+ '</td>'
													+ '<td>'
													+ '<div class="" id="inputFileType">'
													+ '<input type="button" class="" id="buttonPickManyAWS'
													+ pickManyCounterPlusOne
													+ '" value="Choose File" placeholder="background image" onclick="getImages(\'image\',this)" >'
													+ '<input type="text"  value="" placeholder="No file chosen" id="inputPickMany'
													+ pickManyCounterPlusOne
													+ '" name="option" src="" required readonly>'
													+ '</div>'
													+ '</td>'
													+ '<td><input type="checkbox" name="correctanswer" id="'
													+ pickManyCounterPlusOne
													+ '" value="'
													+ pickManyCounterPlusOne
													+ '"></td>'
													+ '<td><i class="fa fa-trash fa-trashPickMany"></i></td>'
													+ '</tr>');

							$('#answerPreviewPickManyDiv')
									.append(
											'<div id="answerPreviewPickManyOptionDiv'
													+ pickManyCounterPlusOne
													+ '">'
													+ '<div class="" id="answerOptionImageType'
													+ pickManyCounterPlusOne
													+ '"></div>' + '</div>');

						}
						if (($('.optionTypePickMany').find(':selected').text() == "Text")
								|| ($('#mediaTypeId').val() == "1")) {
							$('#tablePickMany')
									.append(
											'<tr>'
													+ '<td>'
													+ pickManyCounterPlusOne
													+ '</td>'
													+ '<td>'
													+ '<input type="text" name="option" class="pickOneOption" id="inputPickMany'
													+ pickManyCounterPlusOne
													+ '" placeholder="Option '
													+ pickManyCounterPlusOne
													+ '" required>'
													+ '</td>'
													+ '<td>'
													+ '<input type="checkbox" name="correctanswer" id="'
													+ pickManyCounterPlusOne
													+ '" value="'
													+ pickManyCounterPlusOne
													+ '">'
													+ '</td>'
													+ '<td><i class="fa fa-trash fa-trashPickMany"></i></td>'
													+ '</tr>');

							$('#answerPreviewPickManyDiv')
									.append(
											'<div id="answerPreviewPickManyOptionDiv'
													+ pickManyCounterPlusOne
													+ '">'
													+ '<div class="checkbox">'
													+ '<label><input type="checkbox" value=""><span id="answerPickManyLabelSpan'
													+ pickManyCounterPlusOne
													+ '"></span></label>'
													+ '</div>' + '</div>');
						}

					});

	/* deleting row of Pick many table */
	$(document)
			.on(
					'click',
					'.fa-trashPickMany',
					function() {
						/*
						 * var dropZoneId =
						 * $(this).closest('tr').find('td:first-child').text();
						 * 
						 * $(this).closest('tr').remove();
						 * $('#answerPreviewPickManyOptionDiv'+dropZoneId).remove();
						 */

						var dropZoneId = $(this).closest('tr').find(
								'td:first-child').text();

						var siblings = $(this).closest('tr').siblings();

						$(this).closest('tr').remove();

						siblings
								.each(function(index) {
									$(this).children('td').first().text(
											index + 1);
									/*
									 * $(this).children('td:nth-child(2)
									 * input').attr('id','dropZoneOptionName'+
									 * (index+1) );
									 */

									if ($('.optionTypePickMany').find(
											':selected').text() == "Text") {
										$(this).find('td:nth-child(2) input')
												.attr(
														'id',
														'inputPickMany'
																+ (index + 1));
										$(this)
												.find('td:nth-child(2) input')
												.attr('placeholder',
														'Option ' + (index + 1));
									} else {
										// alert("image serial number");
										$(this).find(
												'td:nth-child(2) input:first')
												.attr(
														'id',
														'buttonPickManyAWS'
																+ (index + 1));
										$(this).find(
												'td:nth-child(2) input:last')
												.attr(
														'id',
														'inputPickMany'
																+ (index + 1));
									}

									$(this).find('td:nth-child(3) input').attr(
											'id', (index + 1));
									$(this).find('td:nth-child(3) input').attr(
											'value', (index + 1));
								});

						$('#answerPreviewPickManyOptionDiv' + dropZoneId)
								.remove();
						if ($('.optionTypePickMany').find(':selected').text() == "Image") {

							$('[id ^= "answerPreviewPickManyOptionDiv"]').each(
									function(index) {
										$(this).attr(
												"id",
												"answerPreviewPickManyOptionDiv"
														+ (index + 1));
										$(this).find('div').attr(
												"id",
												"answerOptionImageType"
														+ (index + 1));
									});

						} else {
							$('[id ^= "answerPreviewPickManyOptionDiv"]').each(
									function(index) {
										$(this).attr(
												"id",
												"answerPreviewPickManyOptionDiv"
														+ (index + 1));
										$(this).find('span').attr(
												"id",
												"answerPickManyLabelSpan"
														+ (index + 1));
									});

						}

					});

	/* Media enabler js */
	$(document).on('change', '#mediaEnableCheckbox', function() {

		if (this.checked) {
			$('#mediaPreviewPickManyDiv').css('display', 'block');

			$('.radioMediaPickManyDiv').css({
				'pointerEvents' : 'auto',
				'opacity' : '1'
			});
			$('.labelTypepickMany').css({
				'pointerEvents' : 'auto',
				'opacity' : '1'
			});
			$('.inputTypeMediaPickManyDiv').css({
				'pointerEvents' : 'auto',
				'opacity' : '1'
			});
			$('#answerPreviewPickManyDiv').css({
				'height' : '46%'
			});

		} else {
			$('#mediaPreviewPickManyDiv').css('display', 'none');
			
			document.getElementById("mediaTypePickMany").value = ""; 
			$('input[name=radioMediaTypePickMany]').prop('checked', false);

			$('.radioMediaPickManyDiv').css({
				'pointerEvents' : 'none',
				'opacity' : '.6'
			});
			$('.labelTypepickMany').css({
				'pointerEvents' : 'none',
				'opacity' : '.6'
			});
			$('.inputTypeMediaPickManyDiv').css({
				'pointerEvents' : 'none',
				'opacity' : '.6'
			});
			$('#answerPreviewPickManyDiv').css({
				'height' : '98%'
			});
		}
	});

	/* Selecting media type */
	$(document)
			.on(
					'change',
					'input[name=radioMediaTypePickMany]',
					function() {
						$('#mediaTypePickMany').val('');
						if (this.value == "videoPickMany") {
							$('#buttonMediaPickManyAWS').attr("onclick",
									"getImages('video',this)");

							$('#mediaPreviewPickManyDiv').css(
									'background-image', 'url("")');

							$('#mediaPreviewPickManyDiv')
									.html(
											'<div class="embed-responsive text-center" style="width: 100%;height: 100%;">'
													+ '<video controls class="embed-responsive-item">'
													+ '<source id="mediaPreviewPickTypeSource" src="" type=video/mp4>'
													+ '</video>' + '</div>');
							gobalMediaType = "video";
						} else if (this.value == "audioPickMany") {
							$('#buttonMediaPickManyAWS').attr("onclick",
									"getImages('audio',this)");

							$('#mediaPreviewPickManyDiv').css(
									'background-image', 'url("")');

							$('#mediaPreviewPickManyDiv')
									.html(
											'<audio controls>'
													+ '<source id="mediaPreviewPickTypeSource" src="" type="audio/mpeg">'
													+
													/*
													 * '<source
													 * id="mediaPreviewPickTypeSource"
													 * src="horse.ogg"
													 * type="audio/ogg">'+
													 */
													'Your browser does not support the audio element.'
													+ '</audio>');

							gobalMediaType = "audio";

						} else if (this.value == "imagePickMany") {
							$('#buttonMediaPickManyAWS').attr("onclick",
									"getImages('image',this)");

							$('#mediaPreviewPickManyDiv').html('');
							gobalMediaType = "image";
						}
					});

	/* media loading into preview */
	$(document)
			.on(
					'change',
					'#mediaTypePickMany',
					function() {
						if (gobalMediaType == "video") {
							// alert("video working media with global if");
							$('#mediaPreviewPickTypeSource').attr('src',
									$(this).attr("src"));
							$("#mediaPreviewPickTypeSource").closest('video')[0]
									.load();
							// alert('video loaded');
						} else if (gobalMediaType == "audio") {
							// alert("audio working media with global if");
							$('#mediaPreviewPickTypeSource').attr('src',
									$(this).attr("src"));
							$("#mediaPreviewPickTypeSource").closest('audio')[0]
									.load();
							// alert('audio loaded');
						} else if (gobalMediaType == "image") {
							$('#mediaPreviewPickManyDiv').css(
									'background-image',
									'url(' + $(this).attr("src") + ')');
						}

						// readURLBackgroundDropBlank(this);
					});

	/* PICK ONE javascript */
	$(document)
			.on(
					'change',
					'.pickOneOptionType',
					function() {
						if ($(this).find(':selected').text() == "Image") {

							$('#tablePickOne tbody tr td:nth-child(2)')
									.each(
											function() {
												var pickOneRowCounter = $(this)
														.closest('tr')
														.find('td:first-child')
														.text();

												$(this)
														.html(
																'<div class="" id="inputFileType">'
																		+ '<input type="button" class="" id="buttonPickOneAWS'
																		+ pickOneRowCounter
																		+ '" value="Choose File" placeholder="background image" onclick="getImages(\'image\',this)" >'
																		+ '<input type="text"  value="" placeholder="No file chosen" id="pickOneOption'
																		+ pickOneRowCounter
																		+ '" name="option" src="" required readonly>'
																		+ '</div>');
											});
							// $('#mediaPreviewPickManyDiv').css('display','block');
							$('[id ^= "answerPreviewPickOneOptionDiv"]')
									.each(
											function() {
												var answerPickOneOptionId = this.id[29];
												// alert(answerPickOneOptionId);
												$(this)
														.html(
																'<div class="" id="answerOptionPickOneImage'
																		+ answerPickOneOptionId
																		+ '">'
																		+ '</div>');
											});
						}
						if ($(this).find(':selected').text() == "Text") {
							$('#tablePickOne tbody tr td:nth-child(2)')
									.each(
											function() {
												var pickOneRowCounter = $(this)
														.closest('tr')
														.find('td:first-child')
														.text();

												$(this)
														.html(
																'<input type="text" name="option" class="pickOneOption" id="pickOneOption'
																		+ pickOneRowCounter
																		+ '" placeholder="Optoin '
																		+ pickOneRowCounter
																		+ '">');
											});

							$('[id ^= "answerPreviewPickOneOptionDiv"]')
									.each(
											function() {
												var answerPickOneOptionId = this.id[29];
												// alert(answerPickManyOptionId);
												$(this)
														.html(
																'<div class="radio">'
																		+ '<label><input type="radio" value="" name="radioPreviewPickOne"><span id="answerPickOneLabelSpan'
																		+ answerPickOneOptionId
																		+ '">Option '
																		+ answerPickOneOptionId
																		+ '</span></label>'
																		+ '</div>');
											});
						}
					});

	$(document)
			.on(
					'click',
					'#pickOneAddOption',
					function() {

						var pickOneCounter = parseInt($(
								'#tablePickOne tr:last td:first').text());
						var pickOneCounterPlusOne = pickOneCounter + 1;

						if (($('.pickOneOptionType').find(':selected').text() == "Image")
								|| ($('#mediaTypeId').val() == "2")) {

							$('#tablePickOne')
									.append(
											'<tr>'
													+ '<td>'
													+ pickOneCounterPlusOne
													+ '</td>'
													+ '<td>'
													+ '<div class="" id="inputFileType">'
													+ '<input type="button" class="" id="buttonPickOneAWS'
													+ pickOneCounterPlusOne
													+ '" value="Choose File" placeholder="background image" onclick="getImages(\'image\',this)">'
													+ '<input type="text" value="" placeholder="No file chosen" id="pickOneOption'
													+ pickOneCounterPlusOne
													+ '" name="option" src="" required readonly>'
													+ '</div>'
													+ '</td>'
													+ '<td><input type="radio" name="correctanswer" required id="" value="'
													+ pickOneCounterPlusOne
													+ '" required></td>'
													+ '<td><i class="fa fa-trash fa-trashPickOne"></i></td>'
													+ '</tr>');

							$('#answerPreviewPickOneDiv')
									.append(
											'<div  id="answerPreviewPickOneOptionDiv'
													+ pickOneCounterPlusOne
													+ '"><div class="" id="answerOptionPickOneImage'
													+ pickOneCounterPlusOne
													+ '"></div></div>');

						}
						if (($('.pickOneOptionType').find(':selected').text() == "Text")
								|| ($('#mediaTypeId').val() == "1")) {

							$('#tablePickOne')
									.append(
											'<tr>'
													+ '<td>'
													+ pickOneCounterPlusOne
													+ '</td>'
													+ '<td>'
													+ '<input type="text" name="option" class="pickOneOption" id="pickOneOption'
													+ pickOneCounterPlusOne
													+ '" placeholder="Option '
													+ pickOneCounterPlusOne
													+ '" required>'
													+ '</td>'
													+ '<td><input type="radio" name="correctanswer" required id="" value="'
													+ pickOneCounterPlusOne
													+ '" required></td>'
													+ '<td><i class="fa fa-trash fa-trashPickOne"></i></td>'
													+ '</tr>');

							$('#answerPreviewPickOneDiv')
									.append(
											'<div id="answerPreviewPickOneOptionDiv'
													+ pickOneCounterPlusOne
													+ '"><div class="radio"><label><input type="radio" name="radioPreviewPickOne" value=""><span id="answerPickOneLabelSpan'
													+ pickOneCounterPlusOne
													+ '">Option '
													+ pickOneCounterPlusOne
													+ '</span></label></div></div>');
						}

					});

	/* deleting row of Pick many table */
	$(document)
			.on(
					'click',
					'.fa-trashPickOne',
					function() {
						/*
						 * var pickOneTrashId =
						 * $(this).closest('tr').find('td:first-child').text();
						 * $(this).closest('tr').remove();
						 * 
						 * $('#answerPreviewPickOneOptionDiv'+pickOneTrashId).remove();
						 */

						var dropZoneId = $(this).closest('tr').find(
								'td:first-child').text();

						var siblings = $(this).closest('tr').siblings();

						$(this).closest('tr').remove();

						siblings
								.each(function(index) {
									$(this).children('td').first().text(
											index + 1);
									/*
									 * $(this).children('td:nth-child(2)
									 * input').attr('id','dropZoneOptionName'+
									 * (index+1) );
									 */

									if ($('.pickOneOptionType').find(
											':selected').text() == "Text") {
										// alert("One selected text");
										$(this).find('td:nth-child(2) input')
												.attr(
														'id',
														'pickOneOption'
																+ (index + 1));
										$(this)
												.find('td:nth-child(2) input')
												.attr('placeholder',
														'Option ' + (index + 1));
									} else {
										// alert("One selected Image");
										$(this).find(
												'td:nth-child(2) input:first')
												.attr(
														'id',
														'buttonPickOneAWS'
																+ (index + 1));
										$(this).find(
												'td:nth-child(2) input:last')
												.attr(
														'id',
														'pickOneOption'
																+ (index + 1));
									}

									$(this).find('td:nth-child(3) input').attr(
											'id', (index + 1));
									$(this).find('td:nth-child(3) input').attr(
											'value', (index + 1));
								});

						$('#answerPreviewPickOneOptionDiv' + dropZoneId)
								.remove();
						if ($('.pickOneOptionType').find(':selected').text() == "Image") {

							$('[id ^= "answerPreviewPickOneOptionDiv"]').each(
									function(index) {
										$(this).attr(
												"id",
												"answerPreviewPickOneOptionDiv"
														+ (index + 1));
										$(this).find('div').attr(
												"id",
												"answerOptionPickOneImage"
														+ (index + 1));
									});

						} else {
							$('[id ^= "answerPreviewPickOneOptionDiv"]').each(
									function(index) {
										$(this).attr(
												"id",
												"answerPreviewPickOneOptionDiv"
														+ (index + 1));
										$(this).find('span').attr(
												"id",
												"answerPickOneLabelSpan"
														+ (index + 1));
									});

						}
					});

	/* PICK ONE Media enabler js */
	$(document).on('change', '#mediaEnableCheckboxPickOne', function() {
		if (this.checked) {
			$('#mediaPreviewPickOneDiv').css('display', 'block');

			$('.radioMediaPickOneDiv').css({
				'pointerEvents' : 'auto',
				'opacity' : '1'
			});
			$('.labelTypepickOne').css({
				'pointerEvents' : 'auto',
				'opacity' : '1'
			});
			$('.inputTypeMediaPickOneDiv').css({
				'pointerEvents' : 'auto',
				'opacity' : '1'
			});
			$('#answerPreviewPickOneDiv').css({
				'height' : '46%'
			});

		} else {
			$('#mediaPreviewPickOneDiv').css('display', 'none');
			
			document.getElementById("mediaTypePickOne").value = ""; 
			$('input[name=radioMediaTypePickOne]').prop('checked', false);


			$('.radioMediaPickOneDiv').css({
				'pointerEvents' : 'none',
				'opacity' : '.6'
			});
			$('.labelTypepickOne').css({
				'pointerEvents' : 'none',
				'opacity' : '.6'
			});
			$('.inputTypeMediaPickOneDiv').css({
				'pointerEvents' : 'none',
				'opacity' : '.6'
			});
			$('#answerPreviewPickOneDiv').css({
				'height' : '98%'
			});
		}
	});

	/* Selecting PICK ONE media type */
	$(document)
			.on(
					'change',
					'input[name=radioMediaTypePickOne]',
					function() {

						$('#mediaTypePickOne').val('');
						if (this.value == "videoPickOne") {
							$('#buttonMediaPickOneAWS').attr("onclick",
									"getImages('video',this)");

							$('#mediaPreviewPickOneDiv').css(
									'background-image', 'url("")');

							$('#mediaPreviewPickOneDiv')
									.html(
											'<div class="embed-responsive text-center" style="width: 100%;height: 100%;">'
													+ '<video controls class="embed-responsive-item">'
													+ '<source id="mediaPreviewPickOneTypeSource" src="" type=video/mp4>'
													+ '</video>' + '</div>');
							globalMediaTypePickOne = "video";
						} else if (this.value == "audioPickOne") {
							$('#buttonMediaPickOneAWS').attr("onclick",
									"getImages('audio',this)");

							$('#mediaPreviewPickOneDiv').css(
									'background-image', 'url("")');

							$('#mediaPreviewPickOneDiv')
									.html(
											'<audio controls>'
													+ '<source id="mediaPreviewPickOneTypeSource" src="" type="audio/mpeg">'
													+
													/*
													 * '<source
													 * id="mediaPreviewPickTypeSource"
													 * src="horse.ogg"
													 * type="audio/ogg">'+
													 */
													'Your browser does not support the audio element.'
													+ '</audio>');

							globalMediaTypePickOne = "audio";

						} else if (this.value == "imagePickOne") {
							$('#buttonMediaPickOneAWS').attr("onclick",
									"getImages('image',this)");

							$('#mediaPreviewPickOneDiv').html('');
							globalMediaTypePickOne = "image";
						}
					});

	/* PICK ONE media loading into preview */
	$(document).on(
			'change',
			'#mediaTypePickOne',
			function() {
				if (globalMediaTypePickOne == "video") {
					// alert("PICK ONE preview video loading");
					$('#mediaPreviewPickOneTypeSource').attr('src',
							$(this).attr("src"));
					$("#mediaPreviewPickOneTypeSource").closest('video')[0]
							.load();
					// alert('video loaded');
				} else if (globalMediaTypePickOne == "audio") {
					// alert("audio working media with global if");
					$('#mediaPreviewPickOneTypeSource').attr('src',
							$(this).attr("src"));
					$("#mediaPreviewPickOneTypeSource").closest('audio')[0]
							.load();
					// alert('audio loaded');
				} else if (globalMediaTypePickOne == "image") {
					$('#mediaPreviewPickOneDiv').css('background-image',
							'url(' + $(this).attr("src") + ')');
				}

				// readURLBackgroundDropBlank(this);
			});

	/* Clearing Media file for Pick Type starts */
	$(document).on('click', '#clearMediaPickType', function() {
		$('#mediaTypePickMany').val('');
		$('#mediaPreviewPickManyDiv').css('display', 'none');
	});
	/* Clearing Media file for Pick Type ends */

	/* Edit question javascript starts */

	$(document)
			.on(
					'click',
					'.questionBankEdit',
					function() {
						var type = $(this).closest('td').prev().text();

						switch (type) {
						case "Drag and Drop":
							// $('#myPreviewModal .dynamicQuestionTypeSelect
							// option:selected').removeAttr('selected',
							// 'selected');
							$('.dynamicQuestionTypeSelect option:eq(1)').attr(
									'selected', 'selected');
							$('.dynamicOptionContent')
									.load(
											'Questions/dynamicOptionContent/dragAndDropOption.html');

							break;
						case "Pick One":
							// $('#myPreviewModal .dynamicQuestionTypeSelect
							// option:selected').removeAttr('selected',
							// 'selected');
							$('.dynamicQuestionTypeSelect option:eq(5)').attr(
									'selected', 'selected');
							$('.dynamicOptionContent')
									.load(
											'Questions/dynamicOptionContent/pickOneOption.html');

							break;
						default:
							// code block
						}
					});

	/* Edit question javascript ends */

	/* Preview question javascript starts */

	$(document).on('click', '.questionBankPreview', function() {
		var type = $(this).closest('td').prev().text();

		switch (type) {
		case "Drag and Drop":

			break;
		case "Pick One":

			break;
		default:
			// code block
		}
	});

	/* Preview question javascript ends */

	/* On Cancel create question starts */
	$(document)
			.on(
					'click',
					'#cancelCreateQuestion , #myCreateQuestionModal .close',
					function() {

						$('.form-horizontal').trigger("reset");

						$('.richText-editor').html('');
						$('.dynamicOptionContent').html('');

						if (selectedValue == 1) {
							// alert("cancel 1 working");
							$('#previewCreateQuestionModal')
									.html(
											'<div class="modal-dialog">'
													+ '<div class="modal-content">'
													+ '<div class="modal-body">'
													+ '<button aria-hidden="true" data-dismiss="modal" class="close previewClose" type="button"><img class="closeImg" src="images/deleteButton.png"></button>'
													+ '<div class="previewContainer" >'
													+
													/*
													 * '<div class="row
													 * previewDivQuestion"
													 * style="">'+ '<div
													 * class="col-xs-1 col-sm-1
													 * col-md-1 col-lg-1"
													 * style="height:
													 * 70px;background-color:
													 * rgba(0,0,0,0.3);">'+ '<label><b>Q:</b><span
													 * class="questionNumberInPreview"></span></label>'+ '</div>'+ '<div
													 * class="col-xs-11
													 * col-sm-11 col-md-11
													 * col-lg-11">'+ '<label>'+ '<span
													 * id="dragAndDropPreviewQuestionText"
													 * style="color: #fff;"></span>'+ '</label>'+ '</div>'+ '</div>'+
													 */
													'<div class="row rows header2 previewDivQuestion">'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="QuestionNumbers">Question <span class="questionNumberInPreview"></span></span>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="remainingTime"><small>Remaining Time: </small></span><span id="timer">XX:XX</span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row" id="questionDataRow">'
													+ '<div class="col-sm-12 col-sm-12 col-md-12 col-lg-12 questionData">'
													+ '<span id="dragAndDropPreviewQuestionText" ></span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row">'
													+ '<div class="previewProportion" style="position: relative;width: 1162px;height: 455px;margin: 0 auto;">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 dropZonePreviewContainer text-center">'
													+ '<div class="" id="dropZonePreviewName1">Drop Zone 1</div>'
													+ '<div class="" id="dropZonePreviewName2">Drop Zone 2</div>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 dragItemPreviewContainer text-center">'
													+ '<div class="" id="dropItemPreviewName1"> Item 1 </div>'
													+ '<div class="" id="dropItemPreviewName2"> Item 2 </div>'
													+ '<div class="" id="dropItemPreviewName3"> Item 3 </div>'
													+ '<div class="" id="dropItemPreviewName4"> Item 4 </div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row ">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right divSubmitPreviewDragAndDrop">'
													+ '<button type="button" class="btn btn-default SubmitTestButton" id="previewSubmitAndNext">SUBMIT <i class="fa fa-angle-double-right"></i></button>'
													+ '</div>' + '</div>'
													+ '</div>' + '</div>'
													+ '</div>');
						}
						if (selectedValue == 2) {
							$('#previewCreateDropDownModal')
									.html(
											'<div class="modal-dialog">'
													+ '<div class="modal-content">'
													+ '<div class="modal-body">'
													+ '<button aria-hidden="true" data-dismiss="modal" class="close previewClose" type="button"><img class="closeImg" src="images/deleteButton.png"></button>'
													+ '<div class="previewContainer" >'

													/*
													 * + '<div class="row
													 * previewDivQuestion"
													 * style="">' + '<div
													 * class="col-xs-1 col-sm-1
													 * col-md-1 col-lg-1"
													 * style="height:
													 * 70px;background-color:
													 * rgba(0,0,0,0.3);">' + '<label><b>Q:</b><span
													 * class="questionNumberInPreview"></span></label>' + '</div>' + '<div
													 * class="col-xs-11
													 * col-sm-11 col-md-11
													 * col-lg-11">' + '<label>' + '<span
													 * id="dropdownPreviewQuestionText"
													 * style="color: #fff;"></span>' + '</label>' + '</div>' + '</div>'
													 */
													+ '<div class="row rows header2 previewDivQuestion">'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="QuestionNumbers">Question <span class="questionNumberInPreview"></span></span>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="remainingTime"><small>Remaining Time: </small></span><span id="timer">XX:XX</span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row" id="questionDataRow">'
													+ '<div class="col-sm-12 col-sm-12 col-md-12 col-lg-12 questionData">'
													+ '<span id="dropdownPreviewQuestionText"></span>'
													+ '</div>'
													+ '</div>'

													+ '<div class="row">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center previewProportion" style="position: relative;width: 1162px;height: 455px;margin: 0 auto;">'
													+ '<div class="col-md-11 " id="previewModalDropDown">'
													+ '<div id="dropdownPreviewContainerDiv1">'
													+ '<div id="dropdownPreviewImageDiv1">'
													+ '</div>'
													+ '<div id="dropdownPreviewSelectDiv1">'
													+ '<select name="" id="dropdownPreviewImageSelect1">'
													+ '</select>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													/*
													 * + '<div class="row ">' + '<div
													 * class="col-xs-12
													 * col-sm-12 col-md-12
													 * col-lg-12 text-right">' + '<button
													 * type="button" class="btn
													 * btn-default"
													 * id="previewSubmitAndNext">SUBMIT
													 * AND NEXT</button>' + '</div>' + '</div>'
													 */
													+ '<div class="row ">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right divSubmitPreviewForAll">'
													+ '<button type="button" class="btn btn-default SubmitTestButton" id="previewSubmitAndNext">SUBMIT <i class="fa fa-angle-double-right"></i></button>'
													+ '</div>' + '</div>'

													+ '</div>' + '</div>'
													+ '</div>');
						}
						if (selectedValue == 3) {
							$('#previewCreateDropBlankModal')
									.html(
											'<div class="modal-dialog">'
													+ '<div class="modal-content">'
													+ '<div class="modal-body">'
													+ '<button aria-hidden="true" data-dismiss="modal" class="closebutton previewClose" type="button">'
													+ '<img class="closeImg" src="images/deleteButton.png">'
													+ '</button>'
													+ '<div class="previewContainer" style="height: 78vh;">'
													+ '<div class="row rows header2 previewDivQuestion">'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="QuestionNumbers">Question <span> </span></span>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="remainingTime"><small>Remaining Time: </small></span><span id="timer">XX:XX</span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row" id="questionDataRow">'
													+ '<div class="col-sm-12 col-sm-12 col-md-12 col-lg-12 questionData">'
													+ '<span id="dropBlankPreviewQuestionText">${question.questionLong}</span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center previewProportion" style="position: relative;width: 1162px;height: 455px;margin: 0 auto;">'
													+ '<div class="col-md-10 " id="previewModalDropBlank">'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row ">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right divSubmitPreviewDragAndDrop">'
													+ '<button type="button" class="btn btn-default SubmitTestButton" id="previewSubmitAndNext">SUBMIT <i class="fa fa-angle-double-right"></i></button>'
													+ '</div>' + '</div>'
													+ '</div>' + '</div>'
													+ '</div>');
							
							$('#paragraphDropDownBlank').prev('.form-group').remove();
							$('#paragraphDropDownBlank').css({
								'borderTop': '1px solid rgb(218, 218, 218)'
							});
							$('.richText-toolbar>button').remove();
							
						}
						if (selectedValue == 4) {
							$('#previewCreateHotSpotModal')
									.html(
											'<div class="modal-dialog">'
													+ '<div class="modal-content">'
													+ '<div class="modal-body">'
													+ '<button aria-hidden="true" data-dismiss="modal" class="close previewClose" type="button"><img class="closeImg" src="images/deleteButton.png"></button>'
													+ '<div class="previewContainer" style="height: 78vh;">'

													+ '<div class="row rows header2 previewDivQuestion">'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="QuestionNumbers">Question <span class="questionNumberInPreview"></span></span>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="remainingTime"><small>Remaining Time: </small></span><span id="timer">XX:XX</span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row" id="questionDataRow">'
													+ '<div class="col-sm-12 col-sm-12 col-md-12 col-lg-12 questionData">'
													+ '<span id="hotSpotPreviewQuestionText" ></span>'
													+ '</div>'
													+ '</div>'

													/*
													 * + '<div class="row
													 * previewDivQuestion"
													 * style="">' + '<div
													 * class="col-xs-1 col-sm-1
													 * col-md-1 col-lg-1"
													 * style="height:
													 * 70px;background-color:
													 * rgba(0,0,0,0.3);">' + '<label><b>Q:</b><span
													 * class="questionNumberInPreview"></span></label>' + '</div>' + '<div
													 * class="col-xs-11
													 * col-sm-11 col-md-11
													 * col-lg-11">' + '<label>' + '<span
													 * id="hotSpotPreviewQuestionText"
													 * style="color: #fff;"></span>' + '</label>' + '</div>' + '</div>'
													 */

													+ '<div class="row">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center previewProportion" style="position: relative;width: 1162px;height: 455px;margin: 0 auto;">'
													+ '<div class="col-md-9 " id="previewModalHotSpot">'
													+ '<div class="" id="hotSpotPreviewRegionName1">Region-1</div>'
													+ '<div class="" id="hotSpotPreviewRegionName2">Region-2</div>'
													+ '<div class="" id="hotSpotPreviewRegionName3">Region-3</div>'
													+ '<div class="" id="hotSpotPreviewRegionName4">Region-4</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'

													+ '<div class="row ">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right divSubmitPreviewDragAndDrop">'
													+ '<button type="button" class="btn btn-default SubmitTestButton" id="previewSubmitAndNext">SUBMIT <i class="fa fa-angle-double-right"></i></button>'
													+ '</div>' + '</div>'

													/*
													 * + '<div class="row ">' + '<div
													 * class="col-xs-12
													 * col-sm-12 col-md-12
													 * col-lg-12 text-right">' + '<button
													 * type="button" class="btn
													 * btn-default"
													 * id="previewSubmitAndNext">SUBMIT
													 * AND NEXT</button>' + '</div>' + '</div>'
													 */

													+ '</div>' + '</div>'
													+ '</div>');
						}
						if (selectedValue == 5) {
							$('#previewCreatePickManyModal')
									.html(
											'<div class="modal-dialog">'
													+ '<div class="modal-content">'
													+ '<div class="modal-body">'
													+ '<button aria-hidden="true" data-dismiss="modal" class="close previewClose" type="button"><img class="closeImg" src="images/deleteButton.png"></button>'
													+ '<div class="previewContainer" >'

													/*
													 * + '<div class="row
													 * previewDivQuestion"
													 * style="">' + '<div
													 * class="col-xs-1 col-sm-1
													 * col-md-1 col-lg-1"
													 * style="height:
													 * 70px;background-color:
													 * rgba(0,0,0,0.3);">' + '<label><b>Q:</b><span
													 * class="questionNumberInPreview"></span></label>' + '</div>' + '<div
													 * class="col-xs-11
													 * col-sm-11 col-md-11
													 * col-lg-11">' + '<label>' + '<span
													 * id="pickManyPreviewQuestionText"
													 * style="color: #fff;"></span>' + '</label>' + '</div>' + '</div>'
													 */
													+ '<div class="row rows header2 previewDivQuestion">'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="QuestionNumbers">Question <span class="questionNumberInPreview"></span></span>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="remainingTime"><small>Remaining Time: </small></span><span id="timer">XX:XX</span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row" id="questionDataRow">'
													+ '<div class="col-sm-12 col-sm-12 col-md-12 col-lg-12 questionData">'
													+ '<span id="pickManyPreviewQuestionText"></span>'
													+ '</div>'
													+ '</div>'

													+ '<div class="row">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center previewProportion">'
													+ '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="mediaPreviewPickManyDiv">'
													+ '<div class="embed-responsive text-center" style="width: 100%;height: 100%;">'
													+ '<video controls class="embed-responsive-item">'
													+ '<source id="mediaPreviewPickTypeSource" src="" type="video/mp4">'
													+ '</video>'
													+ '</div>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" id="answerPreviewPickManyDiv">'
													+ '<div id="answerPreviewPickManyOptionDiv1">'
													+ '<div class="checkbox">'
													+ '<label><input type="checkbox" value=""><span id="answerPickManyLabelSpan1">Option 1</span></label>'
													+ '</div>'
													+ '</div>'
													+ '<div id="answerPreviewPickManyOptionDiv2">'
													+ '<div class="checkbox">'
													+ '<label><input type="checkbox" value=""><span id="answerPickManyLabelSpan2">Option 2</span></label>'
													+ '</div>'
													+ '</div>'
													+ '<div id="answerPreviewPickManyOptionDiv3">'
													+ '<div class="checkbox">'
													+ '<label><input type="checkbox" value=""><span id="answerPickManyLabelSpan3">Option 3</span></label>'
													+ '</div>'
													+ '</div>'
													+ '<div id="answerPreviewPickManyOptionDiv4">'
													+ '<div class="checkbox">'
													+ '<label><input type="checkbox" value=""><span id="answerPickManyLabelSpan4">Option 4</span></label>'
													+ '</div>'
													+ '</div>'

													/*
													 * + '<div
													 * id="answerPreviewPickManyOptionDiv5">' + '<div
													 * class="checkbox">' + '<label><input
													 * type="checkbox" value=""><span
													 * id="answerPickManyLabelSpan5">Option
													 * 5</span></label>' + '</div>' + '</div>'
													 */

													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'

													/*
													 * + '<div class="row " >' + '<div
													 * class="col-xs-12
													 * col-sm-12 col-md-12
													 * col-lg-12 text-right"
													 * id="submitPreviewDiv">' + '<button
													 * type="button" class="btn
													 * btn-default"
													 * id="previewSubmitAndNext">SUBMIT
													 * AND NEXT</button>' + '</div>' + '</div>'
													 */
													+ '<div class="row ">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right divSubmitPreviewForAll">'
													+ '<button type="button" class="btn btn-default SubmitTestButton" id="previewSubmitAndNext">SUBMIT <i class="fa fa-angle-double-right"></i></button>'
													+ '</div>' + '</div>'

													+ '</div>' + '</div>'
													+ '</div>');
						}
						if (selectedValue == 6) {
							$('#previewCreatePickOneModal')
									.html(
											'<div class="modal-dialog">'
													+ '<div class="modal-content">'
													+ '<div class="modal-body">'
													+ '<button aria-hidden="true" data-dismiss="modal" class="close previewClose" type="button"><img class="closeImg" src="images/deleteButton.png"></button>'
													+ '<div class="previewContainer" >'

													/*
													 * + '<div class="row
													 * previewDivQuestion"
													 * style="">' + '<div
													 * class="col-xs-1 col-sm-1
													 * col-md-1 col-lg-1"
													 * style="height:
													 * 70px;background-color:
													 * rgba(0,0,0,0.3);">' + '<label><b>Q:</b><span
													 * class="questionNumberInPreview"></span></label>' + '</div>' + '<div
													 * class="col-xs-11
													 * col-sm-11 col-md-11
													 * col-lg-11">' + '<label>' + '<span
													 * id="pickOnePreviewQuestionText"
													 * style="color: #fff;"></span>' + '</label>' + '</div>' + '</div>'
													 */
													+ '<div class="row rows header2 previewDivQuestion">'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="QuestionNumbers">Question <span class="questionNumberInPreview"></span></span>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">'
													+ '<span class="remainingTime"><small>Remaining Time: </small></span><span id="timer">XX:XX</span>'
													+ '</div>'
													+ '</div>'
													+ '<div class="row" id="questionDataRow">'
													+ '<div class="col-sm-12 col-sm-12 col-md-12 col-lg-12 questionData">'
													+ '<span id="pickOnePreviewQuestionText"></span>'
													+ '</div>'
													+ '</div>'

													+ '<div class="row">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center previewProportion">'
													+ '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" id="mediaPreviewPickOneDiv">'
													+ '<div class="embed-responsive text-center" style="width: 100%;height: 100%;">'
													+ '<video controls class="embed-responsive-item">'
													+ '<source id="mediaPreviewPickOneTypeSource" src="" type="video/mp4">'
													+ '</video>'
													+ '</div>'
													+ '</div>'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" id="answerPreviewPickOneDiv">'
													+ '<div  id="answerPreviewPickOneOptionDiv1">'

													+ '<div class="radio">'
													+ '<label><input type="radio" value="" name="radioPreviewPickOne"><span id="answerPickOneLabelSpan1">Option 1</span></label>'
													+ '</div>'
													+ '</div>'
													+ '<div  id="answerPreviewPickOneOptionDiv2">'
													+ '<div class="radio">'
													+ '<label><input type="radio" value="" name="radioPreviewPickOne"><span id="answerPickOneLabelSpan2">Option 2</span></label>'
													+ '</div>'
													+ '</div>'
													+ '<div  id="answerPreviewPickOneOptionDiv3">'
													+ '<div class="radio">'
													+ '<label><input type="radio" value="" name="radioPreviewPickOne"><span id="answerPickOneLabelSpan3">Option 3</span></label>'
													+ '</div>'
													+ '</div>'
													+ '<div  id="answerPreviewPickOneOptionDiv4">'
													+ '<div class="radio">'
													+ '<label><input type="radio" value="" name="radioPreviewPickOne"><span id="answerPickOneLabelSpan4">Option 4</span></label>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'
													+ '</div>'

													/*
													 * + '<div class="row " >' + '<div
													 * class="col-xs-12
													 * col-sm-12 col-md-12
													 * col-lg-12 text-right"
													 * id="submitPreviewDiv">' + '<button
													 * type="button" class="btn
													 * btn-default"
													 * id="previewSubmitAndNext">SUBMIT
													 * AND NEXT</button>' + '</div>' + '</div>'
													 */
													+ '<div class="row ">'
													+ '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right divSubmitPreviewForAll" id="submitPreviewDiv">'
													+ '<button type="button" class="btn btn-default SubmitTestButton" id="previewSubmitAndNext">SUBMIT <i class="fa fa-angle-double-right"></i></button>'
													+ '</div>' + '</div>'

													+ '</div>' + '</div>'
													+ '</div>');
						}
						$('#backgroundImageInput').trigger('change');

					});
	/* On Cancel create question ends */

});

/* Preview hotspot after selecting image */
function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			/* $('#previewHotSpot').attr('src', e.target.result); */
			$('#previewHotSpot').css('background-image',
					'url(' + e.target.result + ')');
			$('#previewModalHotSpot').css('background-image',
					'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}

/*
 * function addHotSpotRegion() { $('#previewHotSpot').html('<div
 * class="resize-drag draggable" id="hotSpotRegionName1" style="position:
 * absolute;">Region-1</div>' + '<div class="resize-drag draggable"
 * id="hotSpotRegionName2" style="border-radius: 50%;position: absolute;top:
 * 25%;">Region-2</div>' + '<div class="resize-drag draggable"
 * id="hotSpotRegionName3" style="position: absolute;top: 50%;">Region-3</div>' + '<div
 * class="resize-drag draggable pentagon" id="hotSpotRegionName4"
 * style="position: absolute;top: 75%;">Region-4</div>'); }
 */

function readURLDragAndDrop(input) {
	var lastNumber = input.id[13];
	// console.log(lastNumber);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			/* $('#previewHotSpot').attr('src', e.target.result); */
			$('#dropItemName' + lastNumber).css('background-image',
					'url(' + e.target.result + ')');
			/* For Preview */
			$('#dropItemPreviewName' + lastNumber).css('background-image',
					'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function readURLDropDown(input) {
	var lastNumber = input.id[21];
	// console.log(lastNumber);

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			/* $('#previewHotSpot').attr('src', e.target.result); */
			$('#previewDropDown' + lastNumber).css('background-image',
					'url(' + e.target.result + ')');
			$('#previewDropDown' + lastNumber + ' h1').remove();

			$('#dropdownPreviewImageDiv' + lastNumber).css('background-image',
					'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}
/*----------*/

function readURLBackgroundInput(input) {

	// alert($(input).attr('src'));
	$('#previewCreateQuestionModal .modal-body').css('background-image',
			'url(' + $(input).attr("src") + ')');
	/*
	 * if (input.files && input.files[0]) { var reader = new FileReader();
	 * 
	 * reader.onload = function (e) { $('#previewCreateQuestionModal
	 * .modal-body').css('background-image', 'url(' + e.target.result + ')'); }
	 * 
	 * reader.readAsDataURL(input.files[0]); }
	 */
}
/*----------------*/
function readURLBackground(input) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$('#previewCreateQuestionModal .modal-body').css(
					'background-image', 'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function readURLPreviewDragDropItem(input) {

	if (input.files && input.files[0]) {

		var reader = new FileReader();

		reader.onload = function(e) {
			$('#dropItemPreviewName1').css('background-image',
					'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function readURLBackgroundHotSpot(input) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$('#previewCreateHotSpotModal .modal-body').css('background-image',
					'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function readURLBackgroundDropDown(input) {

	/*
	 * if (input.files && input.files[0]) { var reader = new FileReader();
	 * 
	 * reader.onload = function (e) { $('#previewCreateDropDownModal
	 * .modal-body').css('background-image', 'url(' + e.target.result + ')'); }
	 * 
	 * reader.readAsDataURL(input.files[0]); }
	 */
}

function readURLBackgroundPickMany(input) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$('#previewCreatePickManyModal .modal-body').css(
					'background-image', 'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function readURLBackgroundDropBlank(input) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			$('#previewCreateDropBlankModal .modal-body').css(
					'background-image', 'url(' + e.target.result + ')');
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function readURLMediaPickType(input) {

	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {

			/*
			 * var test = e.target.result; if( test *= "image"){
			 */

			// $('#mediaPreviewPickManyDiv').html('');
			$('#mediaPreviewPickManyDiv').css('background-image',
					'url(' + e.target.result + ')');

			// $('#mediaPreviewPickTypeSource').attr('src', e.target.result);

		}

		reader.readAsDataURL(input.files[0]);
	}
}

/*
 * interact('.resize-drag') .draggable({ onmove : window.dragMoveListener,
 * restrict : { restriction : 'parent', elementRect : { top : 0, left : 0,
 * bottom : 1, right : 1 } }, }) .resizable({ // resize from all edges and
 * corners edges : { left : true, right : true, bottom : true, top : true },
 *  // keep the edges inside the parent restrictEdges : { outer : 'parent',
 * endOnly : true, },
 *  // minimum size restrictSize : { min : { width : 40, height : 20 }, },
 * 
 * inertia : true, }) .on( 'resizemove', function(event) { var target =
 * event.target, x = (parseFloat(target .getAttribute('data-x')) || 0), y =
 * (parseFloat(target .getAttribute('data-y')) || 0);
 *  // update the element's style target.style.width = event.rect.width + 'px';
 * target.style.height = event.rect.height + 'px';
 *  // translate when resizing from top or left edges x += event.deltaRect.left;
 * y += event.deltaRect.top;
 * 
 * target.style.webkitTransform = target.style.transform = 'translate(' + x +
 * 'px,' + y + 'px)';
 * 
 * target.setAttribute('data-x', x); target.setAttribute('data-y', y); //
 * target.textContent = Math.round(event.rect.width) + // '\u00D7' +
 * Math.round(event.rect.height); });
 *  // target elements with the "draggable" class
 * interact('.draggable').draggable( { // enable inertial throwing inertia :
 * true, // keep the element within the area of it's parent restrict : {
 * restriction : "parent", endOnly : true, elementRect : { top : 0, left : 0,
 * bottom : 1, right : 1 } }, // enable autoScroll autoScroll : true,
 *  // call this function on every dragmove event onmove : dragMoveListener, //
 * call this function on every dragend event onend : function(event) { var
 * textEl = event.target.querySelector('p');
 * 
 * textEl && (textEl.textContent = 'moved a distance of ' + (Math.sqrt(Math
 * .pow(event.pageX - event.x0, 2) + Math.pow(event.pageY - event.y0, 2) |
 * 0)).toFixed(2) + 'px'); } });
 * 
 * function dragMoveListener(event) { var target = event.target, // keep the
 * dragged position in the data-x/data-y attributes x =
 * (parseFloat(target.getAttribute('data-x')) || 0) + event.dx, y =
 * (parseFloat(target .getAttribute('data-y')) || 0) + event.dy;
 *  // translate the element target.style.webkitTransform =
 * target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
 *  // update the posiion attributes target.setAttribute('data-x', x);
 * target.setAttribute('data-y', y); }
 *  // this is used later in the resizing and gesture demos
 * window.dragMoveListener = dragMoveListener;
 * 
 * =====Drag and Drop=====
 *  // enable draggables to be dropped into this
 * interact('.dropzone').dropzone({ // only accept elements matching this CSS
 * selector
 * 
 * accept: '#yes-drop',
 *  // Require a 75% element overlap for a drop to be possible overlap : 0.50,
 *  // listen for drop related events:
 * 
 * 
 * ondropactivate: function (event) { // add active dropzone feedback
 * event.target.classList.add('drop-active'); }, ondragenter: function (event) {
 * var draggableElement = event.relatedTarget, dropzoneElement = event.target; //
 * feedback the possibility of a drop
 * dropzoneElement.classList.add('drop-target');
 * draggableElement.classList.add('can-drop'); draggableElement.textContent =
 * 'Dragged in'; }, ondragleave: function (event) { // remove the drop feedback
 * style event.target.classList.remove('drop-target');
 * event.relatedTarget.classList.remove('can-drop');
 * event.relatedTarget.textContent = 'Dragged out'; }, ondrop: function (event) {
 * event.relatedTarget.textContent = 'Dropped'; }, ondropdeactivate: function
 * (event) { // remove active dropzone feedback
 * event.target.classList.remove('drop-active');
 * event.target.classList.remove('drop-target'); }
 * 
 * });
 * 
 * interact('.drag-drop').draggable({ inertia : true, restrict : { restriction :
 * "parent", endOnly : true, elementRect : { top : 0, left : 0, bottom : 1,
 * right : 1 } }, autoScroll : true, // dragMoveListener from the dragging demo
 * above onmove : dragMoveListener, });
 * $('#removeBgImage').click(function(event){ event.preventDefault();
 * $("#backgroundImageInput").attr("src","images/Image_LightGrey.jpg");
 * $("#backgroundImageInput").val('Image_LightGrey.jpg');
 * $('#backgroundImageInput').trigger('change');
 * 
 * 
 * });
 */