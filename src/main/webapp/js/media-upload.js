var fileButtonId='';
var mediaType='';
var mediaSrc='';
var mediaName='';
var thisElementTagName;
var optionImageDiv;
var oldItemName='';
var newItemName='';
var globalMediaList='';
var totalSelectedFileForQuestion;
var fullFolderNameGlobal = '';
var previousfullFolderNameGlobal ='';
var fullFolderNameStack = [];
var fullFolderNameCounter = -1;
var copiedItems = [];
var sourcePath = '';
var destinationPath = '';
var copyOrMoveItems = '';
var dashboardMediaSelectPreventClassPresent = false;
var dashboardMediaSelectPreventClassPresentCount = 0;
var renameNameWithoutExtension = '';
var renameNameExtension = '';

//var enterPressed=false;

function getImages(mediaType1,thisElement){
	
	
	//CHECK MODAL OPENS FROM DASHBOARD OR NOT 
	if(dashboardMediaSelectPreventClassPresentCount <1){
		dashboardMediaSelectPreventClassPresentCount++;
		if($(thisElement).hasClass('dashboardMediaSelectPrevent')){
			dashboardMediaSelectPreventClassPresent = true;
			
		}else{
			dashboardMediaSelectPreventClassPresent = false;
			$('#selectFileButton').closest('div').css('display','');
		}
	}
	
	
	//DISPLAY/REMOVE SELECT BUTTON FROM DASHBOARD MODAL
	if(dashboardMediaSelectPreventClassPresent){
		$('#selectFileButton').closest('div').css('display','none');
	}else{
		$('#selectFileButton').closest('div').css('display','');
	}

	if(mediaType1.includes('image')){
		mediaType = 'image'; 
	}else if(mediaType1.includes('audio')){
		mediaType = 'audio';
	}else if(mediaType1.includes('video')){
		mediaType = 'video';
	}
	
	if(fullFolderNameStack.length == 0){
	
		//FOR DEV AND TEST ENV PLEASE MAKE CHANGES HERE
		//FOR DEV ENV KEEP 'dev/Images' OTHERS 'Images'  SAME FOR AUDIO VIDEO
	if(mediaType == 'image'){
			
			fullFolderName = 'Images';
		}else if(mediaType == 'audio'){
			fullFolderName = 'Audio';
		} else if(mediaType == 'video'){
			fullFolderName = 'Videos';
		}
		
		fullFolderNameStack.push(fullFolderName);
		fullFolderNameCounter++;
	}else{
		fullFolderName = fullFolderNameStack[fullFolderNameCounter];
	}
	
	fileButtonId=$(thisElement).next('input').attr('id');

	thisElementTagName=$(thisElement).get(0).tagName;
	optionImageDiv=thisElement;
	
	
	$.ajax({
		type : "GET",
		contentType : "application/json; charset=UTF-8",
		url : "mediaGalleryAmazon.do",
		data : {
				'fullFolderName': fullFolderName
			},
			success : function(result) {

				globalMediaList = result.allMediaList;
				displayContentOnModal(result);			
			},	
			error : function() {
				 sweetAlert('Error while request..');
			}
		});
	
}

/*---------PGC starts-----------*/
function openModal() {
	  document.getElementById('myImageZoomModal').style.display = "block";
	  $('#imageGalleryGrid').css('opacity','.2');
	}

	function closeModal() {
	  document.getElementById('myImageZoomModal').style.display = "none";
	  $('#imageGalleryGrid').css('opacity','1');
	}

	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
	  /* ----------- */
	  var negativeValueForN = slideIndex + n;
	  var excessValueForN = document.getElementsByClassName("mySlides").length;
	  
	  if( (negativeValueForN < 1) || (negativeValueForN > excessValueForN) ){
		  return false;
	  }
	  /* ----------- */
	  showSlides(slideIndex += n);
	}


	function currentSlide(n) {
	  showSlides(slideIndex = n);  
	}

	function showSlides(n) {
	  //console.log('wreker'+ n);
	  var i;
	  var slides = document.getElementsByClassName("mySlides");
	  var dots = document.getElementsByClassName("demo");
	  var captionText = document.getElementById("caption");
	  if (n > slides.length) {
		  //slideIndex = 1
		  /* -------------------- */
		  return false;
		  /* --------------------- */
	  }
	  if (n < 1) {
		  //slideIndex = slides.length
		  /* -------------------- */
		  return false;
		  /* --------------------- */
	  }
	  for (i = 0; i < slides.length; i++) {
	      slides[i].style.display = "none";
	  }
	  for (i = 0; i < dots.length; i++) {
	      dots[i].className = dots[i].className.replace(" active", "");
	  }
	  slides[slideIndex-1].style.display = "block";
	  dots[slideIndex-1].className += " active";
	  captionText.innerHTML = dots[slideIndex-1].alt;
	  
	  
	}

	function plusBeltSlides(n) {
	  /* ----------- */
	  var negativeValueForN = slideIndex + n;
	  var excessValueForN = document.getElementsByClassName("mySlides").length;
	  //console.log("belt:"+ negativeValueForN +' '+ (excessValueForN-2) );
	  if( (negativeValueForN <= 3) ){
		  slideIndex = 3;
		  showBeltSlides( slideIndex );
		  return false;
	  }else if( (negativeValueForN >= (excessValueForN-2)) ){
		  slideIndex = excessValueForN-2;
		  showBeltSlides( slideIndex );
		  return false;
	  }
	  /* ----------- */
	  showBeltSlides(slideIndex += n);
	  //$('.belt').css('left','-850px');
	}

	function showBeltSlides(n) {
		  //alert('showBletSlides'+ n);
		  var i;
		  var slides = document.getElementsByClassName("mySlides");
		  var dots = document.getElementsByClassName("demo");
		  var captionText = document.getElementById("caption");
		  if (n > slides.length) {
			  //slideIndex = 1
			  /* -------------------- */
			  return false;
			  /* --------------------- */
		  }
		  if (n < 1) {
			  //slideIndex = slides.length
			  /* -------------------- */
			  return false;
			  /* --------------------- */
		  }
		  for (i = 0; i < slides.length; i++) {
		      slides[i].style.display = "none";
		  }
		  for (i = 0; i < dots.length; i++) {
		      dots[i].className = dots[i].className.replace(" active", "");
		  }
		  slides[slideIndex-1].style.display = "block";
		  dots[slideIndex-1].className += " active";
		  captionText.innerHTML = dots[slideIndex-1].alt;
		  	  
		}
/*---------PGC ends-----------*/

$(document).ready(function(){
	
	var testingNext = 0 ;

	//RESET INPUT TYPE FILE ON MEDIA MODAL
	$('#mediaModalAWSBucket').on('hidden.bs.modal', function () {	
		 $('#resetInputTypeFileButtonMedia').click();
		 
		 //RESET SELECT BUTTON CHECKING VALUES
		 dashboardMediaSelectPreventClassPresent = false;
		 dashboardMediaSelectPreventClassPresentCount = 0;
		 
		});
	
	$('#createFolderModal').on('hidden.bs.modal', function () {		  
		$('#newFolderName').val('');		
		});
	
	$(document).on('click','#modalCloseButton',function(){
		// sweetAlert("working close");
		$('#myCreateQuestionModal').css({
			'overflowX': 'hidden',
	    	'overflowY': 'auto'
		});
	    
	});
	
	
	//SELECTING FOLDER OR ITEMS
	$(document).on('click','.mediaItem',function(event){
		//alert('working');
		if(  event.ctrlKey ){
			$(this).addClass('selectedAWSItem');
		}else {
			$( ".mediaItem" ).each(function() {
				  $( this ).removeClass("selectedAWSItem");
			});
			$(this).addClass('selectedAWSItem');
		}
	});
	
	
	
	//checking for only one file to be select for upload in a question at a time 
	$(document).on('click','#selectFileButton',function(){
		
		var totalItem = $('.selectedAWSItem').length;
		//alert('totalItem = '+ totalItem);
		if(totalItem == 0){
			sweetAlert('No item choosen');
		}else if(totalItem > 1){
			sweetAlert('Only one item allowed');
		}else if(totalItem == 1){
			if($('.selectedAWSItem').hasClass("folder")){
				$('.selectedAWSItem').trigger('dblclick');
			}else if($('.selectedAWSItem').hasClass("file")){
				if(mediaType == 'image'){
					var imgTag = $('.selectedAWSItem img');
					if(fileButtonId!=null){
						$('#'+fileButtonId).val(imgTag.attr('alt'));
						$('#'+fileButtonId).attr('src',imgTag.attr('src'));
						$('#'+fileButtonId).trigger('change');
						
						$('#'+fileButtonId).attr('value',imgTag.attr('alt'));
					}
					
					if(thisElementTagName == 'DIV'){
						//sweetAlert('this div');
						$(optionImageDiv).css('background-image','url('+imgTag.attr("src")+')');
					}
				}else if(mediaType == 'audio'){
					var audioTag = $('.selectedAWSItem div audio source');
					if(fileButtonId!=null){
						$('#'+fileButtonId).val(audioTag.attr('alt'));
						$('#'+fileButtonId).attr('src',audioTag.attr('src'));
						$('#'+fileButtonId).trigger('change');
						
						//$('#'+fileButtonId).attr('value',imgTag.attr('alt'));
					}
				}else if(mediaType == 'video'){
					var audioTag = $('.selectedAWSItem div video source');
					if(fileButtonId!=null){
						$('#'+fileButtonId).val(audioTag.attr('alt'));
						$('#'+fileButtonId).attr('src',audioTag.attr('src'));
						$('#'+fileButtonId).trigger('change');
						
						//$('#'+fileButtonId).attr('value',imgTag.attr('alt'));
					}
				}
				
				
				
				$('#mediaModalAWSBucket').modal('toggle');
				$('#myCreateQuestionModal').css({
					'overflowX': 'hidden',
			    	'overflowY': 'auto'
				});
				
			}
		}

		 //reset input type file on media modal
		 $('#resetInputTypeFileMedia').click();
		
	});

	//enabling preview media in  PICK ONE at update time
	$('#mediaEnableCheckboxPickOne').trigger('change');
	
	var radioMediaTypePickOne=document.getElementsByName('radioMediaTypePickOne');
	for(var i=0;i<radioMediaTypePickOne.length;i++){
		if(radioMediaTypePickOne[i].checked){
			//sweetAlert($(radioMediaTypePickOne[i]).val());
			$($(radioMediaTypePickOne[i])).trigger('change');
			$('#mediaTypePickOne').trigger('change');
		}
	}//
	
	//enabling preview media in PICK MANY at update time
	$('#mediaEnableCheckbox').trigger('change');
	
	var radioMediaTypePickMany=document.getElementsByName('radioMediaTypePickMany');
	for(var i=0;i<radioMediaTypePickMany.length;i++){
		if(radioMediaTypePickMany[i].checked){
			//sweetAlert($(radioMediaTypePickMany[i]).val());
			$($(radioMediaTypePickMany[i])).trigger('change');
			$('#mediaTypePickMany').trigger('change');
		}
	}
	
	
	
	/*rename media*/ 
	$(document).on('click','.renameItem',function() {
		//alert("hello");
		var length = $('.selectedAWSItem').length;
		if(length == 1){
			$('.selectedAWSItem div input').addClass(' selectedToRename');
			$('.selectedToRename').each(function(){
				oldItemName = $(this).val();
				
			});
			
			//MAKE SELECTED THE IEM NAME
			
			var input = '';
			if($('.selectedToRename').val().includes('.')){
				renameNameWithoutExtension = $('.selectedToRename').val().split('.').shift();
				renameNameExtension = $('.selectedToRename').val().split('.').pop();
				$('.selectedToRename').val(renameNameWithoutExtension);
			}else{
				renameNameWithoutExtension = $('.selectedToRename').val().split('.').shift();
				$('.selectedToRename').val(renameNameWithoutExtension);
			}
			
			$('.selectedToRename').prop('readonly', false);
			$('.selectedToRename').css('background','white');
			
			$('.selectedToRename').select();
			
			
		}else {
			sweetAlert("select only one item !!!");
		}
	});
	
	
	
	 $(document).on('keyup','.selectedToRename',function(e) {
			//e.preventDefault();
		
			if(e.key == 'Enter') {
				
				$(this).trigger('blur');
			}        
			
		});
	 
	 
	 //IF CLICKED OUTSIDE AFTER RENAME 
	 $(document).on('blur','.selectedToRename',function(e) {
		 
		 checkForRename();
				
		});
	 
	 
	 function checkForRename(){
		 var itemType = '';
			
	        
	        //alert('newItemName =='+newItemName.trim());
	       
	        if(oldItemName.includes('.')){
	        	//alert('1');
	        	newItemName = $('.selectedToRename').val()+"."+renameNameExtension;
	        	itemType = 'file';
	        	oldExtension = oldItemName.substring(oldItemName.lastIndexOf('.')+1,oldItemName.length);
		        newExtension = newItemName.substring(newItemName.lastIndexOf('.')+1,newItemName.length);
		        if(oldExtension != newExtension){
		        	sweetAlert("Extension cannot be changed !!!");
		        	return false;
		        }
	        }else{
	        	newItemName = $('.selectedToRename').val();
	        	itemType = 'folder';
	        }
	        
     	if(oldItemName != newItemName){
     		if(itemType == 'folder'){
     			if(!containsInvalidCharacterInFolderName(newItemName)){
	        			renameItem(oldItemName,newItemName,fullFolderNameStack[fullFolderNameCounter]);
	        		}else{
	        			sweetAlert("name can not contain  '/' , ':' , '*' , '?' , '\"' , '<' , '>' , '|'  , '\\' ,  '.' ");
	        			return false;
	        		}
     		}else{
     			//alert('2');
     			if(!containsInvalidCharacter(newItemName)){
     				//alert('3');
	        			renameItem(oldItemName,newItemName,fullFolderNameStack[fullFolderNameCounter]);
	        		}else{
	        			sweetAlert("name can not contain  '/' , ':' , '*' , '?' , '\"' , '<' , '>' , '|' ,  '\\'  ");
	        			return false;
	        		}
     		}
     	} else {
     		
     		$('.selectedToRename').val(oldItemName);
     		$('.selectedToRename').css('background','transparent');
				$('.selectedToRename').prop('readonly', true);
				
     		//sweetAlert("old name and new name same !!!");
     	}
           
	 }
	 
	 
	
	 //CREATE NEW FOLDER
	$(document).on('click','#createFolderSave',function(){
		//alert('fullFolderNameCounter =='+ fullFolderNameCounter);
		var currentFolderPath = fullFolderNameStack[fullFolderNameCounter];
		//alert('currentFolderPath == '+ currentFolderPath);
		var newFolderName = $('#newFolderName').val().trim();
		//alert('newFolderName=='+ newFolderName);
		if((newFolderName == null) || (newFolderName.length == 0)){
			var errorMessage = "please enter folder name";
	    	$("#emptyFolderErrorMessage").html("<b>" + errorMessage + " </b>");
			$('#emptyFolderNameErrorModal').modal("toggle");
			return false;
		}
		if(containsInvalidCharacterInFolderName(newFolderName)){
			//sweetAlert("name can not contain  '/' , ':' , '*' , '?' , '\"' , '<' , '>' , '|' ,  , '\\' , '.'");
			var errorMessage = "name can not contain  '/' , ':' , '*' , '?' , '\"' , '<' , '>' , '|' ,  '\\' ,  '.'";
	    	$("#emptyFolderErrorMessage").html("<b>" + errorMessage + " </b>");
			$('#emptyFolderNameErrorModal').modal("toggle");
			return false;
		}
		
		
		
		$.ajax({
    		type : "GET",
    		url : "createFolder.do",
    		data: {
    			'fullFolderName' : currentFolderPath,
    			'newFolderName' : newFolderName
    		},
    		success : function(result) {
    			
    			if(result.folderAlreadyExist){
					
					$("#emptyFolderErrorMessage").html("<b> Folder already exist !!!</b>" );
    				$('#emptyFolderNameErrorModal').modal("show");
				}else if(result.folderNotCreated){
				
					$("#emptyFolderErrorMessage").html("<b> Problrm while creating Folder !!!</b>" );
    				$('#emptyFolderNameErrorModal').modal("show");
				}else if(result.folderCreated){
				
					$("#emptyFolderErrorMessage").html("<b> Folder Created Successfully !!!</b>" );
    				$('#emptyFolderNameErrorModal').modal("show");
					$('#createFolderModal').modal('hide');
					$('#newFolderName').val('');
					displayContentOnModal(result.jsonResult);
				}
    
    		},	
    		error : function() {
    			
    			sweetAlert('Error while creating folder..');
    			}
    		});
		
	});
	

	$(document).on('dblclick','.folder',function(){		
		
		for(var i = fullFolderNameCounter+1 ; i<fullFolderNameStack.length ;){
				fullFolderNameStack.splice(i, 1);
		}		
		var folderName=$(this).find('div input').val();
		var completePath = fullFolderNameStack[fullFolderNameCounter] + '/' + folderName;
		
		fullFolderNameCounter++;
		if(fullFolderNameStack.indexOf(completePath) < 0){
			
			fullFolderNameStack.push(completePath);
		}
				
		//DISPLAY/REMOVE SELECT BUTTON FROM DASHBOARD MODAL
		if(dashboardMediaSelectPreventClassPresent){
			$('#selectFileButton').closest('div').css('display','none');
		}else{
			$('#selectFileButton').closest('div').css('display','');
		}
		
		getImages(mediaType,this);
	});
	
	$(document).on('click','#backButton',function(){
		
		if(fullFolderNameCounter > 0){
			fullFolderNameCounter--;
			
			getImages(mediaType,this);
		}
		
	});
	
	$(document).on('click','#nextButton',function(){
		
		if(fullFolderNameCounter < (fullFolderNameStack.length-1)){
			
			fullFolderNameCounter++;
			
			getImages(mediaType,this);
		}
		
	});
	
	
	/*FILE/FOLDER CUT COPY PASTE*/
	$(document).on('click','.selectItemToCopy',function(){
		selectItems('copy');
	});
	
	$(document).on('click','.selectItemToMove',function(){
		selectItems('move');
	});
	
	$(document).on('click','.pasteItems',function(){
		destinationPath = fullFolderNameStack[fullFolderNameCounter];
		pasteItems();
	});
	
	
	 $(document).keydown(function(event) {
	      // Ctrl+C or Cmd+C pressed?
	      if ((event.ctrlKey || event.metaKey) && event.keyCode == 67) {
	         //alert('copied');
	    	  selectItems('copy');
	      }

	      // Ctrl+V or Cmd+V pressed?
	      if ((event.ctrlKey || event.metaKey) && event.keyCode == 86) {
	    	  destinationPath = fullFolderNameStack[fullFolderNameCounter];
	    	  if(copyOrMoveItems == 'move'){
	    		  for(var i = 0;i<copiedItems.length ; i++){
	    			  if(sourcePath+"/"+copiedItems[i] == destinationPath){
	    				  sweetAlert("destination folder is sub folder of source folder !!!");
	    				  copiedItems.splice(i,1); 	
	    			  }
	    		  }
    		  }
	    	  
	    	  if((sourcePath != destinationPath)  &&  (copiedItems.length > 0)){
	    		  
	    		  //PASTE THE ITEMS
	    		  pasteItems();
	    		 
	    	  }
	      }

	      // Ctrl+X or Cmd+X pressed?
	      if ((event.ctrlKey || event.metaKey) && event.keyCode == 88) {
	         // Do stuff.
	    	  //alert('cut');
	    	  selectItems('move');
	      } 
	    });
	 
	 
	 function selectItems(action){
		 	copiedItems.splice(0,copiedItems.length);
	   	  sourcePath = '';
	   	  destinationPath = '';
	   	  
	   	  
	        $(".selectedAWSItem").each(function() {
				var item = $(this).find('div input').val();
				if(item){
					copiedItems.push(item.trim());
				}
	        });
	        
	        sourcePath = fullFolderNameStack[fullFolderNameCounter];
	        copyOrMoveItems = action;
	 }
	 
	 function pasteItems(){
		 if(copyOrMoveItems == 'copy'){
			  copyItems();
		  }else if(copyOrMoveItems == 'move'){
			  copyOrMoveItems = '';
			  moveItems();
		  }
	 }
	 
	 
	 
	 $(document).on('click','.navigationLink',function(){
		 //alert('navigationLink working');
			var link = $(this).attr('href');
			fullFolderNameCounter = fullFolderNameStack.indexOf(link);
			getImages(mediaType,this);
		});
	 
	
});

//upload media
function uploadImages(){
	
	 var fd = new FormData();
	 var formDataLength=0;
    var file_data = $('input[type="file"]')[0].files; // for multiple files
    //sweetAlert("length==" +file_data.length );
    var notValidFile = '';
    var filesGreaterThan15MB = '';
   
    for(var i = 0;i<file_data.length;i++){ 
        //sweetAlert("file data=="+(Math.ceil(file_data[i].size/(1024*1024))) + ' MB');
        var fileName=file_data[i].name;
        
        if(Math.ceil(file_data[i].size/(1024*1024)) > 15){
        	//sweetAlert(file_data[i].name +" is greater than 15MB");
        	if(filesGreaterThan15MB.length > 0){
        		filesGreaterThan15MB += ', ' + file_data[i].name;
    		}else {
    			filesGreaterThan15MB += file_data[i].name;
    		}
        }else {
	        if(mediaType == 'image'){
	        	var fileSize = file_data[i].size
	        	if(isImage(file_data[i].name) ){
	        		fd.append("file", file_data[i]);
	        		formDataLength++;
	        	}else{
	        		//sweetAlert(file_data[i].name +'  is not an image file');
	        		if(notValidFile.length > 0){
	        			notValidFile += ', ' + file_data[i].name;
	        		}else {
	        			notValidFile += file_data[i].name;
	        		}
	        		
	        	}
	        }else if(mediaType == 'audio'){
	        	if(isAudio(file_data[i].name)){
	        		fd.append("file", file_data[i]);
	        		formDataLength++;
	        	}else{
	        		//sweetAlert(file_data[i].name +'  is not an audio file');
	        		if(notValidFile.length > 0){
	        			notValidFile += ', ' + file_data[i].name;
	        		}else {
	        			notValidFile += file_data[i].name;
	        		}
	        	}
	        }else if(mediaType == 'video'){
	        	if(isVideo(file_data[i].name)){
	        		fd.append("file", file_data[i]);
	        		formDataLength++;
	        	}else{
	        		//sweetAlert(file_data[i].name +'  is not an video file');
	        		if(notValidFile.length > 0){
	        			notValidFile += ', ' + file_data[i].name;
	        		}else {
	        			notValidFile += file_data[i].name;
	        		}
	        	}
	        }
        }
    }
    
    if(filesGreaterThan15MB.length>0){
    	var errorMessage=filesGreaterThan15MB + " <br>is/are greater than 15MB";
    	
    	$("#mid3").html("<b>" + errorMessage + " </b>");
		$('#filesGreaterThan15MBModal').modal("toggle");
    }
    
  
    if(notValidFile.length>0){
    	var errorMessage=notValidFile + " <br>is/are not a valid format";
    	
    	$("#mid1").html("<b>" + errorMessage + " </b>");
		$('#notValidFormatModal').modal("toggle");
    }
    
    if(formDataLength>0){
    	
    	
    	var other_data = $('form').serializeArray();
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });
        fd.append('fullFolderName',fullFolderNameStack[fullFolderNameCounter]);
        
    	$.ajax({
    		type : "POST",
    		contentType : false ,
    		processData: false,
    		url : "urlMediaUpload.do",
    		cache : false,
    		async: false,
    		data: fd,
    		success : function(result) {
    			if(result.fileNotUploadedList.length > 0){
    				$("#emptyFolderErrorMessage").html("<b> Following item/s are already exist :\n" +
    						""+ result.fileNotUploadedList +"</b>");
    				$('#emptyFolderNameErrorModal').modal("show");
    			}
    			
    			if(result.totalFileUploaded > 0){
    				if(mediaType == 'image'){
        				sweetAlert(result.totalFileUploaded + " Image/s uploaded Successfully");
        			}else if(mediaType == 'audio'){
        				sweetAlert(result.totalFileUploaded + " Audio/s uploaded Successfully");
        			}else if(mediaType == 'video'){
        				sweetAlert(result.totalFileUploaded + " Video/s uploaded Successfully");
        			}
    			}
    			
    			
    			displayContentOnModal(result);
    		},	
    		error : function() {
    			
    			sweetAlert('Error while uploading..');
    			}
    		});
    }else {
    	
    	$("#mid2").html("<b>No files to upload </b>");
		$('#noFileToUploadModal').modal("toggle");
	
    }
    
    $('#resetInputTypeFileMedia').click();
}

//delet media
function deleteImages(){
	
	var fd = new FormData();
	var values = [];
	
	$(".selectedAWSItem").each(function() {
		
			var item = $(this).find('div input').val();
			
			if(item){
				values.push(item.trim());
			}
	});
	if(values.length > 0){
		
		$.ajax({
			type : "GET",
			contentType : "application/json; charset=UTF-8" ,
			url : "deleteMedia.do",
			cache : false,
			async: false,
			data:{
				
				fullFolderName:fullFolderNameStack[fullFolderNameCounter],
				values : JSON.stringify(values)
			},
			success : function(result) {
				sweetAlert('item deleted ..');
	 			displayContentOnModal(result);
			},	
			error : function() {
				
				sweetAlert('Error while deleting..');
			}	
			});
	}else {
		$("#emptyFolderErrorMessage").html("<b>select file </b>");
		$('#emptyFolderNameErrorModal').modal("show");
	}
	
}


//displaying media on modal 
function displayContentOnModal(result){
	//alert('called again');
	var testingNext = 0;
	
	/*---------PGC starts-----------*/
	var beltLeft,uSlideBeltLeft;
	var slidesNumber = result.allMediaList.length;
	//alert("1.slidesNumber again"+slidesNumber);
	
	var activeLeftFirstBelt,activeLeftLastBelt,firstSlide,lastSlide,n,beltWidth ;
	beltWidth = slidesNumber * 170;
	var backFromLastSlide = beltWidth-850 ;	
	//alert('2.beltwidth: '+beltWidth+'\nbackFromLastSlide'+backFromLastSlide);
	
	
	$(document).on('click','.prevBelt',function(){
		
		beltLeft = parseInt( $('.belt').css('left') );
		//alert("prev belt: "+beltLeft);
		if(beltLeft > -850){
			//alert("5 blet");
			$('.belt').css('left','0px');
			$('.belt .column img.active').removeClass('active');
			$('#thirdSlide').addClass('active');
			
			$('.mySlides').each(function(){
				$(this).css('display','none');
			});
			$('.mySlides:nth-child(3)').css('display','block');
			
			return false;
		}
		beltLeft = beltLeft +  850;
		$('.belt').css('left',beltLeft+'px');
		
		

	});
	
	$(document).on('click','.nextBelt',function(){
		
		beltLeft = parseInt( $('.belt').css('left') );
		//alert("next belt: "+beltLeft);
		var firstSlideLeft = -(slidesNumber * 170) + 1700;
		//alert("firstSlideLeft"+ firstSlideLeft);
		if(beltLeft < firstSlideLeft){
			//alert("5 blet last");
			
			$('.belt').css('left','-'+backFromLastSlide+'px');
			
			return false;
		}
		beltLeft = beltLeft -  850;
		$('.belt').css('left',beltLeft+'px');
		
	});
	
	$(document).on('click','.prev',function(){
		var slideBeltActive = $('.belt .column img.active')
		
		firstSlide = parseInt( $('.belt').css('left') );			
		//alert("slide number prev:"+slidesNumber);
		//console.log(Math.abs(firstSlide) + ' ' +backFromLastSlide);
		if( firstSlide == 0 ){				
			return false;
		}else if( $('#lastSlide').hasClass('active') || $('#secondLastSlide').hasClass('active') || $('#thirdLastSlide').hasClass('active') ){				
			//alert('last slide to prev');
			return false;
		}
		
		var currentLeftForPrev = $('.belt').css('left');			
		currentLeftForPrev =  parseInt(currentLeftForPrev) + 170 ;						
		$('.belt').css('left',currentLeftForPrev +'px');
		
		$('.prev').css('pointer-events','none');
		setTimeout(function(){
			//alert("timeout wokring");
			$('.prev').css('pointer-events','auto');
			}, 500);
		
	});
	$(document).on('click','.next',function(){			
		testingNext++;
		//alert('testingNext'+testingNext);
		
		firstSlide = parseInt( $('.belt').css('left') );				
		var firstSlideLeft = (slidesNumber * 170) - 850;
		
		//alert("slide number next"+testingNext+":"+slidesNumber+"\nfirstSlide: "+Math.abs(firstSlide) + "\nfirstSlideLeft:"+Math.abs(firstSlideLeft));
		
		if( Math.abs(firstSlide) == Math.abs(firstSlideLeft) ){	
			//alert("Inside firstSlide: "+Math.abs(firstSlide) + "\nfirstSlideLeft:"+Math.abs(firstSlideLeft));			
			return false;			
		}else if( $('#firstSlide').hasClass('active') || $('#secondSlide').hasClass('active') || $('#thirdSlide').hasClass('active') ){			
			return false;
		}
		
		var currentLeftForNext = $('.belt').css('left');			
		currentLeftForNext =  parseInt(currentLeftForNext) - 170 ;			
		$('.belt').css('left',currentLeftForNext +'px');
		
		$('.next').css('pointer-events','none');
		setTimeout(function(){
			//alert("timeout wokring");
			$('.next').css('pointer-events','auto');
			}, 500);
					
	});
	
	$(document).on('dblclick','#imageGalleryGrid .column img',function(){
		   /*------------------------------- */
		  n = parseInt(this.id);			  
		  firstSlide = n-3;
		  lastSlide = n+2;
		  //alert('firstSlide'+ firstSlide + '\nLastSlide'+lastSlide);
		  if( firstSlide <= 0 ){
			  //alert("first or second slide");
			  $('.belt').css('left','0px');
			  return false;
		  }else if(lastSlide >= slidesNumber){
			  //alert("first or second last slide");
			  $('.belt').css('left','-'+backFromLastSlide+'px');
			  return false;
		  }
		  
		  activeLeftFirstBelt = firstSlide*170 ;
		  activeLeftLastBelt = lastSlide*170 ;
		  //alert('firstSlide'+ activeLeftFirstBelt + '\nLastSlide'+activeLeftLastBelt);
		  $('.belt').css('left','-'+activeLeftFirstBelt+'px');
		  
		   /*-------------------------------  */
	});
	
	/*---------PGC ends-----------*/
	
	var allMediaList = result.allMediaList;
	var allFolderList = result.allFolderList;
	
	$('#amazonImagesModalBody').html('<div class="row" id="imageGalleryGrid">'+	
										
										'</div>'+						
										'<div id="myImageZoomModal" class="modal">'+
										  '<span class="close cursor" onclick="closeModal()"><img src="images/galleryViewAwsClose.png"></span>'+
										  '<div class="modal-content" style="overflow: hidden;border: 0;">'+
										  	'<div class="mySlidesContainer">'+
										  											  												  	
										  	'</div>'+										
										    '<a class="prev" onclick="plusSlides(-1)">&#10094;</a>'+
										    '<a class="next" onclick="plusSlides(1)">&#10095;</a>'+										
										    '<div class="caption-container">'+
										      '<p id="caption"></p>'+
										    '</div>'+																														   										    
										   '<div class="belt-container" style="width: 100%;height:25%;position: relative;">'+ 
											    '<div class="belt" style="height: 100%;">'+
											    	
											    '</div>'+
											    
											    '<a class="prevBelt" onclick="plusBeltSlides(-5);">&#10094;</a>'+
										    	'<a class="nextBelt" onclick="plusBeltSlides(5);">&#10095;</a>'+
										   '</div>'+
										    
										  '</div>'+
										'</div>');
	
	var htmlValueImageFolder ="<div class='gallery mediaItem folder col-md-2' id='selectThisMediaForAction'  >";		
	var htmlValueImageFile ="<div class='gallery mediaItem file col-md-2' id='selectThisMediaForAction'  >";
	var htmlValueAudioFolder ="<div class='gallery mediaItem folder aud col-md-4' id='selectThisMediaForAction' >";	
	var htmlValueAudioFile ="<div class='gallery mediaItem file aud col-md-4' id='selectThisMediaForAction' >";	
	var htmlValueVideoFolder ="<div class='gallery mediaItem folder col-md-4' id='selectThisMediaForAction' style='height:280px;' >";	
	var htmlValueVideoFile ="<div class='gallery mediaItem file col-md-4' id='selectThisMediaForAction' style='height:280px;' >";	
	$('#totalMediaItmes').html(allMediaList.length+allFolderList.length+' items');
	
	if(mediaType == 'image'){
		
		for(var i=0;i<allFolderList.length;i++){
			var imageData = htmlValueImageFolder + "<div class='imageContainerDiv '><img src='images/folderAwsExplorerView.png' alt='"+allFolderList[i].folderName + "' style='width:100%;height:100%;'>";
			imageData = imageData + " <div class='desc '> <input type='text' class='itemName' value='"+ allFolderList[i].folderName +"'  style='width : 100%; text-align : center;' onkeydown='if (event.keyCode == 13) {return false;}' readonly></div></div></div>";
			
			/*$("#amazonImagesModalBody").append(imageData);*/
			$("#imageGalleryGrid").append(imageData);
		}
		for(var i=0; i < allMediaList.length; i++){
			/*var imageData = htmlValueImageFile + "<div class='imageContainerDiv '><img src='"+allMediaList[i].absolutePath +"' alt='"+allMediaList[i].fileName+"' style='width:100%;height:100%;'>";
			imageData = imageData + "<div class='desc '> <input type='text' class='itemName' value='"+ allMediaList[i].fileName +"' style='width : 100%; text-align : center;' onkeydown='if (event.keyCode == 13) {return false;}'></div></div></div>";
			
			$("#amazonImagesModalBody").append(imageData);*/					
			
			$('#imageGalleryGrid').append('<div class="column mediaItem file col-md-2">'+
				    '<img src="'+ allMediaList[i].absolutePath +'" alt="'+ allMediaList[i].fileName +'" style="width: 100%; cursor: default;" ondblclick="openModal();currentSlide('+ ( parseInt(i)+1 ) +')" class="hover-shadow cursor" id="'+ ( parseInt(i)+1) +'">'+
					'<div class="desc"> <input type="text" class="itemName" value="'+ allMediaList[i].fileName +'"  style="width : 100%; text-align : center;" onkeydown="if (event.keyCode == 13) {return false;}" readonly></div></div>');
			
			$('.mySlidesContainer').append('<div class="mySlides">'+
				      '<div class="numbertext">'+ ( parseInt(i)+1 ) +'</div>'+
				      '<img src="'+ allMediaList[i].absolutePath +'" style="width:100%">'+
				      '</div>');
			
			$('.belt').append('<div class="column">'+
					    '<img class="demo cursor" src="'+ allMediaList[i].absolutePath +'" style="width:100%" onclick="currentSlide('+ (i+1) +')" alt="'+ allMediaList[i].fileName +'" >'+
					    '</div>');
			
		}
		
		/*---------PGC starts-----------*/
		beltLeft = parseInt( $('.belt').css('left') );
		uSlideBeltLeft = parseInt( $('.belt').css('left') );
		
		$('.belt .column').first().find('img').attr('id','firstSlide');
		$('.belt .column').last().find('img').attr('id','lastSlide');
		
		$('.belt .column:nth-child(2)').find('img').attr('id','secondSlide');
		$('.belt .column:nth-child(3)').find('img').attr('id','thirdSlide');
		
		$('.belt .column:nth-last-child(2)').find('img').attr('id','secondLastSlide');
		$('.belt .column:nth-last-child(3)').find('img').attr('id','thirdLastSlide');
		
		$('.belt').css('width',beltWidth+'px');
		/*----------PGC ends-------------*/
		
		
	} if(mediaType == 'audio'){
		
		for(var i=0;i<allFolderList.length;i++){
			var audiodata = htmlValueAudioFolder + "<div class=' audioContainer '><img src='images/windowFolder.jpg' alt='"+allFolderList[i].folderName + "' style='width:45%;height:100%;'>";
			audiodata = audiodata + " <div class='desc '> <input type='text' class='itemName' value='"+ allFolderList[i].folderName +"'  style='width : 100%; text-align : center;' onkeydown='if (event.keyCode == 13) {return false;}'></div></div></div>";
			//sweetAlert("image tag====" + imageData);
			$("#amazonImagesModalBody").append(audiodata);
		}
		
		for(var i=0;i<allMediaList.length;i++){
			var audiodata = htmlValueAudioFile + "<div class=' audioContainer'><audio controls><source src='" +allMediaList[i].absolutePath +"' type='audio/mpeg' alt='"+allMediaList[i].fileName+"'>";
			audiodata = audiodata + "<source src='" +allMediaList[i].absolutePath +"' type='audio/wav'>";
			audiodata = audiodata + "<source src='" +allMediaList[i].absolutePath +"' type='audio/ogg'></audio>";
			audiodata = audiodata + " <div class='desc '> <input type='text' class='itemName' value='"+allMediaList[i].fileName+"' style='width : 100%; text-align : center;' onkeydown='if (event.keyCode == 13) {return false;}'></div></div></div>";
			$("#amazonImagesModalBody").append(audiodata);
		}		
		
	}if(mediaType == 'video'){
		
		for(var i=0;i<allFolderList.length;i++){
			var videoData = htmlValueVideoFolder + "<div class='videoContainer '><img src='images/windowFolder.jpg' alt='"+allFolderList[i].folderName + "' style='width:50%;height:100%;'>";
			videoData = videoData + " <div class='desc '> <input type='text' class='itemName' value='"+ allFolderList[i].folderName +"'  style='width : 100%; text-align : center;' onkeydown='if (event.keyCode == 13) {return false;}' ></div></div></div>";
			//sweetAlert("image tag====" + imageData);
			$("#amazonImagesModalBody").append(videoData);
		}
		
		
		for(var i=0;i<allMediaList.length;i++){
			var videoData = htmlValueVideoFile + "<div class='videoContainer'><video width='320' height='240' controls><source src='" +allMediaList[i].absolutePath +"' type='video/mp4' alt='"+allMediaList[i].fileName+"' onclick='this.paused ? this.pause() : this.pause();'>";
			//sweetAlert("1==" + videoData );
			videoData = videoData + "<source src='" +allMediaList[i].absolutePath +"' type='video/ogg' alt='"+allMediaList[i].fileName+"'>";
			videoData = videoData + "<source src='" +allMediaList[i].absolutePath +"' type='video/avi' alt='"+allMediaList[i].fileName+"'> </video>";
			//sweetAlert("2==" + videoData );
			videoData = videoData + "<div class='desc '><input type='text' class='itemName' value='"+allMediaList[i].fileName+"' style='width : 100%; text-align : center;'  onkeydown='if (event.keyCode == 13) {return false;}'></div></div></div>";
			//sweetAlert("video===" + videoData);
			$("#amazonImagesModalBody").append(videoData);
		}
	}
	
	if( !$('#mediaModalAWSBucket').is(':visible') ){
		
		
		$('#mediaModalAWSBucket').modal('show');
		$('#myCreateQuestionModal').css({
			'overflowX': 'hidden',
	    	'overflowY': 'auto'
		});
		
	}
	
	setNavigation(fullFolderNameCounter);
	
	//RESET INPUT TYPE FILE ON MEDIA MODAL
	 $('#resetInputTypeFileButtonMedia').click();
	
	
}

$("#mediaModalAWSBucket").on("hidden.bs.modal", function(){
    $("#amazonImagesModalBody").html("");
    
    //RESETING PATH OF MEDIA FOLDER IN MODAL
    fullFolderNameStack = [];
    fullFolderNameCounter = -1;
});


//checking media is image or not
function isImage(image){
	var allImageExtensions = ['jpeg','jpg','png','bmp','svg'];
	var extension = image.substring(image.lastIndexOf('.')+1, image.length);
	if(allImageExtensions.indexOf(extension.toLowerCase()) > -1){
		return true;
	}else{
		return false;
	}
}
//checking media is audio or not
function isAudio(audio){
	var allAudioExtensions = ['mp3','ogg','wma'];
	var extension = audio.substring(audio.lastIndexOf('.')+1, audio.length);
	if(allAudioExtensions.indexOf(extension.toLowerCase()) > -1){
		return true;
	}else{
		return false;
	}
}
//checking media is video or not
function isVideo(video){
	var allVideoExtensions = ['mp4','3gp','ogg','wmv','avi'];
	var extension = video.substring(video.lastIndexOf('.')+1, video.length);
	if(allVideoExtensions.indexOf(extension.toLowerCase()) > -1){
		return true;
	}else{
		return false;
	}
}

//renaming media
function renameItem(oldItemName,newItemName,folderPath){
	//alert('4');
	
	
	
	$.ajax({
			type : "GET",
			contentType : "application/json; charset=UTF-8",
			url : "renameItem.do",
			data : {
				'oldItemName' : oldItemName,
				'newItemName' : newItemName,
				'folderPath' : folderPath
			},
			success : function(result) {
				
				if(result.itemAlreadyExist.length>0){
					$('.selectedToRename').val(oldItemName)
					$("#emptyFolderErrorMessage").html("<b> Item with same name already exist ");
					$('#emptyFolderNameErrorModal').modal("show");
				}
				if(result.fileRenamed){
					sweetAlert("Rename successfully");
					displayContentOnModal(result);
				}
				
				$('.selectedToRename').css('background','transparent');
				$('.selectedToRename').prop('readonly', true);
			},	
			error : function() {
				
				$('.selectedToRename').val(oldItemName);
				$('.selectedToRename').css('background','transparent');
				$('.selectedToRename').prop('readonly', true);
				
				sweetAlert('Error while renaming..');
				
			}	
		});
	
}

//cheking valid key/alphabet for rename
function validKey(e){
	var keyCode = e.keyCode || e.which;
	
	if( keyCode == 191 || keyCode == 220 || keyCode == 186 || keyCode == 56 || keyCode == 222 ||
			keyCode == 222 ||  keyCode == 188  || keyCode == 190  ||  keyCode == 220){
		return false;
	}else {
		return true;
	}
}

function containsInvalidCharacter(newMediaName){
	var notValidCharacter = ['/',':','*','?','"','<','>','|'];
	var backwardSlash = '\\';
	for(var i = 0; i < newMediaName.length ; i++){
		if(notValidCharacter.indexOf(newMediaName.charAt(i)) > -1){
			return true;
			break;
		}else if(backwardSlash.charAt(0) == newMediaName.charAt(i)){
			return true;
			break;
		}
	}
	return false;
}

function containsInvalidCharacterInFolderName(newMediaName){
	var notValidCharacter = ['/',':','*','?','"','<','>','|','.'];
	var backwardSlash = '\\';
	for(var i = 0; i < newMediaName.length ; i++){
		if(notValidCharacter.indexOf(newMediaName.charAt(i)) > -1){
			return true;
			break;
		}else if(backwardSlash.charAt(0) == newMediaName.charAt(i)){
			return true;
			break;
		}
	}
	return false;
}

function copyItems(){
	
	$.ajax({
		type : "GET",
		contentType : "application/json; charset=UTF-8",
		url : "copyItems.do",
		cache : false,
		async: false,
		data : {
			'copiedItems' : JSON.stringify(copiedItems),
			'sourcePath' : sourcePath,
			'destinationPath' : destinationPath
		},
		success : function(result) {
			//alert('result success');
				if(result.itemAlreadyExist.length > 0){
					
					$("#emptyFolderErrorMessage").html("<b> Following item/s are already exist :\n" +
							""+ result.itemAlreadyExist +"</b>");
					$('#emptyFolderNameErrorModal').modal("show");
				}
				if(result.totalNumberOfItemCopied>0){
					sweetAlert(result.totalNumberOfItemCopied +" item/s copied successfully");
				}
				
				displayContentOnModal(result);
		
		},	
		error : function() {
			
			sweetAlert('Error while copying..');
		}
	});
}

function moveItems(){
	
	$.ajax({
		type : "GET",
		contentType : "application/json; charset=UTF-8",
		url : "moveItems.do",
		cache : false,
		async: false,
		data : {
			'copiedItems' : JSON.stringify(copiedItems),
			'sourcePath' : sourcePath,
			'destinationPath' : destinationPath
		},
		success : function(result) {
			
			//alert('result success');
			if(result.itemAlreadyExist.length > 0){
				
				$("#emptyFolderErrorMessage").html("<b> Following item/s are already exist :\n" +
						""+ result.itemAlreadyExist +"</b>");
				$('#emptyFolderNameErrorModal').modal("show");
			}
			if(result.totalNumberOfItemMoved>0){
				sweetAlert(result.totalNumberOfItemMoved +" item/s moved successfully");
			}
				
				displayContentOnModal(result);
		},	
		error : function() {
			
			sweetAlert('Error while request..');
		}
	});
}

function setNavigation(fullFolderNameCounter){//fullFolderNameStack
	var link = '';
	for(var i = 0; i<=fullFolderNameCounter ;i++){
		var href = fullFolderNameStack[i];
		var text = href.substring(href.lastIndexOf('/')+1);
		link += '<a href="'+ href +'" class="navigationLink" onclick="event.preventDefault();">'+ text +'</a> >';
	}
	
	link = link.substring(0,link.length-1);
	
	$('.navigation').html(link);
}


//ORDERING MEDIA IN ASCENDING ORDER