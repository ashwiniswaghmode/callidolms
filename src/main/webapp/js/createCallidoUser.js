function getSchoolgroupData(schoolGroupId) {
	alert(schoolGroupId);
		var array = {}
		array["schoolGroupId"] = schoolGroupId;
		$.ajax({
			type : "Post",
			contentType : "application/json; charset=UTF-8",
			url : 'getCallidoSchoolGroup.do',
			cache : false,
			async: false,
			data : JSON.stringify(array),
			success : function(result) {
			
				var addata = '<option>select</option>';

				$.each(result.value, function(k, v) {
					addata += '<option value='+v.schoolId+'>'+ v.schoolName + '</option>';
					
				});

				document.getElementById('schoolId').innerHTML = addata;
				
				
			},
			error : function() {
				alert('Error while request..');
			}
		});

}

function updateSchoolgroupData(schoolGroupId1) {
	
		var array = {}
		array["schoolGroupId"] = schoolGroupId1;
		$.ajax({
			type : "Post",
			contentType : "application/json; charset=UTF-8",
			url : 'getCallidoSchoolData.do',
			cache : false,
			async: false,
			data : JSON.stringify(array),
			success : function(result) {
			
				var addata = '<option>select</option>';

				$.each(result.value, function(k, v) {
				
					addata += '<option value='+v.schoolId+'>'+ v.schoolName + '</option>';
					
				});
				alert(addata);
				document.getElementById('schoolId1').innerHTML = addata;
				
				
			},
			error : function() {
				alert('Error while request..');
			}
		});

}
/*function getCallidoSchoolGroupData(roleId1) {
	
	var array = {}
	array["schoolGroupId"] = roleId1;
	$.ajax({
		type : "Post",
		contentType : "application/json; charset=UTF-8",
		url : 'getCallidoSchoolGroupData.do',
		cache : false,
		async: false,
		data : JSON.stringify(array),
		success : function(result) {
		
			var addata = '<option>select</option>';

			$.each(result.value, function(k, v) {
			
				addata += '<option value='+v.schoolId+'>'+ v.schoolName + '</option>';
				
			});
			alert(addata);
			document.getElementById('schoolId1').innerHTML = addata;
			
			
		},
		error : function() {
			alert('Error while request..');
		}
	});

}*/
function getCallidoSchoolData(schoolGroupId1,action) {
	
	var array = {}
	array["schoolGroupId"] = schoolGroupId1;
	$.ajax({
		type : "Post",
		contentType : "application/json; charset=UTF-8",
		url : 'getCallidoSchoolData.do',
		cache : false,
		async: false,
		data : JSON.stringify(array),
		success : function(result) {
		
			var addata = '<option>select</option>';

			$.each(result.value, function(k, v) {
					addata += '<option value='+v.schoolId+'>'+ v.schoolName + '</option>';
			});
			if(action == 'add')
				document.getElementById('schoolId').innerHTML = addata;
			else
				document.getElementById('schoolId1').innerHTML = addata;
			
			
		},
		error : function() {
			alert('Error while request..');
		}
	});
}
	
function getSelectedCallidoSchoolData(schoolGroupId1,schoolId) {
	
	var array = {}
	array["schoolGroupId"] = schoolGroupId1;
	$.ajax({
		type : "Post",
		contentType : "application/json; charset=UTF-8",
		url : 'getCallidoSchoolData.do',
		cache : false,
		async: false,
		data : JSON.stringify(array),
		success : function(result) {
		
			var addata = '<option>select</option>';

			$.each(result.value, function(k, v) {
				
				if(v.schoolId == schoolId)
					addata += '<option value='+v.schoolId+' selected>'+ v.schoolName + '</option>';
				else
					addata += '<option value='+v.schoolId+'>'+ v.schoolName + '</option>';
				
			});
			document.getElementById('schoolId1').innerHTML = addata;
			
			
		},
		error : function() {
			alert('Error while request..');
		}
	});

}