$('#questionTypeDiv').hide();
$('#levelDiv').hide();
$('#usageFlagDiv').hide();
$('#skillDiv').hide();

function getSelectedValue(id) {
  return $("#" + id).find("selectedQuestionType").html();
}
//function for multiple question type selection
$('.mutliSelect input[type="checkbox"]').on('click', function() {
  var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    title = $(this).val() + " ";
  if ($(this).is(':checked')) {
	  $('#questionTypeDiv').show();
	  var test = "'"+title+"'";
	  var html = '<span title="' + title + '"><button type="button" class="btn btn-default dashboardChkboxButton">' + title + '<span id="close" class="closeDashboardButton" onClick="uncheckQuestionType('+test+');this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);return false;">x</span></button></span>';
    $('#selectedQuestionType').append(html);
   
  } else {
	  $('#questionTypeDiv').show();
	  $('span[title="' + title + '"]').remove();
	  
	  if($('#CheckBoxList1 :checkbox:checked').length <= 0){	
		  $('#questionTypeDiv').hide();
		}	
  }

});
//function for multiple level selection
$('.multiLevelSelect input[type="checkbox"]').on('click', function() {

	  var title = $(this).closest('.multiLevelSelect').find('input[type="checkbox"]').val(),
	    title = $(this).val() + " ";
	  if ($(this).is(':checked')) {
		  $('#levelDiv').show();
		  var test = "'"+title+"'";
		  var html = '<span title="' + title + '"><button type="button" class="btn btn-default dashboardChkboxButton">' + title + '<span id="close" class="closeDashboardButton" onClick="uncheckLevel('+test+');this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);return false;">x</span></button></span>';
	    $('#selectedLevel').append(html);
	   
	  } else {
		  $('#levelDiv').show();
		  $('span[title="' + title + '"]').remove();
		  if($('#CheckBoxList2 :checkbox:checked').length <= 0){	
			  $('#levelDiv').hide();
		  }
	  }

	});
//function for multiple usage flag
$('.multiUsageFlag input[type="checkbox"]').on('click', function() {
	  var title = $(this).closest('.multiUsageFlag').find('input[type="checkbox"]').val(),
	    title = $(this).val() + " ";
	  if ($(this).is(':checked')) {
		  $('#usageFlagDiv').show();
		  var test = "'"+title+"'";
		  var html = '<span title="' + title + '"><button type="button" class="btn btn-default dashboardChkboxButton">' + title + '<span id="close" class="closeDashboardButton" onClick="uncheckUsageFlag('+test+');this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);return false;">x</span></button></span>';
	    $('#selectedUsageFlag').append(html);
	   
	  } else {
		  $('#usageFlagDiv').show();
		  $('span[title="' + title + '"]').remove();
		  if($('#CheckBoxList3 :checkbox:checked').length <= 0){	
			  $('#usageFlagDiv').hide();
			}
	  }
	});
//function for multiple skill
$('.skillDashboardText input[type="checkbox"]').on('click', function() {
	 
	  var title = $(this).closest('.hummingbirdNoParent').find('input[type="checkbox"]').val(),
	    title = $(this).val() + " ";
	  if ($(this).is(':checked')) {
		  $('#skillDiv').show();
		  var test = "'"+title+"'";
		  var html = '<span title="' + title + '"><button type="button" class="btn btn-default dashboardChkboxButton">' + title + '<span id="close" class="closeDashboardButton" onClick="uncheckSkill('+test+');this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);return false;">x</span></button></span>';
	    $('#selectedskill').append(html);
	   
	  } else {
		  $('#skillDiv').show();
		  $('span[title="' + title + '"]').remove();
		  if($('#CheckBoxList4 :checkbox:checked').length <= 0){	
			  $('#skillDiv').hide();
			}
	  }
	});
/*$('.multiSkill input[type="checkbox"]').on('click', function() {

	  var title = $(this).closest('.multiSkill').find('input[type="checkbox"]').val(),
	    title = $(this).val() + " ";
	  if ($(this).is(':checked')) {
		  $('#skillDiv').show();
		  var test = "'"+title+"'";
		   
		  var html = '<span title="' + title + '"><button type="button" class="chkedDashboardButton">' + title + '<span id="close" class="closeDashboardButton" onClick="uncheckSkill('+test+');this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);return false;">x</span></button></span>';
	    $('#selectedskill').append(html);
	   
	  } else {
		  $('#skillDiv').show();
		  $('span[title="' + title + '"]').remove();
		  if($('#CheckBoxList4 :checkbox:checked').length <= 0){	
			  $('#skillDiv').hide();
			}
	  }

	});*/
/*$('.multiSkill').on('click',function () {  
	var values=$(this).closest('.multiSkill').find('ul li input[type="checkbox"]');
	 var values1=$(values).attr('checked', this.checked).val();
    var childCheckBoxes = $("ul.multiSkill li input[type='checkbox']");
    var values = "";
    for(i=0; i< childCheckBoxes.length; i++)
    {
    	alert("inside for");
    	if(childCheckBoxes[i].is(':checked')){
    		alert("inside if"+childCheckBoxes[i].is(':checked'));
    		values += childCheckBoxes[i].value + "\n";
    	}
    }      
    alert(values1);
});*/
	function uncheckQuestionType(checkboxId){
		 if($('#CheckBoxList1 :checkbox:checked').length == 1){	
			  $('#questionTypeDiv').hide();
			}
		document.getElementById(checkboxId.trim().toLowerCase()).checked = false;	
	}
	function uncheckLevel(checkboxId){
		if($('#CheckBoxList2 :checkbox:checked').length == 1){	
			  $('#levelDiv').hide();
		  }
		document.getElementById(checkboxId.trim().toLowerCase()).checked = false;	 
	}
	function uncheckUsageFlag(checkboxId){
		 if($('#CheckBoxList3 :checkbox:checked').length == 1){	
			  $('#usageFlagDiv').hide();
			}
		document.getElementById(checkboxId.trim().toLowerCase()).checked = false;	
	}
	function uncheckSkill(checkboxId){
		 if($('#CheckBoxList4 :checkbox:checked').length == 1){	
			  $('#skillDiv').hide();
			}
		document.getElementById(checkboxId.trim().toLowerCase()).checked = false;	
	}