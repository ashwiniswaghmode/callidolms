

var globalSchoolList = [];
var teamTestList = [];
var teamList = [];
var testTypeList = [];
var testList = [];
//var teamCode = [];
//var testName = [];
//var testType = [];


function setSchoolList(allSchoolList){
	//alert('setSchoolList method');
	globalSchoolList = [];
	globalSchoolList = allSchoolList;
	//alert('school list length = '+allSchoolList.length);
	//alert('global school list length = '+globalSchoolList.length);
	
}


function createListOfTeamAndTest(teamListOfSelectedSchool){
	
	for(var i = 0; i<teamListOfSelectedSchool.length; i++){
		if (teamList.filter(function(e) { return e.teamId === teamListOfSelectedSchool[i].teamId; }).length == 0) {
			teamList.push({
				teamId : teamListOfSelectedSchool[i].teamId,
				teamCode : teamListOfSelectedSchool[i].teamCode
			});
		}
		
		if(teamListOfSelectedSchool[i].testType != '' && teamListOfSelectedSchool[i].testType != null){
			if (testTypeList.filter(function(e) { return e.testType === teamListOfSelectedSchool[i].testType; }).length == 0) {
				testTypeList.push({
					teamId : teamListOfSelectedSchool[i].teamId,
					testType : teamListOfSelectedSchool[i].testType
				});
			}
			
			if (testList.filter(function(e) { return e.testId === teamListOfSelectedSchool[i].testId; }).length == 0) {
				testList.push({
					teamId : teamListOfSelectedSchool[i].teamId,
					testType : teamListOfSelectedSchool[i].testType,
					testId : teamListOfSelectedSchool[i].testId,
					testName : teamListOfSelectedSchool[i].testName
				});
			}
		}
	}
	
	for (var i = 0; i < teamList.length; i++) {
		$('#teamId').append($('<option>',{
			value : teamList[i].teamId,
			text : teamList[i].teamCode
		}));
	}
	
	
	
	
	
	
}

$(function(){
	//alert('in js');
	
	

	
	
	/*$(document).on('change', '#testType', function() {
		if ($(this).val() == 'Static test') {
			$('#staticTestId').css({
				'display' : ''
			});
			$('#staticTestId').attr('required', true);

			$('#adaptiveTestId').css({
				'display' : 'none'
			});
			$('#adaptiveTestId').attr('required', false);

			$('#defaultGrade').css({
				'display' : ''
			});

		} else {
			$('#staticTestId').css({
				'display' : 'none'
			});
			$('#staticTestId').attr('required', false);
			$('#adaptiveTestId').css({
				'display' : ''
			});
			$('#adaptiveTestId').attr('required', true);
			$('#defaultGrade').css({
				'display' : 'none'
			});
		}
	});*/
	
	$(document).on('change', '#schoolGroupId', function() {
		$('#schoolId').find('option:not(:first)').remove();
		
		for (var i = 0; i < globalSchoolList.length; i++) {
			if(globalSchoolList[i].schoolGroupId ==$(this).val() ){
				$('#schoolId').append($('<option>', {
					value : globalSchoolList[i].schoolId,
					text : globalSchoolList[i].schoolName
				}));
			}
			
			
		}
		
	});
	
	
	
	$(document).on('change','#schoolId',function() {
		teamTestList = [];
		teamList = [];
		testTypeList = [];
		testList = [];
		var schoolId = $(this).val();
		var academicYear=$('#academicYear').val();
		//alert("academicYear="+academicYear);
		//alert("schoolId="+schoolId);
		if (schoolId == '') {
			return false;
		}
		
		$('#teamId').find('option:not(:first)').remove();
		var array = {
				'academicYear' : academicYear,
				'schoolId' : schoolId
		  };
		
		
		$.ajax({
					type : "POST",
					contentType : "application/json; charset=UTF-8",
					url : "getTeamAndTestListFromSchool.do",
					cache : false,
					data : JSON.stringify(array),
					success : function(result) {
						//alert("result=="+result);
						teamTestList = result.teamTestList;
						 
						//alert("teamTestList length+++++"+teamTestList.length);
						
						if (teamTestList.length > 0) {
        
							for (var i = 0; i < teamTestList.length; i++) {
								//alert("teamTestList....."+teamTestList.length);
								
								//var teamId = teamTestList[i].teamCode.replace(" ", "_");
								var teamId = teamTestList[i].teamId;
								//alert('teamId****='+teamId);
								
								//var teamPresent = 0;
								
								if (teamList.filter(function(e) { return e.teamId === teamId; }).length == 0) {
									teamList.push({
										teamId : teamId,
										teamCode : teamTestList[i].teamCode
									});
								}
								
								/*$.each(teamCode,function(index,value) {
													if (value.teamId ==teamId) {
														teamPresent++;
													}
												});*/

								/*if (teamPresent == 0) {
								alert('teamTestList[i].teamCode***********'+teamTestList[i].teamCode);
									teamCode.push({
										teamId : teamId,
										teamCode : teamTestList[i].teamCode

											});

								}*/
								//alert('teamTestList[i].testType ='+teamTestList[i].testType );
								if(teamTestList[i].testType != '' && teamTestList[i].testType != null){
									if (testTypeList.filter(function(e) { return e.testType === teamTestList[i].testType; }).length == 0) {
										testTypeList.push({
											teamId : teamTestList[i].teamId,
											testType : teamTestList[i].testType
										});
									}
									
									if (testList.filter(function(e) { return e.testId === teamTestList[i].testId; }).length == 0) {
										testList.push({
											teamId : teamTestList[i].teamId,
											testType : teamTestList[i].testType,
											testId : teamTestList[i].testId,
											testName : teamTestList[i].testName
										});
									}
								}
								
								
								
								
								/*var testNameIdPresent = 0;
								$.each(testName, function(index, value) {
									if (value.sglId == teamTestList[i].sglId  &&  value.testName == teamTestList[i].testName) {
										testNameIdPresent++;
									}
								});*/
								
								/*if(testNameIdPresent == 0){
									//alert('teamTestList[i].testName='+teamTestList[i].testName);
									testName.push({
										teamCode : teamCode,
										teamId : teamId,
										sglId : teamTestList[i].sglId,
										testName : teamTestList[i].testName
										
									});
								} */
								
								
								
								/*var testTypeIdPresent = 0;
								$.each(testType, function(index, value) {
									if (value.sglId == teamTestList[i].testId  &&  value.testType == teamTestList[i].testType) {
										testTypeIdPresent++;
									}
								});*/
								
								/*if(testTypeIdPresent == 0){
									alert('teamTestList[i].testType='+teamTestList[i].testType);
									testType.push({
										
										teamCode : teamCode,
										teamId : teamId,
										sglId : teamTestList[i].testId,
										testType : teamTestList[i].testType
										
									});
								}*/
							

							}
						}
						
						//alert("length...."+teamCode.length);
						for (var i = 0; i < teamList.length; i++) {
							$('#teamId').append($('<option>',{
								value : teamList[i].teamId,
								text : teamList[i].teamCode
							}));
						}

					},
					error : function() {
						alert('error occured...')
					}
				});

	});
	
	
	
	$(document).on('change', '#teamId', function() {
		var teamId = $(this).val();
		$('#teamCode').val($('#teamId option:selected').text());
		$('#testType').find('option:not(:first)').remove();
		
		$.each(testTypeList, function(index, value) {
			if(value.teamId == teamId){
				$('#testType').append($('<option>', {
					value : value.testType,
					text : value.testType
				}));
			}
		});
	});
	
	
	

		/*$(document).on('change', '#teamId', function() {
			var testName = $(this).val();
			$('#testType').find('option:not(:first)').remove();
			$.each(testType, function(index, value) {
				if (value.teamCode == teamCode) {
					$('#testType').append($('<option>', {
						value : value.testType,
						text : value.testType
					
					}));
				}
			});
		});*/
	
	
	$(document).on('change', '#testType', function() {
		var teamId = $('#teamId').val();
		var testType = $(this).val();
		$('#testId').find('option:not(:first)').remove();
		//alert('testList length='+testList.length);
		$.each(testList, function(index, value) {
			if (value.teamId == teamId && value.testType == testType ) {
				$('#testId').append($('<option>', {
					value : value.testId,
					text : value.testName
				}));
			}
		});
	});
	
		/*$(document).on('change', '#testType', function() {
				var teamId = $(this).val();
				$('#testName').find('option:not(:first)').remove();
				$.each(testName, function(index, value) {
					if (value.teamCode == teamCode) {
						$('#testName').append($('<option>', {
							value : value.testId,
							text : value.testName
						}));
					}
				});
			});*/
	
	
	
	
});

