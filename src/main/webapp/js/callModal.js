function toggleDiv() {
	document.getElementById("viewDiv").style.display = "block";
	document.getElementById("confirmDiv").style.display = "none";
	$("#forDisabled").hide();
	document.getElementById("createTextId").innerHTML = "Create Team";
	document.getElementById("btnId").value = "Add";
	var e1 = document.getElementById("sgname").value = "";
	var e2 = document.getElementById("drpschool").innerHTML = "";
	var e3 = document.getElementById("drpcallev").innerHTML = "";
	var e4 = document.getElementById("drpcurri").innerHTML = "";
	var e5 = document.getElementById("drppack").innerHTML = "";
	var e6 = document.getElementById("drpdiv").innerHTML = "";
	var e7 = document.getElementById("txtdate").value = "";
	var e8 = document.getElementById("NoofTeams").value = 1;

	var newsg = document.getElementById("sgname_new").value = "";
	var news = document.getElementById("drpschool_new").value = "";
	var newcl = document.getElementById("drpcallev_new").value = "";
	var newcu = document.getElementById("drpcurri_new").value = "";
	var newpac = document.getElementById("drppack_new").value = "";
	var newdiv = document.getElementById("drpdiv_new").value = "";
	var neway = document.getElementById("txtdate_new").value = "";
	/*
	 * var newlitmusId=document.getElementById("litmusId").value=""; var
	 * newlitmusId=document.getElementById("teamCode").value="";
	 */

}

function disabledFields() {
	var getDrp = document.getElementById("drppack");
	var selectedPackage = getDrp.options[getDrp.selectedIndex].text;

	if (selectedPackage == 'Callido Admission') {

		$("#forDisabled").hide();
	}

	else {
		$("#forDisabled").show();

	}
}
function getSchoolData(schoolId) {
	var array = {}
	array["schoolId"] = schoolId;
	$
			.ajax({
				type : "Post",
				contentType : "application/json; charset=UTF-8",
				url : 'getSchoolValue.do',
				cache : false,
				async : false,
				data : JSON.stringify(array),
				success : function(result) {

					var addata = '<option></option>';

					$
							.each(
									result.value,
									function(k, v) {
										addata += '<option value='
												+ v.curriculumName + '>'
												+ v.curriculumName
												+ '</option>';
										document
												.getElementById("schoolabbrevition").value = v.schoolCode;
									});

					document.getElementById('drpcurri').innerHTML = addata;
					document.getElementById('drpcurri_new').innerHTML = addata;

				},
				error : function() {
					alert('Error while request..');
				}
			});
}

function getdivisionabbreviation(divisionId) {

	$.ajax({
		url : 'getDivisionAbbreviation.do',
		type : 'GET',
		data : {
			Id : divisionId
		},
		success : function(result) {
			document.getElementById("divisionabbrevition").value = result;

		}
	});
}

$('#drppack').change(function() {

	var packageId = $(this).val();
	$.ajax({
		url : 'getPackageAbbreviation.do',
		type : 'GET',
		data : {
			Id : packageId
		},
		success : function(result) {
			document.getElementById("packageabbrevition").value = result;

		}
	});
});

function call_modal() {
	if ($('#txtdate').find('option:selected').text() == "") {
		swal('Please select the Academic Year');
		return false;
	} else if ($('#sgname').find('option:selected').text() == "") {
		swal('Please select the SchoolGroup');
		return false;
	} else if ($('#drpschool').find('option:selected').text() == "") {
		swal('Please select the School');
		return false;
	} else if ($('#drpschool').find('option:selected').text() == "") {
		swal('Please select the School');
		return false;
	} else if ($('#drpcurri').find('option:selected').text() == "") {
		swal('Please select the Curriculum');
		return false;
	} else if ($('#drpcallev').find('option:selected').text() == "") {
		swal('Please select the Callidolevel');
		return false;
	} else if ($('#drppack').find('option:selected').text() == "") {
		swal('Please select the Package');
		return false;
	} else if ($('#drppack').find('option:selected').text() != "Callido Admission") {
		if ($('#drpdiv').find('option:selected').text() == "") {
			swal('Please select the Division');
			return false;
		}
	}

	document.getElementById("viewDiv").style.display = "none";
	document.getElementById("confirmDiv").style.display = "block";
	var e1 = document.getElementById("sgname");
	var e2 = document.getElementById("drpschool");
	var e3 = document.getElementById("drpcallev");
	var e4 = document.getElementById("drpcurri");
	var e5 = document.getElementById("drppack");
	var e6 = document.getElementById("drpdiv");
	var e7 = document.getElementById("txtdate");
	var e8 = document.getElementById("NoofTeams");
	var yearVal = e7.value;
	var strUser1 = e1.options[e1.selectedIndex].value;
	var strUser2 = e2.options[e2.selectedIndex].value;
	var strUser3 = e3.options[e3.selectedIndex].value;
	var strUser4 = e4.options[e4.selectedIndex].text;
	var strUser5 = e5.options[e5.selectedIndex].value;
	var strUser6 = e6.options[e6.selectedIndex].value;
	var selectedPackage = e5.options[e5.selectedIndex].text;

	$('#tempId').html('');
	var chk = e8.value;
	var callidolevel = e3.options[e3.selectedIndex].text;
	var school = document.getElementById("schoolabbrevition").value;
	var division = document.getElementById("divisionabbrevition").value;
	var packagecode = document.getElementById("packageabbrevition").value;
	if (chk > 1) {

		for (var i = 1; i <= chk; i++) {
			var temp = school + "_" + callidolevel + "_" + strUser4 + "_"
					+ division + "_0" + i + "_" + yearVal.substring(3, 8);
			var htmlText = '<div class="row"><div class="col-lg-6"><input  type="text" class="new-form-control" value="'
					+ temp
					+ '" required=""style="margin: 0px;" id="teamCode" disabled><label class="newLabel2"  > Team Code</label><input type="hidden" name="teamCode'
					+ i
					+ '" value="'
					+ temp
					+ '"></div>'
					+ '<div class="col-lg-6"> <input type="text" class="new-form-control" style="margin: 0px;" name="litmusTeamId'
					+ i
					+ '" id="litmusId"> <label class="newLabel2"> Litmos Team ID</label></div></div>';

			$('#tempId').append(htmlText);
		}

	} else {
		if (selectedPackage == 'Callido Admission') {

			var temp = school + "_" + callidolevel + "_" + strUser4 + "_"
					+ packagecode + "_" + yearVal.substring(3, 8);
			var htmlText = '<div class="row"><div class="col-lg-6"><input  type="text" class="new-form-control" value="'
					+ temp
					+ '" required=""style="margin: 0px;" id="teamCode" disabled><label class="newLabel2"  > Team Code</label><input type="hidden" name="teamCode'
					+ chk
					+ '" value="'
					+ temp
					+ '"></div>'
					+ '<div class="col-lg-6"> <input type="text" class="new-form-control" style="margin: 0px;" name="litmusTeamId'
					+ chk
					+ '" id="litmusId"> <label class="newLabel2"> Litmos Team ID</label></div></div>';

			$('#tempId').append(htmlText);
		} else {
			var temp = school + "_" + callidolevel + "_" + strUser4 + "_"
					+ division + "_0" + chk + "_" + yearVal.substring(3, 8);
			var htmlText = '<div class="row"><div class="col-lg-6"><input  type="text" class="new-form-control" value="'
					+ temp
					+ '" required=""style="margin: 0px;" id="teamCode" disabled><label class="newLabel2"  > Team Code</label><input type="hidden" name="teamCode'
					+ chk
					+ '" value="'
					+ temp
					+ '"></div>'
					+ '<div class="col-lg-6"> <input type="text" class="new-form-control" style="margin: 0px;" name="litmusTeamId'
					+ chk
					+ '" id="litmusId"> <label class="newLabel2"> Litmos Team ID</label></div></div>';
			$('#tempId').append(htmlText);
		}
	}

	document.getElementById("sgname_new").value = strUser1;
	document.getElementById("drpschool_new").value = strUser2;
	document.getElementById("drpcallev_new").value = strUser3;
	document.getElementById("drpcurri_new").options[0].text = strUser4;
	document.getElementById("drppack_new").value = strUser5;
	document.getElementById("drpdiv_new").value = strUser6;
	document.getElementById("txtdate_new").value = e7.value;

	// set the hidden values
	document.getElementById("sgname_hidden").value = strUser1;
	document.getElementById("drpschool_hidden").value = strUser2;
	document.getElementById("drpcallev_hidden").value = strUser3;
	document.getElementById("drpcurri_hidden").value = strUser4;
	document.getElementById("drppack_hidden").value = strUser5;
	document.getElementById("drpdiv_hidden").value = strUser6;
	document.getElementById("txtdate_hidden").value = e7.value;

	if (selectedPackage == 'Callido Admission') {
		$("#drpDivision").hide();
	} else {
		$("#drpDivision").show();
	}

	document.getElementById("sgname_new").disabled = true;
	document.getElementById("drpschool_new").disabled = true;
	document.getElementById("drpcallev_new").disabled = true;
	document.getElementById("drpcurri_new").disabled = true;
	document.getElementById("drppack_new").disabled = true;
	document.getElementById("drpdiv_new").disabled = true;
	document.getElementById("txtdate_new").disabled = true;
	var litmusid = document.getElementById("litmusIdnext").value;
	document.getElementById("litmusId").value = litmusid;

}

function getSchoolgroupData(schoolGroupId) {
	var array = {}
	array["schoolGroupId"] = schoolGroupId;
	$.ajax({
		type : "Post",
		contentType : "application/json; charset=UTF-8",
		url : 'getSchoolGroupValue.do',
		cache : false,
		async : false,
		data : JSON.stringify(array),
		success : function(result) {

			var addata = '<option></option>';

			$.each(result.value, function(k, v) {
				addata += '<option value=' + v.schoolId + '>' + v.schoolName
						+ '</option>';

			});

			document.getElementById('drpschool').innerHTML = addata;
			document.getElementById('drpschool_new').innerHTML = addata;

		},
		error : function() {
			alert('Error while request..');
		}
	});

}
function getCallidolevel() {
	var curriculum = document.getElementById('drpcurri');
	var curriculumName = curriculum.options[curriculum.selectedIndex].text;
	var schoolId = document.getElementById('drpschool').value;
	var array = {
		'curriculumName' : curriculumName,
		'schoolId' : schoolId
	};
	$.ajax({
		type : "Post",
		contentType : "application/json; charset=UTF-8",
		url : 'getcallidolevelValue.do',
		cache : false,
		async : false,
		data : JSON.stringify(array),
		success : function(result) {

			var addata = '<option></option>';

			$.each(result.value, function(k, v) {
				addata += '<option value=' + v.sglId + '>' + v.callidoLevelName
						+ '</option>';

			});

			document.getElementById('drpcallev').innerHTML = addata;
			document.getElementById('drpcallev_new').innerHTML = addata;
		},
		error : function() {
			alert('Error while request..');
		}
	});
}

function getPackageData(sglId) {
	var array = {}
	array["sglId"] = sglId;
	$.ajax({
		type : "Post",
		contentType : "application/json; charset=UTF-8",
		url : 'getCallidoLevelPackages.do',
		cache : false,
		async : false,
		data : JSON.stringify(array),
		success : function(result) {

			var adddataPackage = '<option></option>';
			$.each(result.value, function(k, v) {

				adddataPackage += '<option value=' + v.packageId + '>'
						+ v.packageName + '</option>';

			});

			document.getElementById('drppack').innerHTML = adddataPackage;
			document.getElementById('drppack_new').innerHTML = adddataPackage;
		},
		error : function() {
			alert('Error while request..');
		}
	});
}

function getGradeLevelDivisions(packageId) {
	var sglId = document.getElementById('drpcallev').value;

	var array = {
		'packageId' : packageId,
		'sglId' : sglId
	};
	$.ajax({
		type : "Post",
		contentType : "application/json; charset=UTF-8",
		url : 'getGradeLevelDivisions.do',
		cache : false,
		async : false,
		data : JSON.stringify(array),
		success : function(result) {

			var addataDivision = '<option value=' + 0 + '></option>';
			$.each(result.value, function(k, v) {

				addataDivision += '<option value=' + v.divisionId + '>'
						+ v.divisionName + '</option>';

			});

			document.getElementById('drpdiv').innerHTML = addataDivision;
			document.getElementById('drpdiv_new').innerHTML = addataDivision;
		},
		error : function() {
			alert('Error while request..');
		}
	});
}

function updateTeampopup(teamId, sglId, academicYear, schoolGroupId, schoolId,
		curriculumName, calidoLevel, packageId, divisionId, teamCode,
		litmusTeamId) {
	document.getElementById("createTextId").innerHTML = "Edit Team";
	document.getElementById("viewDiv").style.display = "block";
	document.getElementById("confirmDiv").style.display = "none";
	document.getElementById("btnId").innerHTML = "Update";
	document.getElementById("teamId").value = teamId;
	document.getElementById("txtdate").value = academicYear;
	document.getElementById("sgname").value = schoolGroupId;
	getSchoolgroupData(schoolGroupId);
	document.getElementById("drpschool").value = schoolId;
	getSchoolData(schoolId);
	var option = document.getElementById("drpcurri").options[0];
	option.value = option.text = curriculumName;
	getCallidolevel();
	document.getElementById("drpcallev").value = sglId;
	getPackageData(sglId);
	disabledFields();
	document.getElementById("drppack").value = packageId;
	getGradeLevelDivisions(packageId);
	document.getElementById("drpdiv").value = divisionId;
	getdivisionabbreviation(divisionId);
	document.getElementById("litmusIdnext").value = litmusTeamId;

}

function deleteTeampopup(teamId) {
	document.getElementById("teamIdDelete").value = teamId;

}

function viewTeampopup(academicYear, schoolgroupname, schoolName,
		curriculumName, calidoLevel, packageName, divisionName, teamCode,
		litmusTeamId) {

	document.getElementById("createTextId").innerHTML = "Team";
	document.getElementById("txtdateview").textContent = academicYear;
	document.getElementById("sgnameview").textContent = schoolgroupname;
	document.getElementById("drpschoolview").textContent = schoolName;
	document.getElementById("drpcurriview").textContent = curriculumName;
	document.getElementById("drpcallevview").textContent = calidoLevel;
	document.getElementById("teamCode").textContent = teamCode;
	document.getElementById("litmusId").textContent = litmusTeamId;
	document.getElementById("drppackview").textContent = packageName;

	if (packageName == 'Callido Admission') {

		document.getElementById("forDisabledView").style.display = 'none';
	} else {
		document.getElementById("forDisabledView").style.display = 'block';
		document.getElementById("drpdivview").textContent = divisionName;

	}

}
