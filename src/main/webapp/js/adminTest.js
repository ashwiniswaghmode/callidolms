        $(document).ready(function () {
        	
           $('#tList2').dataTable({
            	  "columnDefs": [
					    { targets: 0 , visible: true }
					   
					  ]
			});
           
          /*  $( "#datepicker" ).datepicker({
			 beforeShowDay: DisableMonday
			});
*/
			$('[data-toggle="tooltip"]').tooltip();
        });

        function DisableMonday(date) {
 
  var day = date.getDay();
 // If day == 1 then it is MOnday
 if (day == 1) {
 
 return [false] ; 
 
 } else { 
 
 return [true] ;
 }
  
}

		var i=1;
		function addColumn(){
		//alert("working");
			
			$('.tableRCRecived').find('tr').each(function(){ 
				$(this).find('th').eq(-1).after('<th style="width: 100px;" contenteditable="true">HEADER '+i+'</th>'); 
				$(this).find('td').eq(-1).after('<td contenteditable="true">ROW</td>'); 
			});
			i += 1;	
			
		}
		
		function getdate() {
			var tt = document.getElementById('txtDate').value;

			var date = new Date(tt);
			var newdate = new Date(date);

			newdate.setDate(newdate.getDate() + 90);
			
			var dd = newdate.getDate();
			var mm = newdate.getMonth() + 1;
			var y = newdate.getFullYear();

			var someFormattedDate = mm + '/' + dd + '/' + y;
			document.getElementById('follow_Date').value = someFormattedDate;
		}

	var expanded = false;

	function showCheckboxes() {
	  var checkboxes = document.getElementById("checkboxes");
	  if (!expanded) {
	    checkboxes.style.display = "block";
	    expanded = true;
	  } else {
	    checkboxes.style.display = "none";
	    expanded = false;
	  }
	}


$(":checkbox").change(function() {
    var checkedValues = $(":checkbox:checked").map(function() {
        return this.value;
    }).get();

    $("tbody tr").hide();
    for (var i = 0; i < checkedValues.length; i++) {
        $("tbody tr td:contains('" + checkedValues[i] + "')").parent("tr").show();
    }
});

    function dynamicdropdown(listindex)
    {
        switch (listindex)
        {
        case "APAC" :
            document.getElementById("state").options[0]=new Option("--Select Org Level 2--","");
            document.getElementById("state").options[1]=new Option("Sales","Sales");
			document.getElementById("state").options[2]=new Option("Finance","Finance");
			document.getElementById("state").options[3]=new Option("Admin","Admin");
            break;
        case "EMEA" :
            document.getElementById("state").options[0]=new Option("--Select Org Level 2--","");
            document.getElementById("state").options[1]=new Option("Sales","Sales");
			document.getElementById("state").options[2]=new Option("Finance","Finance");
			document.getElementById("state").options[3]=new Option("Admin","Admin");
            break;
		case "Sales" :
            document.getElementById("cities").options[0]=new Option("--Select Org Level 3--","");
            document.getElementById("cities").options[1]=new Option("Inside Sales","Inside Sales");
            document.getElementById("cities").options[2]=new Option("Sales","Sales");
            break;
        case "Finance" :
            document.getElementById("cities").options[0]=new Option("--Select Org Level 3--","");
            document.getElementById("cities").options[1]=new Option("Accounting","Accounting");
            document.getElementById("cities").options[2]=new Option("Invoice","Invoice");
            break;
		case "Admin" :
            document.getElementById("cities").options[0]=new Option("--Select Org Level 3--","");
            document.getElementById("cities").options[1]=new Option("Payroll","Payroll");
            document.getElementById("cities").options[2]=new Option("IT Admin","IT Admin");
            document.getElementById("cities").options[2]=new Option("Logistics","Logistics");
            break;
        }
        return true;
    }			