$(function(){
	//alert("questionBank2.js changed text");
	
	var selectedValueBlank, selectedTextBlank;
    $(document).on('change', '[class ^= "dropDownTypeSelect"]', function () {
    	
        var dropDownTypeSelectCount = this.className[18];
        
        if ($(this).find(':selected').text() == "Image") {            
            $('.selectionDropDownImage'+dropDownTypeSelectCount).css('display','block');
            $('#dropdownQuestionImage'+dropDownTypeSelectCount).val('');
            $('#previewDropDown'+dropDownTypeSelectCount).css('background-image','');

            $('#previewDropDown'+dropDownTypeSelectCount).html('<h1 style="color: #fff;display: table-cell;text-align: center;vertical-align: middle;font-weight: bold;">Preview</h1>');

/*             var attr = $('#previewDropDown'+dropDownTypeSelectCount).attr('style');

            if( typeof attr !== typeof undefined && attr !== false ){
                //alert("has bg");
                $('#previewDropDown'+dropDownTypeSelectCount).html('');
            }else{
                $('#previewDropDown'+dropDownTypeSelectCount).html('<h1 style="color: #fff;display: table-cell;text-align: center;vertical-align: middle;font-weight: bold;">Preview</h1>');
            }  */  
            
            $('#dropdownPreviewImageDiv'+dropDownTypeSelectCount).html('');
        }
        if ($(this).find(':selected').text() == "Text") {
        	//alert('found');
            $('.selectionDropDownImage'+dropDownTypeSelectCount).css('display','none');
            $('#previewDropDown'+dropDownTypeSelectCount).html('<textarea name="mediaName" id="textareaDropDown'+ dropDownTypeSelectCount +'" contenteditable="true" style="width: 100%; height: 100%;border: 0;border-radius: 5px;resize: none;"></textarea>');
            
            $('#dropdownPreviewImageDiv'+dropDownTypeSelectCount).html('<textarea id="textareaDropDownPreview'+ dropDownTypeSelectCount +'" style="width: 100%; height: 100%;border: 0;border-radius: 5px;resize: none;cursor: move;" readonly></textarea>');
        
        }
        
        
    });
    
    
    $(document).on('change', '.dragAndDropTypeSelect', function () {
        var dropDownTypeSelectCount = this.className[21];
        
        
        if ($(this).find(':selected').text() == "Image") { 
        	
			$('#dragItemTable tbody tr td:nth-child(2)').each(function(){
				var dragAndDropRowCounter = $(this).closest('tr').find('td:first-child').text();
				
				$(this).html('<div class="" id="inputFileType">'+
						 '<input type="button" class="" id="buttonDropDownInputAWS'+dragAndDropRowCounter+'" value="Choose File" placeholder="background image" onclick="getImages(\'image\',this)">'+
							'<input type="text" value="" placeholder="No file chosen" id="dragItemImage'+dragAndDropRowCounter+'" name="dropZoneOption" src="" required="" readonly="">'+																				
						'</div>');
			});	 
        	
			$('[id ^= "dropItemName"]').each(function(){
				var textDropItemCounter = this.id[12];				
				$(this).html('');
				$(this).text('Item '+textDropItemCounter);
				
			});
			
			$('[id ^= "dropItemPreviewName"]').each(function(){
				var textPreviewDropItemCounter = this.id[19];
				$(this).html('');
				$(this).text('Item '+textPreviewDropItemCounter);
			});
					
            
/*            $('[id ^= "answerPreviewPickManyOptionDiv"]').each(function(){
                var answerPickManyOptionId = this.id[30];
                //alert(answerPickManyOptionId);
                $(this).html('<div class="" id="answerOptionImageType'+answerPickManyOptionId+'">'+                                                
                '</div>');
            });*/

        }
        if ($(this).find(':selected').text() == "Text") {        	
        	
			$('#dragItemTable tbody tr td:nth-child(2)').each(function(){
				var dragAndDropRowCounter = $(this).closest('tr').find('td:first-child').text();
				
				$(this).html('<textarea id="textareaTableDropItem'+dragAndDropRowCounter+'" name="dropZoneOption" placeholder="Option '+dragAndDropRowCounter+'" style="width: 100%; height: 21px;border: 0;padding: 0;resize: none;" required></textarea>');
				/*$(this).css({
					'paddingBottom':'0px !important'
				});*/
			});	     
			
			$('[id ^= "dropItemName"]').each(function(){
				var textDropItemCounter = this.id[12];				
				$(this).html('<textarea id="textareaDropItem'+textDropItemCounter+'"   placeholder="Item '+textDropItemCounter+'" readonly style="width: 100%;height: 100%;border:0;resize: none;cursor: move;"></textarea>');
				/*$(this).removeAttr('style');*/
				$(this).css({
					'backgroundImage': ''
				});
			});
			
			$('[id ^= "dropItemPreviewName"]').each(function(){
				var textPreviewDropItemCounter = this.id[19];
				$(this).html('<textarea id="textareaPreviewDropItem'+textPreviewDropItemCounter+'" placeholder="Item '+textPreviewDropItemCounter+'" readonly style="width: 100%;height: 100%;border:0;resize: none;cursor: move;color: #000;"></textarea>');
				/*$(this).removeAttr('style');*/
				$(this).css({
					'backgroundImage': ''
				});
			});
        
        }        
        
    });
    
    $(document).on('keyup','[id ^= "textareaTableDropItem"]',function(){
    	
    	var textInputCounter = this.id[21];
    	
    	$('#textareaDropItem'+textInputCounter).val( $(this).val() );
    	$('#textareaPreviewDropItem'+textInputCounter).val( $(this).val() );
    });
    
/*    $(document).on('click','#createQuestionSave',function(){
    	//alert("updating save");
    	var textBoxHtml = $('#textBox').html();
    	//alert(textBoxHtml);
    	$('#htmlTextBox').html(textBoxHtml);
    });*/
    $(document).on('click','#createQuestionSave',function(){    	
    	var textBoxHtml = $('#textBox').html();
    	//alert("textBoxHtml "+textBoxHtml); 
    	
    	var pString = '<style type="text/css"><!--td {border: 1px solid #ccc;}br {mso-data-placement:same-cell;}--></style>';
    	//alert("pString "+pString); 
    	
    	if(textBoxHtml.indexOf(pString) != -1){
    		$('#textBox style').remove();    		
    	}
    	//$('#htmlTextBox').html(textBoxHtml);
    	
    	var textBoxHtmlRefined = $('#textBox').html();    	
    	$('#htmlTextBox').html(textBoxHtmlRefined);
    });
    $(document).on('click','#previewCreateQuestion',function(){
    	var selectedValue = $('.dynamicQuestionTypeSelect').find(':selected').val();
    	var selectedText = $('.dynamicQuestionTypeSelect').find(':selected').text();
    	
    	if(selectedValue == 5){
    		//alert("selected value 5");
    		
        	if( ($('#mediaEnableCheckbox').prop('checked') == false ) && ( $('.optionTypePickMany').find(':selected').text() == "Text") ){
        		$('#mediaPreviewPickManyDiv').removeClass().addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
    			$('#answerPreviewPickManyDiv').removeClass().addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left");
    			
        		$('#answerPreviewPickManyDiv').css({
        			'textAlign': 'left'
        		});
        		$('[id ^= "answerPreviewPickManyOptionDiv"]').css({
        			'minWidth': '85%',
        			'height': 'auto',
        			'margin': '0px 0px 10px 5px',
        			'paddingLeft': '15px'
        		});
        	}else if( ($('#mediaEnableCheckbox').prop('checked') == false ) && ( $('.optionTypePickMany').find(':selected').text() == "Image") ){
        		$('#mediaPreviewPickManyDiv').removeClass().addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
    			$('#answerPreviewPickManyDiv').removeClass().addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left");
    			
        		$('[id ^= "answerPreviewPickManyOptionDiv"]').css({
        			'minWidth': '180px',
        			'height': '180px',
        			'margin': '0px 45px 18px 0px',
        			'padding': '0'
        		});
        		$('#answerPreviewPickManyDiv').css({
        			'textAlign': 'center'
        		});
        		$('[id ^= "answerOptionImageType"]').css({
        			'height': '100%',
        			'borderRadius': '4px'
        		});
        	}else if( ($('#mediaEnableCheckbox').prop('checked') == true ) && ( $('.optionTypePickMany').find(':selected').text() == "Text") ){
        		
        			$('#mediaPreviewPickManyDiv').removeClass().addClass("col-xs-5 col-sm-5 col-md-5 col-lg-5");
        			$('#answerPreviewPickManyDiv').removeClass().addClass("col-xs-7 col-sm-7 col-md-7 col-lg-7 text-left");
        			
        			$('#mediaPreviewPickManyDiv').css({
        				/*'display': 'block',*/
        				'height': '200px',
        				'float': 'left',
        				'marginTop': '20px'
        			});
        			
        			$('#answerPreviewPickManyDiv').css({    				
        				'float': 'left',
        				'textAlign': 'left'
        			});
        			    			
            		$('[id ^= "answerPreviewPickManyOptionDiv"]').css({
            			'minWidth': '85%',
            			'height': 'auto',
            			'margin': '0px 0px 10px 5px',
            			'paddingLeft': '15px'
            		});
        		
        		
        	}else if( ($('#mediaEnableCheckbox').prop('checked') == true ) && ( $('.optionTypePickMany').find(':selected').text() == "Image") ){
        		$('#mediaPreviewPickManyDiv').removeClass().addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
    			$('#answerPreviewPickManyDiv').removeClass().addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left");
    			
        		$('#mediaPreviewPickManyDiv').css({
        			'height': '200px',
        			'float': 'none',
        			'marginTop': '10px'
        		});
        		$('[id ^= "answerPreviewPickManyOptionDiv"]').css({
        			'minWidth': '160px',
        			'height': '160px',
        			'margin': '0px 30px 20px 0px',
        			'padding': '0'
        		});
        		$('#answerPreviewPickManyDiv').css({
        			'textAlign': 'center',
        			'float': 'none'
        		});
        		$('[id ^= "answerOptionImageType"]').css({
        			'height': '100%',
        			'borderRadius': '4px'
        		});
        	}
        	
    	} /*selectedValue if ends*/
    	else if(selectedValue == 6){
    		
    		if( ($('#mediaEnableCheckboxPickOne').prop('checked') == false ) && ( $('.pickOneOptionType').find(':selected').text() == "Text") ){
    			//alert("media 0 text 1");
        		$('#mediaPreviewPickOneDiv').removeClass().addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
    			$('#answerPreviewPickOneDiv').removeClass().addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left");
    			
        		$('#answerPreviewPickOneDiv').css({
        			'textAlign': 'left'
        		});
        		$('[id ^= "answerPreviewPickOneOptionDiv"]').css({
        			'minWidth': '85%',
        			'height': 'auto',
        			'margin': '0px 0px 10px 5px',
        			'paddingLeft': '15px'
        		});
        	}else if( ($('#mediaEnableCheckboxPickOne').prop('checked') == false ) && ( $('.pickOneOptionType').find(':selected').text() == "Image") ){
        		$('#mediaPreviewPickOneDiv').removeClass().addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
    			$('#answerPreviewPickOneDiv').removeClass().addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left");
    			
        		$('[id ^= "answerPreviewPickOneOptionDiv"]').css({
        			'minWidth': '180px',
        			'height': '180px',
        			'margin': '0px 45px 18px 0px',
        			'padding': '0'
        		});
        		$('#answerPreviewPickOneDiv').css({
        			'textAlign': 'center'
        		});
        		$('[id ^= "answerOptionPickOneImage"]').css({
        			'height': '100%',
        			'borderRadius': '4px'
        		});
        	}else if( ($('#mediaEnableCheckboxPickOne').prop('checked') == true ) && ( $('.pickOneOptionType').find(':selected').text() == "Text") ){
        		
        			$('#mediaPreviewPickOneDiv').removeClass().addClass("col-xs-5 col-sm-5 col-md-5 col-lg-5");
        			$('#answerPreviewPickOneDiv').removeClass().addClass("col-xs-7 col-sm-7 col-md-7 col-lg-7 text-left");
        			
        			$('#mediaPreviewPickOneDiv').css({
        				/*'display': 'block',*/
        				'height': '200px',
        				'float': 'left',
        				'marginTop': '15px'
        			});
        			
        			$('#answerPreviewPickOneDiv').css({    				
        				'float': 'left',
        				'textAlign': 'left'
        			});
        			    			
            		$('[id ^= "answerPreviewPickOneOptionDiv"]').css({
            			'minWidth': '85%',
            			'height': 'auto',
            			'margin': '0px 0px 10px 5px',
            			'paddingLeft': '15px'
            		});
        		
        		
        	}else if( ($('#mediaEnableCheckboxPickOne').prop('checked') == true ) && ( $('.pickOneOptionType').find(':selected').text() == "Image") ){
        		$('#mediaPreviewPickOneDiv').removeClass().addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
    			$('#answerPreviewPickOneDiv').removeClass().addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left");
    			
        		$('#mediaPreviewPickOneDiv').css({
        			'height': '200px',
        			'float': 'none',
        			'marginTop': '10px'
        		});
        		$('[id ^= "answerPreviewPickOneOptionDiv"]').css({
        			'minWidth': '160px',
        			'height': '160px',
        			'margin': '0px 30px 20px 0px',
        			'padding': '0'
        		});
        		$('#answerPreviewPickOneDiv').css({
        			'textAlign': 'center',
        			'float': 'none'
        		});
        		$('[id ^= "answerOptionPickOneImage"]').css({
        			'height': '100%',
        			'borderRadius': '4px'
        		});
        	}
    		
    	}
    	

    	
    });
    
    $(document).on('change','.dynamicQuestionTypeSelect',function() {
				selectedValueBlank = $(this).find(':selected').val();
				selectedTextBlank = $(this).find(':selected').text();
				//alert('selectedValueBlank '+selectedValueBlank+'selectedTextBlank '+selectedTextBlank);
				
				if(selectedValueBlank == 3){
					/*$('#paragraphDropDownBlank').before('<div class="form-group" id="paragraphBeforeDropDownBlank" style="border-top: 1px solid #dadada; padding-top: 20px;"></div>');
					$('#paragraphBeforeDropDownBlank').html( $('#paragraphDropDownBlank').html() );*/
					
					$('#paragraphDropDownBlank').before('<div class="form-group">'+
						        '<label class="col-sm-2 col-md-2 col-lg-2 control-label">Instruction <span class="text-danger"> * </span></label>'+
						        '<div class="col-sm-10 col-md-10 col-lg-10">'+
						        	'<textarea name="queInstruction" id="instructionBlank" placeholder="Please select appropriate words"></textarea>'+                                              
						        '</div>'+
					        '</div>');
					$('#paragraphDropDownBlank').css({
						'borderTop': '0px solid #dadada'
					});
					$('#instructionBlank').closest('.form-group').css({
						'borderTop': '1px solid #dadada',
						'paddingTop': '20px'
					});
				}else{
					//alert('esle selected');
					$('#paragraphDropDownBlank').css({
						'borderTop': '1px solid #dadada'
					});
					$('#instructionBlank').closest('.form-group').remove();
				}
			});
    
   
   $(document).on('mouseover','#mediaPreviewPickManyDiv',function() {
    	var $currentEl = $(this);
    	
    	if( ($('input[name=radioMediaTypePickMany]:checked').attr('value') == 'imagePickMany') && ( $('.optionTypePickMany').find(':selected').text() == "Image" ) ){
    		$currentEl.css({
    			'transform': 'scale(1.5)',
    			'transformOrigin': 'top center'
    		});
    	}
    	else if( $('input[name=radioMediaTypePickMany]:checked').attr('value') == 'imagePickMany' ){
    		$currentEl.css({
    			'transform': 'scale(1.5)'
    		});    		
    	} 
    });
   
   $(document).on('mouseover','#mediaPreviewPickOneDiv',function() {
   	var $currentEl = $(this);
   	
   	if( ($('input[name=radioMediaTypePickOne]:checked').attr('value') == 'imagePickOne') && ( $('.pickOneOptionType').find(':selected').text() == "Image" ) ){
		$currentEl.css({
			'transform': 'scale(1.5)',
			'transformOrigin': 'top center'
		});
	}
	else if( $('input[name=radioMediaTypePickOne]:checked').attr('value') == 'imagePickOne' ){
   		$currentEl.css({
   			'transform': 'scale(1.5)'
   		});    		
   	}
   });
    /*$(document).hover('#mediaPreviewPickManyDiv',function(){
    	alert("working cool");
    	  $(this).css({"border": "1px solid red"});
    	  }, function(){
    	  $(this).css({"border": "1px solid blue"});
    	});*/
    
   $(document).on('mouseout','#mediaPreviewPickManyDiv',function() {
   	var $currentEl = $(this);
   	
   	if( ($('input[name=radioMediaTypePickMany]:checked').attr('value') == 'imagePickMany') && ( $('.optionTypePickMany').find(':selected').text() == "Image" ) ){   		
   		$currentEl.css({
   			'transform': 'scale(1)',
   			'transformOrigin': 'top center'
   		});
   	}
   	else if( $('input[name=radioMediaTypePickMany]:checked').attr('value') == 'imagePickMany' ){
   		$currentEl.css({
   			'transform': 'scale(1)'
   		});    		
   	} 
   });
   
   $(document).on('mouseout','#mediaPreviewPickOneDiv',function() {
	   	var $currentEl = $(this);
	   	
	   	if( ($('input[name=radioMediaTypePickOne]:checked').attr('value') == 'imagePickOne') && ( $('.pickOneOptionType').find(':selected').text() == "Image" ) ){   		
	   		$currentEl.css({
	   			'transform': 'scale(1)',
	   			'transformOrigin': 'top center'
	   		});
	   	}
	   	else if( $('input[name=radioMediaTypePickOne]:checked').attr('value') == 'imagePickOne' ){
	   		$currentEl.css({
	   			'transform': 'scale(1)'
	   		});    		
	   	} 
   });
   
    
});