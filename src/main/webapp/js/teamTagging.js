function getSchoolData(schoolGroupId) {
			var array = {}
			array["schoolGroupId"] = schoolGroupId;
			$
					.ajax({
						type : "Post",
						contentType : "application/json; charset=UTF-8",
						url : 'getSchoolValueTagging.do',
						cache : false,
						async : false,
						data : JSON.stringify(array),
						success : function(result) {

							var addata = '<option>select</option>';

							$.each(result.value, function(k, v) {
								addata += '<option value='+v.schoolId+'>'
										+ v.schoolName + '</option>';

							});

							document.getElementById('schoolTagging').innerHTML = addata;
							document.getElementById('schoolTagnext').innerHTML = addata;

						},
						error : function() {
							alert('Error while request..');
						}
					});

		}


function getCallidolevel() {
	
	  var curriculum=document.getElementById('Tagcurri');
	  var curriculumName = curriculum.options[curriculum.selectedIndex].text;
	  var schoolId = document.getElementById('schoolTaggingId').value;
	  //alert("curriculum"+curriculum);
	  var array = {
			'curriculumName' : curriculumName,
			'schoolId' : schoolId
	  };
		$.ajax({
			type : "Post",
			contentType : "application/json; charset=UTF-8",
			url : 'getcallidolevelValue.do',
			cache : false,
			async: false,
			data : JSON.stringify(array),
			success : function(result) {
			
				var addata = '<option></option>';

				$.each(result.value, function(k, v) {
					addata += '<option value='+v.sglId+'>'+ v.callidoLevelName + '</option>';
					
				});

				document.getElementById('tagCallidoLevel').innerHTML = addata;
			},
			error : function() {
				alert('Error while request..');
			}
		});
	}


function getPackageData(sglId) {
	//alert(sglId);
	  var array = {}
		array["sglId"] = sglId;
		$.ajax({
			type : "Post",
			contentType : "application/json; charset=UTF-8",
			url : 'getCallidoLevelPackagesTeam.do',
			cache : false,
			async: false,
			data : JSON.stringify(array),
			success : function(result) {
				
              var adddataTeam='<option></option>';
				$.each(result.value, function(k, v) {
					
					
					adddataTeam += '<option value='+v.teamId+'>'+ v.teamCode + '</option>';
					
				});
				
				document.getElementById('tagTeam').innerHTML = adddataTeam;
			},
			error : function() {
				alert('Error while request..');
			}
		});
	}

$(function(){
	$(document).on('change','#schoolGroupTagging',function(){
		$('#schoolTagging').focus();
		
	});
});