// WRITE THE VALIDATION SCRIPT.
     function isNumber(evt) { 
     //alert('this value='+evt.value)
    var correctDropZone = evt.value;
    var dropzone= $('.abc').children('tr').length;
   
    if(isNaN(correctDropZone)){
    evt.value = '';
    return false;
    }else{
    if(correctDropZone>0 && correctDropZone<=dropzone){
    return true;
    }else{
    evt.value = '';
    swal('Drop Zone '+' '+correctDropZone+' '+'Not Present');
    }
    }
      
        return true;
    }

    
    function onlyAlphabets(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode ==32))
                return true;
            else
            	
                return false;
        }
        catch (err) {
            alert(err.Description);
        }
    }
    
    $(function(){
    	//alert("Inside validation script");
    	
		var selectedValidValueGlobal = $('.dynamicQuestionTypeSelect').find(':selected').val();		    	
        var editBodyId = $("body").attr("id");
        
        /*initializing media file name field*/
        if( editBodyId == "editQuestionDashboard" ){
        	//alert('inside edit');
        	if ( (selectedValidValueGlobal == 1) ) {   
        		$('[id ^= "dropZoneOptionName"]').each(function(){        			
    				var dropZoneOptionId = this.id[18];    			
    				
    				if ($(this).val() == "") {
    					$('#dropZoneName' + dropZoneOptionId).text("Drop Zone " + dropZoneOptionId);
    					$('#dropZonePreviewName' + dropZoneOptionId).text("Drop Zone " + dropZoneOptionId);
    				}
        		});
        		    			
    		}
        	
        	if ( (selectedValidValueGlobal == 2) ) {   
        		$('[id ^= "previewDropDown"]').each(function(){
        			
        		});
    			var fileNameMedia = $('#mediaTypePickMany').attr('value');    			
    			$('#mediaTypePickMany').val( fileNameMedia );        		    			
    		}
        	if ( (selectedValidValueGlobal == 5) && ( $('#mediaEnableCheckbox').prop("checked") == true ) ) {          		        			
    			var fileNameMedia = $('#mediaTypePickMany').attr('value');    			
    			$('#mediaTypePickMany').val( fileNameMedia );        		    			
    		}
        	if ( (selectedValidValueGlobal == 6) && ( $('#mediaEnableCheckboxPickOne').prop("checked") == true ) ) {          		        			
    			var fileNameMediaPickOne = $('#mediaTypePickOne').attr('value');    			
    			$('#mediaTypePickOne').val( fileNameMediaPickOne );        		    			
        	}        	
        	
		}
    	
    	$(document).on('click','#createQuestionSave',function(e){  
    		var selectedValidValue = $('.dynamicQuestionTypeSelect').find(':selected').val();
    		
    		if (selectedValidValue == 1) {
        		$('[id ^= "dragItemImage"]').each(function(){
        			if($(this).attr('src') == "" ){
        				swal("Please select Drag Item Image");
        				//$(this).focus();
        				e.preventDefault();
        			}
        		});
    		}else if(selectedValidValue == 2){    			
    			$('[id ^= "dropdownQuestionImage"]:visible').each(function(){
        			if($(this).attr('src') == "" ){
        				swal("Please select Dropdown Image");
        				//$(this).focus();
        				e.preventDefault();
        			}
        		});
    			$('.dynamicOptionContent [id ^= "textareaDropDown"]').each(function(){
        			if($(this).val() == "" ){
        				swal("Please fill the Textarea");
        				//$(this).focus();
        				e.preventDefault();
        			}
        		});
    		}else if(selectedValidValue == 4){      			
    			if( $('#hotSpotImage').attr('src') == "" ){    				
    				swal("Please select Hotspot Image");
    				//$('#hotSpotImage').focus();
    				e.preventDefault();
    			}        		
    		}else if(selectedValidValue == 5){       			
    			if( $('#mediaEnableCheckbox').prop("checked") == true ){    				
    				if( $('#mediaTypePickMany').attr('src') == "" ){
        				swal("Please select Media File");
        				//$(this).focus();
        				e.preventDefault();
        			}    			
    				
    			}
    			
    			$('[id ^= "inputPickMany"]').each(function(){
        			if($(this).attr('src') == "" ){
        				swal("Please select Option Image");
        				//$(this).focus();
        				e.preventDefault();
        			}
        		});
    		}else if(selectedValidValue == 6){ 
    			if( $('#mediaEnableCheckboxPickOne').prop("checked") == true ){    				
    				if( $('#mediaTypePickOne').attr('src') == "" ){
        				swal("Please select Media File");
        				//$(this).focus();
        				e.preventDefault();
        			}
    			}
    			$('[id ^= "pickOneOption"]').each(function(){
        			if($(this).attr('src') == "" ){
        				swal("Please select Option Image");
        				//$(this).focus();
        				e.preventDefault();
        			}
        		});
    		}
    		
    	});
    	
    });