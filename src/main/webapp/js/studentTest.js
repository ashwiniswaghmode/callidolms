$(function() {
	
	

	$('#submitTestButton').click(function(e) {
		
		history.pushState({ 'status': 'ongoing' }, null, null);
		
		var queType=$('#questionType').val();
		var pickOne = "pick one";
		var pickMany = "pick many";


		
		
		if (queType.toUpperCase() === pickOne.toUpperCase()) {
			
			var checked = 0;
			var mediaOptionType = $('#mediaOptionTypePickOne').val();
			
			if(mediaOptionType == 1){
				checked =parseInt( $("input[type=radio]:checked").length );
				
				
				if (checked == 0) {
					
					 e.preventDefault();
					    var form = $(this).parents('form');
					    swal({
					        title: "Are you sure?",
					        text: "You have not answered this question.\n " +
					        		"Do you still want to continue?",
					        type: "warning",
					        showCancelButton: true,
					        cancelButtonText: 'Go back to question',
					        confirmButtonColor: "#dd3333",
					        confirmButtonText: "Continue",
					        cancelButtonColor: '#dd3333',
					        closeOnConfirm: false
					    },function(isConfirm){
					        if (isConfirm) form.submit();
					    });
				}
			}
			else if(mediaOptionType == 2){
				
				var imgOption = 0;
				imgOption = document.getElementById("optionId1").value;
				
				if (imgOption == "") {
					
					 e.preventDefault();
					    var form = $(this).parents('form');
					    swal({
					        title: "Are you sure?",
					        text: "You have not answered this question.\n " +
					        		"Do you still want to continue?",
					        type: "warning",
					        showCancelButton: true,
					        cancelButtonText: 'Go back to question',
					        confirmButtonColor: "#dd3333",
					        confirmButtonText: "Continue",
					        cancelButtonColor: '#dd3333',
					        closeOnConfirm: false
					    },function(isConfirm){
					        if (isConfirm) form.submit();
					    });
				}	
			}

		}
		
		
		
		if (queType.toUpperCase() === pickMany.toUpperCase()) {
		
			/* alert('in pick many');*/
			var checked = 0;
			var mediaOptionType = $('#mediaOptionTypePickMany').val();
			
			if(mediaOptionType == 1){
				checked =parseInt( $("input[type='checkbox']:checked").length );
				
				if (checked==0) {
					 e.preventDefault();
					var form = $(this).parents('form');
					    swal({
					        title: "Are you sure?",
					        text: "You have NOT answered this question.\n " +
					        		"Do you still want to continue?",
					        type: "warning",
					        showCancelButton: true,
					        cancelButtonText: 'Go back to question',
					        confirmButtonColor: "#dd3333",
					        confirmButtonText: "Continue",
					        cancelButtonColor: '#dd3333',
					        closeOnConfirm: false
					    }, function(isConfirm){
					        if (isConfirm) form.submit();
					    });
				}
				
			}else if(mediaOptionType == 2){
				
					var count = 0;
					var optionPickManyImg;
					var optionCount = 0;
					optionCount = $('#optionCount').val();
					
					for(var i=1; i<=optionCount; i++){
						optionPickManyImg = $('#optionIMG'+ i).val();
						if(optionPickManyImg == "selected"){
							count++;
						}
					}
					
					if(count == 0){
						 e.preventDefault();
							var form = $(this).parents('form');
							    swal({
							        title: "Are you sure?",
							        text: "You have NOT answered this question.\n " +
							        		"Do you still want to continue?",
							        type: "warning",
							        showCancelButton: true,
							        cancelButtonText: 'Go back to question',
							        confirmButtonColor: "#dd3333",
							        confirmButtonText: "Continue",
							        cancelButtonColor: '#dd3333',
							        closeOnConfirm: false
							    }, function(isConfirm){
							        if (isConfirm) form.submit();
							    });
				}
				
			}	

		}
		
		
	});
	
	
	$('#backToQuestion').click(function() {
		$('#answerNotSelectedModal').modal('hide');
	});
	
	
	
});