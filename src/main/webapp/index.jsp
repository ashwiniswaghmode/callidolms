<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="shortcut icon" type="image/png" href="/imgs/favicon.png" /> -->
    <title>Login</title>

    <!-- inject:css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/simple-line-icons.css">
    <link rel="stylesheet" href="css/weather-icons.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- endinject -->

    <!-- Main Style  -->
    <link rel="stylesheet" href="css/SAE.css">
     <link rel="stylesheet" href="css/login.css">
     <link rel="stylesheet" href="css/new_common.css">
<!--horizontal-timeline-->
    <link rel="stylesheet" href="css/style.css">

<style>
    .note{
  	 position: relative;
    border: none;
    width: 306px;
    height: 22px;
  	color:blue;
    }
    
</style>
</head>
<body>

    <div id="ui" class="ui imagebg">

        <!--header start-->
        <header id="header" class="ui-header">

            <div class="navbar-header">
                <!--logo start-->
                <a href="#" class="navbar-brand">
                    <span class="logo"><img src="images/callido_logo.png" alt="" style="width: 150px;" /></span>
                    <span class="logo-compact"><img src="imgs/logo-icon-dark.png" alt=""/></span>
                </a>
                <!--logo end-->
            </div>
        </header>
        <div class="sign-in-wrapper ">
            <div class="sign-container loginContainer">
            	<div class="row">
            		<h3 style="text-align: center;color: #1D1E4E;">Admin Login</h3>
            	</div>
               <form class="sign-in-form"  id="loginForm" role="form" action="login.do"  method="post">
					<!--  <div class="form-group"> -->
					<div class="row">
						<div>
							<input type="text" id="floatField4" class="form-control"  placeholder="Enter Email Id" autocomplete="new-password" name="userId" required="" style="background-color:#fff!important;">
							<label for="floatField4" class="flotlbl floatingLableLogin" >User Name</label>
						</div>
					</div>	
					<div class="row">
						<div class="float-container">
							<input type="password" id="floatField5" class="form-control test eyeClass"  placeholder="Enter Password" autocomplete="new-password" name="password" required="">
							<label for="floatField5" class="floatingLabel flotlbl floatingLableLogin">Password									 
							</label>
							<span toggle="#floatField5" class="fa fa-fw fa-eye field-icon toggle-password eye-img" ></span> 
								  					
						</div> 
					</div>
                    <div class="form-group text-left">
                    <div class="row mt-25">
                    	<div class="col-md-6">
                       <!--  <label class="i-checks"> -->
                        <label class="i-checks pl-0">
                            <input type="checkbox" class="mr-0">
                            <i></i>
                        </label>
                      <!--   <span class="remember-me">Remember me</span> -->
                        <span class="fs-13">Remember me</span>
                        </div>
                        <div class="col-md-6">
                        <a href="forgotpassword.do" class="forget-pwd fgt-text" ><small>Forgot password?</small></a>
                    </div>
                    </div>
                    </div>
                     <div class="row">
                    <button type="submit" class="sg-btn login-btn mt-8-0" >Login</button>
                    </div>
                   
                    <div class="row">
                    	<p class="text-muted help-block mt-10"><small class="have-account">Do not have an account?</small></p>
                        <a  href="registration.do" class="register fs-13">Register Now</a>
                    </div>
                    
                </form>
                <!-- <div class="text-center copyright-txt">
                    <small>MegaDin - Copyright � 2017</small>
                </div> -->
            </div>
        </div>
    </div>    


    <!-- inject:js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/autosize.min.js"></script>
    <!-- endinject -->

    <!--horizontal-timeline-->
    <script src="js/horizontal-timeline/js/jquery.mobile.custom.min.js"></script>
    <script src="js/main.js"></script>

    <!-- Common Script   -->
    <script src="js/SAE.js"></script>
    
    
    <script>

    $("#").on('submit',function(ev) {
        ev.preventDefault();
        
        var userId = $("#floatField4").val();
        var password = $("#floatField5").val();
        alert(" loginform ",userId);
        var data = {
            "userId": userId,
            "password": password
        }
        
        var preparedData= JSON.stringify(data);
        
        $.ajax({
            type: "POST",
            url: "loginLMS.do",
            dataType: 'json',
            data: preparedData,
            contentType: "application/json",
           
            success: function(response)
            {
                alert(" success");
                if(response.message =="Login success"){
    				
                }else{
                    alert(response.message );
                }
                
            },
            error: function () {
                swal("Error while login");
                }
        });
        //this is mandatory other wise your from will be submitted.
        return false; 
    });

    </script>
 <script>
		window.onload = Onload;
		function Onload() {
			var errorMsg = "<c:out value="${user.message}" />";
			$("#mdid1").html("<b>" + errorMsg + " </b>");
			if (errorMsg != "")
				$('#exampleModalCenter3').modal("toggle");
		}
	
	$(".userNAME").mouseover(function() {
	    $(this).children(".note").show();
	}).mouseout(function() {
	    $(this).children(".note").hide();
	});
	
	</script>

<script type="text/javascript">
	 $(".toggle-password").click(function() { 
		  $(this).toggleClass("fa-eye fa-eye-slash");
			  var input = $($(this).attr("toggle"));
			  if (input.attr("type") == "password") {
				  input.attr("type", "text");
			  } else {
				  input.attr("type", "password");
			  }
			});
</script> 
</body>
</html>